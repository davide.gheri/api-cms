<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->hasHeader('X-Locale')) {
            LaravelLocalization::setLocale($request->header('X-Accept-Language'));
        } else if ($request->has('locale')) {
            LaravelLocalization::setLocale($request->input('locale'));
        }
        return $next($request);
    }
}
