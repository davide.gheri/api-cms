<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\V1\CreateTermRequest;
use App\Http\Requests\Api\V1\UpdateTermRequest;
use App\Http\Resources\ContentResource;
use App\Http\Resources\TermResource;
use App\Repositories\TaxonomyRepository;
use App\Repositories\TermRepository;
use Illuminate\Http\Request;
use Api\Core\Http\Controllers\Controller;

class TermController extends Controller
{
    /**
     * @var TermRepository
     */
    private $repository;
    /**
     * @var TaxonomyRepository
     */
    private $taxonomyRepository;

    public function __construct(TermRepository $repository, TaxonomyRepository $taxonomyRepository)
    {
        $this->repository = $repository;
        $this->taxonomyRepository = $taxonomyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $taxonomyId
     * @return \Illuminate\Http\Resources\Json\ResourceCollection
     */
    public function index($taxonomyId)
    {
        $taxonomy = $this->taxonomyRepository->find($taxonomyId);

        return TermResource::collection($taxonomy->terms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateTermRequest $request
     * @param $taxonomyId
     * @return TermResource
     */
    public function store(CreateTermRequest $request, $taxonomyId)
    {
        $data = array_merge($request->only(['title', 'description', 'contents']), ['taxonomy_id' => $taxonomyId]);

        return new TermResource($this->repository->create($data));
    }

    /**
     * Display the specified resource.
     *
     * @param $taxonomyId
     * @param  int $id
     * @return TermResource
     */
    public function show($taxonomyId, $id)
    {
        return new TermResource($this->repository->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTermRequest $request
     * @param $taxonomyId
     * @param  int $id
     * @return TermResource
     */
    public function update(UpdateTermRequest $request, $taxonomyId, $id)
    {
        $data = array_merge($request->only(['title', 'description', 'contents']), ['taxonomy_id' => $taxonomyId]);

        return new TermResource($this->repository->update($data, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $taxonomyId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($taxonomyId, $id)
    {
        $this->repository->delete($id);

        return response('OK', 200);
    }

    public function contents($taxonomyId, $id)
    {
        $term = $this->repository->find($id);

        return ContentResource::collection($term->contents);
    }
}
