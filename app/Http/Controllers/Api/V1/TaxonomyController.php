<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\v1\CreateTaxonomyRequest;
use App\Http\Requests\Api\V1\UpdateTaxonomyRequest;
use App\Http\Resources\ContentTypeResource;
use App\Http\Resources\TaxonomyResource;
use App\Repositories\TaxonomyRepository;
use Illuminate\Http\Request;
use Api\Core\Http\Controllers\Controller;

class TaxonomyController extends Controller
{
    /**
     * @var TaxonomyRepository
     */
    private $repository;

    public function __construct(TaxonomyRepository $repository)
    {
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\ResourceCollection
     */
    public function index()
    {
        return TaxonomyResource::collection($this->repository->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateTaxonomyRequest $request
     * @return TaxonomyResource
     */
    public function store(CreateTaxonomyRequest $request)
    {
        $data = $request->only(['name', 'description', 'slug', 'content_types']);

        return new TaxonomyResource($this->repository->create($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return TaxonomyResource
     */
    public function show($id)
    {
        return new TaxonomyResource($this->repository->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTaxonomyRequest $request
     * @param  int $id
     * @return TaxonomyResource
     */
    public function update(UpdateTaxonomyRequest $request, $id)
    {
        $data = $request->only(['name', 'description', 'slug', 'content_types']);

        return new TaxonomyResource($this->repository->update($data, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        return response('OK', 200);
    }

    public function contentTypes($id)
    {
        $taxonomy = $this->repository->find($id);

        return ContentTypeResource::collection($taxonomy->contentTypes);
    }
}
