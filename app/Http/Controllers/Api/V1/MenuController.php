<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\V1\CreateMenuRequest;
use App\Http\Requests\Api\V1\UpdateMenuRequest;
use App\Http\Resources\MenuResource;
use App\Repositories\MenuRepository;
use Illuminate\Http\Request;
use Api\Core\Http\Controllers\Controller;

class MenuController extends Controller
{
    /** @var MenuRepository */
    private $repository;

    public function __construct(MenuRepository $repository)
    {
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MenuResource::collection($this->repository->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMenuRequest $request)
    {
        $data = $request->only(['name', 'description', 'slug']);

        return new MenuResource($this->repository->create($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new MenuResource($this->repository->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenuRequest $request, $id)
    {
        $data = $request->only(['name', 'description', 'slug']);

        return new MenuResource($this->repository->update($data, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        return response('OK', 200);
    }
}
