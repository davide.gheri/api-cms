<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\V1\CreateContentRequest;
use App\Http\Requests\Api\V1\UpdateContentRequest;
use App\Http\Resources\AttachmentResource;
use App\Http\Resources\ContentResource;
use App\Models\Content;
use App\Repositories\ContentRepository;
use App\Repositories\ContentTypeRepository;
use Api\Core\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    /**
     * @var ContentRepository
     */
    private $repository;
    /**
     * @var ContentTypeRepository
     */
    private $contentTypeRepository;

    public function __construct(ContentRepository $repository, ContentTypeRepository $contentTypeRepository)
    {
        $this->repository = $repository;
        $this->contentTypeRepository = $contentTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $contentTypeId
     * @return \Illuminate\Http\Resources\Json\ResourceCollection
     */
    public function index(Request $request, $contentTypeId)
    {
        if ($request->has('status')) {
            $this->repository->setScope('status', $request->input('status'));
        }
        $this->repository->where('content_type_id', $contentTypeId);

        if($request->has('paginate') && (
            $request->input('paginate') == 'false' ||
            $request->input('paginate') == 0 ||
            $request->input('paginate') == false
        )) {
            $contents = $this->repository->all();
        } else {
            $contents = $this->repository->paginate();
        }

        return ContentResource::collection($contents);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateContentRequest $request
     * @param $contentTypeId
     * @return ContentResource
     */
    public function store(CreateContentRequest $request, $contentTypeId)
    {
        $data = array_merge($request->only(['fields', 'status', 'terms', 'title']), ['content_type_id' => $contentTypeId]);

        return new ContentResource($this->repository->create($data));
    }

    /**
     * Display the specified resource.
     *
     * @param $contentTypeId
     * @param  int $id
     * @return ContentResource
     */
    public function show($contentTypeId, $id)
    {
        return new ContentResource($this->repository->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContentRequest $request
     * @param $contentTypeId
     * @param  int $id
     * @return ContentResource
     */
    public function update(UpdateContentRequest $request, $contentTypeId, $id)
    {
        $data = array_merge($request->only(['fields', 'status', 'terms', 'title']), ['content_type_id' => $contentTypeId]);

        return new ContentResource($this->repository->update($data, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($contentTypeId, $id)
    {
        $this->repository->delete($id);

        return response('OK', 200);
    }

    public function attachment(Request $request, $contentTypeId, $id)
    {
        if ($request->hasFile('attachment') && $request->file('attachment')->isValid()) {
            /** @var Content $content */
            $content = $this->repository->find($id);
            $name = $request->input('field', 'default');
            $media = $content->addMediaFromRequest('attachment')
                ->withResponsiveImages()
                ->toMediaCollection($name);
            $content->clearMediaCollectionExcept($name, $media);
            $content->fields()->updateOrCreate(['name' => $name], ['value' => $media->getFullUrl()]);

            return new AttachmentResource($media);
        }
    }

    public function nextPrev($contentTypeId, $id)
    {
        $content = $this->repository->find($id);
        $next = Content::published()
            ->where('content_type_id', $content->content_type_id)
            ->where('created_at', '>', $content->created_at)
            ->orderBy('created_at')
            ->first();
        $prev = Content::published()
            ->where('content_type_id', $content->content_type_id)
            ->where('created_at', '<', $content->created_at)
            ->orderBy('created_at')
            ->first();

        return [
            'next' => $next ? new ContentResource($next) : null,
            'prev' => $prev ? new ContentResource($prev) : null,
        ];
    }

    public function last($contentTypeId)
    {
        $this->repository->setScope('status', Content::PUBLISHED);
        $this->repository->setScope('last');
        $this->repository->where('content_type_id', $contentTypeId);

        return new ContentResource($this->repository->all()->first());
    }
}
