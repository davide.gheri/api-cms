<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\V1\CreateContentTypeFieldRequest;
use App\Http\Requests\Api\V1\UpdateContentTypeFieldRequest;
use App\Http\Resources\ContentTypeFieldResource;
use App\Repositories\ContentTypeFieldRepository;
use App\Repositories\ContentTypeRepository;
use Api\Core\Http\Controllers\Controller;

class ContentTypeFieldController extends Controller
{
    /** @var ContentTypeFieldRepository */
    private $repository;

    /** @var ContentTypeRepository */
    private $contentTypeRepository;

    public function __construct(ContentTypeFieldRepository $repository, ContentTypeRepository $contentTypeRepository)
    {
        $this->repository = $repository;
        $this->contentTypeRepository = $contentTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $contentTypeId
     * @return \Illuminate\Http\Resources\Json\ResourceCollection
     */
    public function index($contentTypeId)
    {
        $contentType = $this->contentTypeRepository->find($contentTypeId);

        return ContentTypeFieldResource::collection($contentType->fields);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateContentTypeFieldRequest $request
     * @param $contentTypeId
     * @return ContentTypeFieldResource
     */
    public function store(CreateContentTypeFieldRequest $request, $contentTypeId)
    {
        $data = array_merge($request->only(['name', 'options', 'slug', 'field_schema_id']), [
            'content_type_id' => $contentTypeId
        ]);

        return new ContentTypeFieldResource($this->repository->create($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ContentTypeFieldResource
     */
    public function show($contentTypeId, $id)
    {
        return new ContentTypeFieldResource($this->repository->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContentTypeFieldRequest $request
     * @param $contentTypeId
     * @param  int $id
     * @return ContentTypeFieldResource
     */
    public function update(UpdateContentTypeFieldRequest $request, $contentTypeId, $id)
    {
        $data = array_merge($request->only(['name', 'options', 'slug', 'field_schema_id']), [
            'content_type_id' => $contentTypeId
        ]);

        return new ContentTypeFieldResource($this->repository->update($data, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($contentTypeId, $id)
    {
        $this->repository->delete($id);

        return response('OK', 200);
    }
}
