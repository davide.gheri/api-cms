<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\V1\CreateContentTypeRequest;
use App\Http\Requests\Api\V1\UpdateContentTypeRequest;
use App\Http\Resources\ContentTypeResource;
use App\Http\Resources\TaxonomyResource;
use App\Repositories\ContentTypeRepository;
use Api\Core\Http\Controllers\Controller;

class ContentTypeController extends Controller
{
    /** @var ContentTypeRepository */
    private $repository;

    public function __construct(ContentTypeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\ResourceCollection
     */
    public function index()
    {
        return ContentTypeResource::collection($this->repository->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateContentTypeRequest $request
     * @return ContentTypeResource
     */
    public function store(CreateContentTypeRequest $request)
    {
        $contentType = $this->repository->create($request->only(['name', 'description', 'slug']));

        return new ContentTypeResource($contentType);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ContentTypeResource
     */
    public function show($id)
    {
        return new ContentTypeResource($this->repository->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContentTypeRequest $request
     * @param  int $id
     * @return ContentTypeResource
     */
    public function update(UpdateContentTypeRequest $request, $id)
    {
        $contentType = $this->repository->update($request->only(['name', 'description', 'slug']), $id);

        return new ContentTypeResource($contentType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);

        return response('OK', 200);
    }

    public function taxonomies($id)
    {
        $contentType = $this->repository->find($id);

        return TaxonomyResource::collection($contentType->taxonomies);
    }
}
