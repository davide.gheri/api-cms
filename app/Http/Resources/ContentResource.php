<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fields = $this->resource->fields->mapWithKeys(function($field) {
            if ($field->name != 'excerpt' && $field->contentTypeField->schema->name == 'attachment') {
                return [$field->name => new AttachmentResource($this->resource->getMedia($field->name)->first())];
            }
            return [$field->name => json_decode($field->getTranslation('value', app()->getLocale())) ?: $field->getTranslation('value', app()->getLocale())];
        });
        $terms = $this->resource->terms()->with('taxonomy')->get()->groupBy('taxonomy.slug')->mapWithKeys(function($terms, $taxonomy) {
            return [$taxonomy => TermResource::collection($terms)];
        });

        return [
            'id' => $this->resource->id,
            'title' => $this->resource->title,
            'status' => $this->resource->getStatus(),
            'fields' => $fields,
            'terms' => $terms,
            'content_type_id' => $this->resource->content_type_id,
            'content_type' => new ContentTypeResource($this->resource->type),
            'created_at' => $this->resource->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->resource->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
