<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AttachmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'collection_name' => $this->resource->collection_name,
            'filename' => $this->resource->file_name,
            'id' => $this->resource->id,
            'model' => $this->resource->model,
            'name' => $this->resource->name,
            'filesize' => $this->resource->size,
            'full_url' => $this->resource->getFullUrl(),
            'url' => $this->resource->getUrl(),
            'path' => $this->resource->getPath(),
            'thumb' => $this->resource->getFullUrl(ends_with($this->resource->file_name, 'svg') ? '' : 'thumb')
        ];
    }
}
