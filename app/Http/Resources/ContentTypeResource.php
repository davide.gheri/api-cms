<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = $this->resource->toArray();
        foreach ($this->resource->getTranslatableAttributes() as $name) {
            $attributes[$name] = $this->resource->getTranslation($name, app()->getLocale());
        }

        unset($attributes['pivot']);
        $attributes['fields'] = ContentTypeFieldResource::collection($this->resource->fields);
        $attributes['content_count'] = $this->resource->contents()->count();

        return $attributes;
    }
}
