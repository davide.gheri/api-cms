<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TermResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = $this->resource->toArray();
        foreach ($this->resource->getTranslatableAttributes() as $name) {
            $attributes[$name] = $this->resource->getTranslation($name, app()->getLocale());
        }
        unset($attributes['taxonomy_id']);
        unset($attributes['pivot']);
        $attributes['content_count'] = $this->resource->contents()->count();
        $attributes['taxonomy'] = new TaxonomyResource($this->resource->taxonomy);
        return $attributes;
    }
}
