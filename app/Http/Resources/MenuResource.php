<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = $this->resource->toArray();
        foreach ($this->resource->getTranslatableAttributes() as $name) {
            $attributes[$name] = $this->resource->getTranslation($name, app()->getLocale());
        }

        $attributes['menu_items_count'] = $this->resource->menu_items()->count();

        return $attributes;
    }
}
