<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaxonomyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = $this->resource->toArray();
        foreach ($this->resource->getTranslatableAttributes() as $name) {
            $attributes[$name] = $this->resource->getTranslation($name, app()->getLocale());
        }
        if (empty($attributes['pivot'])) {
//            $attributes['contentTypes'] = ContentTypeResource::collection($this->resource->contentTypes);
        }
        unset($attributes['content_types']);
        unset($attributes['pivot']);

        $attributes['term_count'] = $this->resource->terms()->count();

        return $attributes;
    }
}
