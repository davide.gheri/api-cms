<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateContentTypeFieldRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user('api')->tokenCan('update-content-types');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('field');

        return [
            'name' => 'required|string|max:255',
            'options' => 'required|array',
            'field_schema_id' => 'required|integer|exists:field_schemas,id',
            'slug' => [
                'required', 'string', 'max:255',
                Rule::unique('content_type_fields')->ignore($id)
            ]
        ];
    }
}
