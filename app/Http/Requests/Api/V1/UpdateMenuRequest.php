<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('api')->tokenCan('update-menus');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('menu');

        return [
            'name' => 'required|string|max:255',
            'description' => 'max:255',
            'slug' => [
                'required', 'string', 'max:255',
                Rule::unique('menus')->ignore($id)
            ]
        ];
    }
}
