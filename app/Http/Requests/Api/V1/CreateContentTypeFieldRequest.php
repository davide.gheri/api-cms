<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;

class CreateContentTypeFieldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('api')->tokenCan('create-content-types');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'options' => 'required|array',
            'slug' => 'required|string|max:255|unique:content_type_fields',
            'field_schema_id' => 'required|integer|exists:field_schemas,id'
        ];
    }
}
