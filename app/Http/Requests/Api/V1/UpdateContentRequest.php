<?php

namespace App\Http\Requests\Api\V1;


class UpdateContentRequest extends CreateContentRequest
{
    public function authorize()
    {
        return $this->user('api')->tokenCan('update-contents');
    }
}
