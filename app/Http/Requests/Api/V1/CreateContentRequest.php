<?php

namespace App\Http\Requests\Api\V1;

use App\Rules\Fields;
use App\Rules\Status;
use Illuminate\Foundation\Http\FormRequest;

class CreateContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('api')->tokenCan('create-contents');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'fields.*' => [new Fields($this)],
            'status' => [new Status()],
            'terms.*' => 'integer|exists:terms,id'
        ];
    }
}
