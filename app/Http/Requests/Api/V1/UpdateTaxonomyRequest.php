<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateTaxonomyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('api')->tokenCan('update-taxonomies');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('taxonomy');
        return [
            'name' => 'string|max:255',
            'description' => 'max:255',
            'content_types.*' => 'integer|exists:content_types,id',
            'slug' => [
                'required', 'string', 'max:255',
                Rule::unique('taxonomies')->ignore($id)
            ]
        ];
    }
}
