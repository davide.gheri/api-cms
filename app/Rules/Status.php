<?php

namespace App\Rules;

use App\Models\Content;
use Illuminate\Contracts\Validation\Rule;

class Status implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if((int) $value === 0) {
            if (in_array($value, ['PUBLISHED', 'DRAFT', 'ARCHIVED'])) {
                return true;
            }
            return false;
        }
        if (in_array($value, [Content::PUBLISHED, Content::DRAFT, Content::ARCHIVED])) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The status is malformed';
    }
}
