<?php

namespace App\Rules;

use App\Repositories\ContentTypeRepository;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class Fields implements Rule
{
    private $basename = 'fields.';
    private $name;
    /**
     * @var Request
     */
    private $request;

    /**
     * Create a new rule instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($attribute == 'fields.excerpt') {
            return true;
        }
        $this->name = str_replace($this->basename, '', $attribute);
        $type = app(ContentTypeRepository::class)->find($this->request->route('content_type'));
        $schema = $type->fields()->whereSlug($this->name)->first();
        if (!$schema) {
            return false;
        }
        $options = $schema->options;
        $passes = true;
        if (!empty($options['required']) && $options['required']) {
            $passes = !empty($value);
        }
        if (!empty($options['maxlength']) && !empty($value)) {
            $passes = strlen($value) <= $options['maxlength'];
        }
        return $passes;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The ' . $this->name . ' field is invalid';
    }
}
