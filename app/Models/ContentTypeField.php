<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentTypeField extends Model
{
    protected $fillable = [
        'name',
        'options',
        'slug',
        'field_schema_id',
        'content_type_id'
    ];

    protected $casts = [
        'options' => 'array'
    ];

    public function schema()
    {
        return $this->belongsTo(FieldSchema::class, 'field_schema_id');
    }

    public function contentType()
    {
        return $this->belongsTo(ContentType::class);
    }

    public function getOptionsAttribute($value)
    {
        $schema = $this->schema;
        return array_merge($schema->options, json_decode($value, true));
    }
}
