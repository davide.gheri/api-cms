<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Term extends Model
{
    use HasTranslations;

    protected $fillable = ['title', 'description', 'taxonomy_id'];

    public $translatable = ['title', 'description'];

    public function taxonomy()
    {
        return $this->belongsTo(Taxonomy::class);
    }

    public function contents()
    {
        return $this->belongsToMany(Content::class);
    }
}
