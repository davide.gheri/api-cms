<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Content extends Model implements HasMedia
{
    use HasMediaTrait;
    
    const PUBLISHED = 1;
    const DRAFT = 2;
    const ARCHIVED = 3;
    
    protected $fillable = ['content_type_id', 'status', 'title'];

    public function registerMediaConversions(Media $media = null)
    {
        try {
            $this->addMediaConversion('thumb')
                ->width(368)
                ->height(232)
                ->sharpen(10);
        } catch (InvalidManipulation $e) {
        }
    }

    public function type()
    {
        return $this->belongsTo(ContentType::class, 'content_type_id');
    }

    public function fields()
    {
        return $this->hasMany(ContentField::class, 'content_id');
    }

    public function terms()
    {
        return $this->belongsToMany(Term::class);
    }

    public function getStatus()
    {
        switch ($this->status) {
            case self::PUBLISHED:
                return 'PUBLISHED';
            case self::DRAFT:
                return 'DRAFT';
            case self::ARCHIVED:
                return 'ARCHIVED';
            default:
                return 'DRAFT';
        }
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopePublished($query)
    {
        return $this->scopeStatus($query, static::PUBLISHED);
    }

    public function scopeLast($query)
    {
        return $query->orderBy('created_at')->limit(1);
    }

}
