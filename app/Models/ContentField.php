<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ContentField extends Model
{
    use HasTranslations;

    protected $fillable = ['name', 'value', 'content_id'];

    public $translatable = ['value'];

    public function content()
    {
        return $this->belongsTo(Content::class, 'content_id');
    }

    public function contentTypeField()
    {
        return $this->belongsTo(ContentTypeField::class, 'name', 'slug')->with('schema');
    }

}
