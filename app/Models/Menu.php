<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Menu extends Model
{
    use HasTranslations;

    protected $fillable = [
        'name', 'slug', 'description'
    ];

    public $translatable = ['name', 'description'];

    public function menu_items()
    {
        return $this->hasMany(MenuItem::class);
    }
}
