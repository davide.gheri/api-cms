<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ContentType extends Model
{
    use HasTranslations;

    protected $fillable = [
        'name', 'slug', 'description'
    ];

    public $translatable = ['name', 'description'];

    public function fields()
    {
        return $this->hasMany(ContentTypeField::class);
    }

    public function contents()
    {
        return $this->hasMany(Content::class, 'content_type_id');
    }

    public function taxonomies()
    {
        return $this->belongsToMany(Taxonomy::class);
    }
}
