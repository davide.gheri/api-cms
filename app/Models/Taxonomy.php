<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Taxonomy extends Model
{
    use HasTranslations;

    protected $fillable = ['name', 'description', 'slug'];

    public $translatable = ['name', 'description'];

    public function contentTypes()
    {
        return $this->belongsToMany(ContentType::class);
    }

    public function terms()
    {
        return $this->hasMany(Term::class);
    }
}
