<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FieldSchema extends Model
{
    protected $fillable = ['name', 'options', 'available_options'];

    protected $casts = [
        'options' => 'array',
        'available_options' => 'array'
    ];

    public $timestamps = false;
}
