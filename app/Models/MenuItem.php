<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    public $fillable = [
        'url', 'name'
    ];

    public function item() {
        return $this->morphTo();
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
}
