<?php

namespace App\Repositories;

use App\Models\Menu;
use Api\Core\Repositories\Repository;

class MenuRepository extends Repository
{
    public function model(): string
    {
        return Menu::class;
    }
}
