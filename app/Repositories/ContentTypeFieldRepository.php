<?php

namespace App\Repositories;

use App\Models\ContentTypeField;
use Api\Core\Repositories\Repository;

class ContentTypeFieldRepository extends Repository
{
    public function model(): string
    {
        return ContentTypeField::class;
    }
}
