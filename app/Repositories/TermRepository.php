<?php

namespace App\Repositories;

use App\Models\Term;
use Illuminate\Database\Eloquent\Model;
use Api\Core\Repositories\Repository;

class TermRepository extends Repository
{
    public function model(): string
    {
        return Term::class;
    }

    public function create(array $data): Model
    {
        $term = $this->model->create($data);
        if (isset($data['contents'])) {
            $this->syncContents($data['contents'], $term);
        }
        return $term;
    }

    public function update(array $data, $id): Model
    {
        $term = $this->resolveModel($id);
        $term->update($data);

        $this->syncContents($data['contents'] ?? [], $term);

        return $term;
    }

    protected function syncContents($ids, &$term)
    {
        $term->contents()->sync($ids);
    }
}
