<?php

namespace App\Repositories;

use App\Models\ContentType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Api\Core\Repositories\Repository;

class ContentTypeRepository extends Repository
{
    public function model(): string
    {
        return ContentType::class;
    }

    public function find($id, $columns = array('*')): Model
    {
        $this->applyScopes();
        try {
            return $this->model->where('slug', $id)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            return $this->model->findOrFail($id);
        }
    }
}
