<?php

namespace App\Repositories;

use App\Models\Content;
use App\Models\ContentTypeField;
use App\Models\FieldSchema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Api\Core\Repositories\Repository;

class ContentRepository extends Repository
{
    public function model(): string
    {
        return Content::class;
    }

    public function create(array $data): Model
    {
        $data['status'] = $this->setStatus($data['status']);

        $content = $this->model->create($data);
        if (!empty($data['fields'])) {
            $this->updateOrCreateFields($data['fields'], $content);
        }
        if (!empty($data['terms'])) {
            $this->syncTerms($data['terms'], $content);
        }

        return $content;
    }

    public function update(array $data, $id): Model
    {
        if (!empty($data['status'])) {
            $data['status'] = $this->setStatus($data['status']);
        }
        $content = $this->resolveModel($id);
        $content->update($data);
        $data['terms'] = $data['terms'] ?? [];
        $this->syncTerms($data['terms'], $content);
        if (isset($data['fields'])) {
            $this->removeoldFields($data['fields'], $content);
            $this->updateOrCreateFields($data['fields'], $content);
        }
        $content->load('fields');

        return $content;
    }

    public function delete($id): bool
    {
        $model = $this->resolveModel($id);
        $deleted = $model->delete();
        if ($deleted) {
            $model->fields()->delete();
        }
        return $deleted;
    }

    protected function syncTerms($ids, &$content)
    {
        $content->terms()->sync($ids);
    }

    protected function updateOrCreateFields($fields, &$content)
    {
        foreach ($fields as $name => $value) {
            $type_field = ContentTypeField::whereSlug($name)->first();
            if (is_array($value)) $value = json_encode($value);
            $content->fields()->updateOrCreate(['name' => $name], ['value' => $value]);
            if ($name != 'excerpt' && $type_field->schema->name == 'textarea' &&
                (isset($type_field->options['excerpt']) && $type_field->options['excerpt'] == true)) {
                $content->fields()
                    ->updateOrCreate(['name' => 'excerpt'], ['value' => substr(strip_tags($value), 0, 160)]);
            }
        }
    }

    protected function removeoldFields($fields, $content)
    {
        $oldFields = $content->fields->pluck('name')->toArray();
        $toRemove = array_diff($oldFields, array_keys($fields));
        $content->fields()->whereIn('name', $toRemove)->delete();
    }

    protected function setStatus($status = null)
    {
        if (!$status) {
            return Content::DRAFT;
        }
        if ((int) $status === 0) {
            switch ($status) {
                case 'PUBLISHED':
                    return Content::PUBLISHED;
                case 'DRAFT':
                    return Content::DRAFT;
                case 'ARCHIVED':
                    return Content::ARCHIVED;
                default:
                    return Content::DRAFT;
            }
        }
        return $status;
    }
}
