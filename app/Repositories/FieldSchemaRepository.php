<?php

namespace App\Repositories;

use App\Models\FieldSchema;
use Api\Core\Repositories\Repository;

class FieldSchemaRepository extends Repository
{
    public function model(): string
    {
        return FieldSchema::class;
    }
}
