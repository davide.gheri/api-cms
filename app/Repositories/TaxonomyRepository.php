<?php

namespace App\Repositories;

use App\Models\Taxonomy;
use Illuminate\Database\Eloquent\Model;
use Api\Core\Repositories\Repository;

class TaxonomyRepository extends Repository
{
    public function model(): string
    {
        return Taxonomy::class;
    }

    public function create(array $data): Model
    {
        $taxonomy = $this->model->create($data);
        if (isset($data['content_types'])) {
            $this->syncContentTypes($data['content_types'], $taxonomy);
        }

        return $taxonomy;
    }

    public function update(array $data, $id): Model
    {
        $taxonomy = $this->resolveModel($id);
        $taxonomy->update($data);

        $this->syncContentTypes($data['content_types'] ?? [], $taxonomy);

        return $taxonomy;
    }

    protected function syncContentTypes($ids, &$taxonomy)
    {
        $taxonomy->contentTypes()->sync($ids);
    }
}
