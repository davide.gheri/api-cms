<?php

namespace App\Providers;

use Api\User\Providers\AuthServiceProvider;
use Api\Media\Providers\MediaServiceProvider;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
