<?php

use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('locale')->group(function () {
    Route::group(['namespace' => 'Api\V1', 'prefix' => 'v1', 'as' => 'api.'], function () {
        Route::middleware('auth:api')->group(function () {
            Route::apiResource('field-schemas', 'FieldSchemaController');
            Route::get('content-types/{content_type}/taxonomies', 'ContentTypeController@taxonomies')->name('content-types.taxonomies');
            Route::apiResource('content-types', 'ContentTypeController');
            Route::apiResource('content-types/{content_type}/fields', 'ContentTypeFieldController');
            Route::get('content-types/{content_type}/contents/last', 'ContentController@last');
            Route::post('content-types/{content_type}/contents/{content}/attachments', 'ContentController@attachment');
            Route::get('content-types/{content_type}/contents/{content}/next-prev', 'ContentController@nextPrev');
            Route::apiResource('content-types/{content_type}/contents', 'ContentController');

            Route::get('taxonomies/{taxonomy}/content-types', 'TaxonomyController@contentTypes')->name('taxonomies.content-types');
            Route::apiResource('taxonomies', 'TaxonomyController');
            Route::get('taxonomies/{taxonomy}/terms/{term}/contents', 'TermController@contents')->name('terms.contents');
            Route::apiResource('taxonomies/{taxonomy}/terms', 'TermController');

            Route::apiResource('roles', 'RoleController', ['only' => 'index']);

            Route::apiResource('menus', 'MenuController');
        });

        Route::get('lang/{lang}', function($lang) {
            $path = resource_path('lang/' . $lang);
            if (!File::exists($path)) {
                return response()->json([]);
            }
            $dict = Cache::remember('lang_' . $lang, 0, function() use ($path) {
                $files = collect(File::allFiles($path));
                $dict = $files->mapWithKeys(function(\SplFileInfo $file) {;
                    $info = pathinfo($file->getFilename());
                    return [$info['filename'] => include $file->getRealPath()];
                });
                collect($dict['common'])->each(function($trans, $key) use ($dict) {
                    $dict->put($key, $trans);
                });
                unset($dict['common']);

                return $dict;
            });
            return response()->json($dict);
        });
    });
});
