<?php

return [
    'driver' => 'uploads',

    'manipulations' => [
        'thumb' => [
            'format' => \Spatie\Image\Manipulations::FORMAT_JPG,
            'width' => 200,
            'height' => 200,
            'optimize' => null,
            'quality' => 60
        ]
    ]
];