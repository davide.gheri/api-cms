<?php

return [
    'contents' => 'Contents',
    'manage' => 'Manage Contents',

    'name' => [
        'required' => 'Name is required!',
        'label' => 'Name',
    ],
    'slug' => [
        'required' => 'Slug is required!',
        'label' => 'Slug',
    ],
    'description' => [
        'required' => 'Description is required!',
        'label' => 'Description',
    ],

    'content-type' => [
        'fields' => [
            'edit' => 'Edit Content type fields',
            'edit-field' => 'Edit field :field',
            'add-field' => 'Add new field',
            'relation' => 'Content types relation',
        ],
        'create-new' => 'Create new Content type'
    ]
];
