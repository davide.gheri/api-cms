<?php

return [
    'name' => 'Name',
    'type' => 'Type',
    'actions' => 'Actions',
    'slug' => 'Slug',
    'last-edit' => 'Last edit'
];
