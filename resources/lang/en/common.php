<?php

return [
    'create-new' => 'Create new',
    'add-new' => 'Add new',
    'delete' => 'Delete',
    'select-option' => 'Select an option...',

    'confirm' => [
        'title' => 'Confirm',
        'message' => 'Are you sure?'
    ],
];
