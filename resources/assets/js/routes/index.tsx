import * as React from 'react';
import { ConnectedRouter } from 'react-router-redux';
import { Redirect, Route, Switch } from 'react-router';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { LocaleContext } from '@core/common/contexts';
import { asyncComponent } from '@core/common/routes/asyncComponent';
import { PrivateRoute, ProtectedRoute, ScrollTop } from '@core/backend/routes';

const AsyncMain = asyncComponent(import('../components/Main'));
const AsyncLogin = asyncComponent(import('../components/Login'));
const AsyncActivate = asyncComponent(import('../components/Activate'));
const Async404 = asyncComponent(import('../components/404'));
const Async403 = asyncComponent(import('../components/403'));

export default (props: {history: any}) => {
  return (
    <ConnectedRouter history={props.history}>
      <ScrollTop>
        <Switch>
          <Route path="/login" component={AsyncLogin} />
          <Route path="/activate/:token" component={AsyncActivate}/>
          <Route path="/404" component={Async404}/>
          <Route path="/403" component={Async403}/>
          <PrivateRoute path="/" component={AsyncMain} />
        </Switch>
      </ScrollTop>
    </ConnectedRouter>
  );
};

const AsyncDashboard = asyncComponent(import('../components/Dashboard'));

const AsyncContentTypes = asyncComponent(import('../components/Content-types'));
const AsyncContentType = asyncComponent(import('../components/Content-types/Content-type'));

const AsyncTaxonomies = asyncComponent(import('../components/Taxonomies'));
const AsyncTaxonomy = asyncComponent(import('../components/Taxonomies/Taxonomy'));

const AsyncTerms = asyncComponent(import('../components/Terms'));
const AsyncContents = asyncComponent(import('../components/Contents'));
const AsyncContent = asyncComponent(import('../components/Contents/Content'));

const AsyncUsers = asyncComponent(import('../components/Users'));

const AsyncMedias = asyncComponent(import('../components/Medias'));
const AsyncMedia = asyncComponent(import('../components/Medias/Media'));

const AsyncMenus = asyncComponent(import('../components/Menus'));

export const MainRoutes = () => {
  return (
    <LocaleContext.Consumer>
      {locale => {
        const setLocalizedUrl = url => `/${locale + url}`;
        return (
          <TransitionGroup>
            <CSSTransition classNames="fade" timeout={300}>
              <Switch>
                <ProtectedRoute perm="create-contents"
                                path={setLocalizedUrl('/content-types/:id(\\d+)/contents/create')}
                                render={props => <AsyncContent {...props} isNew={true}/>}/>

                <ProtectedRoute perm={['update-contents', 'delete-contents']}
                                path={setLocalizedUrl('/content-types/:id(\\d+)/contents/:content_id(\\d+)')}
                                component={AsyncContent}/>
                <ProtectedRoute perm={['create-contents', 'update-contents', 'delete-contents']}
                                path={setLocalizedUrl('/content-types/:id(\\d+)/contents')}
                                component={AsyncContents}/>
                <ProtectedRoute perm={['update-content-types', 'delete-content-types']}
                                path={setLocalizedUrl('/content-types/:id(\\d+)')}
                                component={AsyncContentType}/>
                <ProtectedRoute perm={['create-content-types', 'update-content-types', 'delete-content-types', 'create-contents', 'update-contents', 'delete-contents']}
                                path={setLocalizedUrl('/content-types')}
                                component={AsyncContentTypes}/>
                <ProtectedRoute perm={['create-terms', 'update-terms', 'delete-terms']}
                                path={setLocalizedUrl('/taxonomies/:id(\\d+)/terms')}
                                component={AsyncTerms}/>
                <ProtectedRoute perm={['update-taxonomies', 'delete-taxonomies']}
                                path={setLocalizedUrl('/taxonomies/:id(\\d+)')}
                                component={AsyncTaxonomy}/>
                <ProtectedRoute perm={['create-taxonomies', 'update-taxonomies', 'delete-taxonomies', 'create-terms', 'update-terms', 'delete-terms']}
                                path={setLocalizedUrl('/taxonomies')}
                                component={AsyncTaxonomies}/>
                <ProtectedRoute perm={['create-users', 'update-users', 'delete-users']}
                                path={setLocalizedUrl('/users')}
                                component={AsyncUsers}/>

                <ProtectedRoute perm={['create-menus', 'update-menus', 'delete-menus']}
                                path={setLocalizedUrl('/menus')}
                                component={AsyncMenus}/>

                <Route path="/medias/:id(\d+)" component={AsyncMedia}/>
                <Route path="/medias" component={AsyncMedias}/>

                <Route path={`/${locale}`} component={AsyncDashboard}/>
                <Route path="/" render={props => <Redirect to={`/${locale}`}/>}/>
                <Route path="*" render={props => <Redirect to="/404"/>}/>
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        );
      }}
    </LocaleContext.Consumer>
  );
};
MainRoutes.displayName = 'MainRoutes';
