
export interface TaxonomiesState {
  taxonomies: TaxonomyModel[];
  loading: boolean;
  loadingSingle: {[key: number]: boolean};
  errors: {[key: number]: any};
  contentTypes: {[key:number]: any};
}

export interface TaxonomyModel {
  id?: number;
  name?: string;
  slug?: string;
  term_count?: number;
  description?: string;
}
