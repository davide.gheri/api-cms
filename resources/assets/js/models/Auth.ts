
export interface AuthState {
  user: any;
  token: string;
  tokenId: string;
  authenticated: boolean;
  loading: boolean;
  errored: boolean;
}

export default AuthState;
