import { Pagination } from './Pagination';

export interface Sizes {
  width: number;
  height: number;
  size: number;
}

export interface MediaModel {
  id: number;
  name: string;
  file_name: string;
  url: string;
  size: Sizes;
  mime_type: string;
  conversions?: {
    [key: string]: string;
  };
}

export interface MediaState {
  medias: MediaModel[];
  loading: boolean;
  errors: any;
  page: Pagination;
  progress: number;
  loadingSingle: any;
}
