
export interface ClientModel {
  id: number;
  user_id: number | null;
  name: string;
  revoked: boolean;
  password_client: boolean;
  personal_access_client: boolean;
}

export interface TokenModel {
  id: string;
  client_id: number;
  client: ClientModel,
  name: string;
  revoked: boolean;
  user_id: number;
  scopes: string[];
  expires_at: string;
}

export interface TokensState {
  tokens: TokenModel[];
  loading: boolean;
  modal: {
    show: boolean;
    content: string;
  };
}
