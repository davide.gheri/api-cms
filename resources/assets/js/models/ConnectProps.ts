import {
  ContentTypeFieldsActions,
  ContentTypesActions,
  FieldSchemasActions,
  TaxonomiesActions,
  TermsActions,
  ContentsActions,
  UsersActions,
  RolesActions,
  TokensActions,
  MediasActions,
  MenusActions,
} from '@actions';

import { AuthActions } from '@core/backend/actions/auth';

export interface ConnectProps extends
  ContentTypesActions,
  TaxonomiesActions,
  ContentTypeFieldsActions,
  FieldSchemasActions,
  TermsActions,
  ContentsActions,
  UsersActions,
  RolesActions,
  AuthActions,
  TokensActions,
  MediasActions,
  MenusActions
  {
  [key: string]: any;
  push: (url: string) => any;
}
