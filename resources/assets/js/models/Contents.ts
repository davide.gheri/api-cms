import { ContentType } from './ContentTypes';
import { TermModel } from './Term';
import { Pagination } from './Pagination';

export enum ContentStatus {
  PUBLISHED,
  DRAFT,
  ARCHIVED,
}

export interface ContentsState {
  contents: any[];
  loading: boolean;
  loadingSingle: any;
  errors: any;
  page?: Pagination;
}

export interface ContentModel {
  id: number;
  title: string;
  status: ContentStatus;
  fields: {
    [key: string]: any;
  };
  terms: TermModel[];
  content_type_id: number;
  content_type: ContentType;
}
