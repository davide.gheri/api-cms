
export interface FieldSchema {
  id?: number;
  name?: string;
  field_type?: string;
  options?: any;
  available_options?: any;
}

export interface FieldSchemasState {
  schemas: any[];
  loading: boolean;
}
