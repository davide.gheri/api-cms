import { TaxonomyModel } from './Taxonomies';

export interface TermState {
  terms: {[key: number]: TermModel[]},
  loading: boolean;
  loadingSingle: any;
  errors: any;
}

export interface TermModel {
  id?: number;
  title?: string;
  description?: string;
  content_count: number;
  taxonomy: TaxonomyModel;
}
