import { TaxonomyModel } from './Taxonomies';
import { Pagination } from './Pagination';

export interface ContentTypesState {
  contentTypes: ContentType[];
  loading: boolean;
  loadingSingle: {[key: string]: boolean};
  errors: {
    [key: number]: {[key: string]: boolean},
  };
  taxonomies: {
    [key: number]: TaxonomyModel[],
  };
  page?: Pagination;
}

export interface ContentType {
  id?: number;
  name?: string;
  description?: string;
  created_at?: string;
  updated_at?: string;
  slug?: string;
  fields?: any;
  content_count?: number;
}
