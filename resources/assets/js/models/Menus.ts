import { Pagination } from './Pagination';

export interface MenusState {
  menus: MenuModel[];
  loading: boolean;
  loadingSingle: {[key: number]: boolean};
  errors: {[key: number]: any};
  page?: Pagination;
}

export interface MenuModel {
  id?: number;
  name?: string;
  slug?: string;
  menu_items_count?: number;
  description?: string;
}
