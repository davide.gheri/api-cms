
export interface Pagination {
  current: number;
  last: number;
  perPage: number;
  total: number;
  links: {
    first: string;
    last: string;
    next: string | null;
    prev: string | null;
  };
}
