import { FieldSchema } from './FieldSchema';

export interface ContentTypeFieldsState {
  fields: {
    [key: number]: ContentTypeFieldsModel[],
  };
  loading: boolean;
  loadingSingle: any;
  errors: any;
}

export interface ContentTypeFieldsModel {
  id?: number;
  name?: string;
  field_schema_id?: number;
  slug?: string;
  options?: {
    type: string;
    [key: string]: any;
  };
  schema?: FieldSchema;
}
