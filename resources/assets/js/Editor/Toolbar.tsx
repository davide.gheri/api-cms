import * as React from 'react';
import createToolbarPlugin from 'draft-js-static-toolbar-plugin';
import createEmojiPlugin from 'draft-js-emoji-plugin';
import {
  ItalicButton,
  BoldButton,
  UnderlineButton,
  HeadlineOneButton,
  HeadlineTwoButton,
  HeadlineThreeButton,
  UnorderedListButton,
  OrderedListButton,
  BlockquoteButton,
  CodeBlockButton,
} from 'draft-js-buttons';
import { AddImageButton } from './Image';
import { Icon } from '@core/common/components';

const FullscreenButton = props => {
  const toggleFullscreen = e => {
    e.preventDefault();
    this.ref.closest('.editor-container').classList.toggle('fullscreen');
  };

  return (
    <div className="headlineButtonWrapper relative">
      <button className="headlineButton" ref={node => this.ref = node} onClick={toggleFullscreen} type="button"><Icon icon="expand-arrows"/></button>
    </div>
  );
};

export const emojiPlugin = createEmojiPlugin();
export const { EmojiSuggestions, EmojiSelect } = emojiPlugin;

export const staticToolbarPlugin = createToolbarPlugin({
  structure: [
    ItalicButton,
    BoldButton,
    UnderlineButton,
    HeadlineOneButton,
    HeadlineTwoButton,
    HeadlineThreeButton,
    UnorderedListButton,
    OrderedListButton,
    BlockquoteButton,
    CodeBlockButton,
    EmojiSelect,
    AddImageButton,
    FullscreenButton,
  ],
});

export const { Toolbar } = staticToolbarPlugin;
