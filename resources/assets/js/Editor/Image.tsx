import * as React from 'react';
import { composeDecorators } from 'draft-js-plugins-editor';
import createImagePlugin from 'draft-js-image-plugin';
import createAlignmentPlugin from 'draft-js-alignment-plugin';
import createFocusPlugin from 'draft-js-focus-plugin';
import createResizeablePlugin from 'draft-js-resizeable-plugin';
import createBlockDndPlugin from 'draft-js-drag-n-drop-plugin';
import { Button, FormGroup, Icon, TextInput } from '@core/common/components';
import * as classNames from 'classnames';
import MediasModal from '@components/Medias/MediasModal';

export const focusPlugin = createFocusPlugin();
export const resizeablePlugin = createResizeablePlugin();
export const blockDndPlugin = createBlockDndPlugin();
export const alignmentPlugin = createAlignmentPlugin();
const decorator = composeDecorators(
  resizeablePlugin.decorator,
  alignmentPlugin.decorator,
  focusPlugin.decorator,
  blockDndPlugin.decorator,
);
export const imagePlugin = createImagePlugin({decorator});

export const { AlignmentTool } = alignmentPlugin;

export class AddImageButton extends React.Component<any, any> {
  state = {
    popover: false,
    modal: false,
    url: '',
  };

  onMouseDown = e => e.preventDefault();

  togglePopover = () => {
    this.setState({popover: !this.state.popover});
  };

  addImage = () => {
    const { getEditorState, setEditorState } = this.props;
    setEditorState(imagePlugin.addImage(getEditorState(), this.state.url));
    this.setState({popover: false, url: ''});
  };

  changeUrl = e => {
    this.setState({ url: e.target.value });
  };

  toggleMediaModal = () => {
    this.setState({modal: !this.state.modal});
  };

  render() {
    const { popover, url, modal } = this.state;
    const popoverClasses = classNames('addImagePopover bg-white');
    return (
      <div className="headlineButtonWrapper relative">
        <button type="button" onClick={this.togglePopover} className="headlineButton"><Icon icon="image"/></button>
          {popover &&
            <div className={popoverClasses}>
              <FormGroup>
               <TextInput
                 placeholder="Paste the image url" onChange={this.changeUrl} value={url}
                 append={() => <Button onClick={this.toggleMediaModal}><Icon icon="search"/></Button>}
               />
              </FormGroup>
              <Button type="button" onClick={this.addImage}>Add</Button>
            </div>
          }
          {modal && <MediasModal onChoose={url => this.setState({url})} onClose={this.toggleMediaModal}/>}
      </div>
    );
  }
}
