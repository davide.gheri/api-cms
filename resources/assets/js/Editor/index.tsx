import * as React from 'react';
import DraftEditor, { createEditorStateWithText } from 'draft-js-plugins-editor';
import { Toolbar, staticToolbarPlugin, emojiPlugin, EmojiSuggestions } from './Toolbar';
import { InlineToolbar, inlineToolbarPlugin } from './InlineToolbar';
import { AlignmentTool, imagePlugin, focusPlugin, blockDndPlugin, alignmentPlugin, resizeablePlugin } from './Image';

const plugins = [
  staticToolbarPlugin,
  inlineToolbarPlugin,
  emojiPlugin,
  imagePlugin,
  focusPlugin,
  blockDndPlugin,
  alignmentPlugin,
  resizeablePlugin,
];

export class Editor extends React.Component<any, any> {
  editor: any;
  constructor(props: any) {
    super(props);
    this.state = {
      editorState: createEditorStateWithText(props.content || ''),
    };
  }

  onChange = editorState => {
    this.setState({
      editorState,
    });
  };

  focus = () => {
    this.editor.focus();
  };

  render() {
    return (
      <div className="editor-container">
        <div className="static-toolbar"><Toolbar /></div>
          <div className="editor" onClick={this.focus}>
            <DraftEditor
              editorState={this.state.editorState}
              onChange={this.onChange}
              plugins={plugins}
              ref={element => { this.editor = element; }}
            />
            <InlineToolbar />
            <EmojiSuggestions/>
            <AlignmentTool/>
          </div>
        </div>
    );
  }
}

export default Editor;
