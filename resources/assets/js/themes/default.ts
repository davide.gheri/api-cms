import { themes, Themes } from '@core/common/contexts';

const dark = Object.assign(themes.dark, {
  errorBoundary: 'bg-grey-darkest text-white',
});

const teal = Object.assign(themes.teal, {
  errorBoundary: 'bg-white',
});

const blue = Object.assign(themes.blue, {
  errorBoundary: 'bg-white',
});

export default {
  dark,
  teal,
  blue,
} as Themes;
