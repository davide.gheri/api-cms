import { TaxonomiesState, Action } from '@models';
import { Reducer } from '@core/backend/reducers/reducer';

const initialState: TaxonomiesState = {
  taxonomies: [],
  loading: false,
  loadingSingle: {},
  errors: {},
  contentTypes: {},
};

class TaxonomiesReducer extends Reducer<TaxonomiesState> {
  setTaxLoading = payload => this.setLoading(payload);

  setTaxSingleLoading = payload => this.setSingleLoading(payload);

  getTaxSuccess = payload => this.setEntities(payload, 'taxonomies');

  taxCreated = payload => this.setCreatedEntity(payload, 'taxonomies');

  deleteTaxSuccess = payload => this.deleteEntity(payload, 'taxonomies');

  updateTaxSuccess = payload => this.setUpdatedEntity(payload, 'taxonomies');

  updateTaxFailure = payload => this.setErrors(payload);

  getTaxCtsSuccess = payload => this.setNestedEntities(payload, 'contentTypes');
}

const reducer = new TaxonomiesReducer();

export default (state = initialState, action: Action): TaxonomiesState => {
  return reducer.handle(state, action);
};
