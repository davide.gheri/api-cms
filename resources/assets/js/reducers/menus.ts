import * as types from '../constants';
import { MenusState, MenuModel, Action } from '@models';
import pagination, { setPagination } from '@core/backend/reducers/pagination';

const initialState: MenusState = {
  menus: [],
  loading: false,
  loadingSingle: {},
  errors: {},
  page: pagination,
};

export default (state = initialState, action: Action): MenusState => {
  let newMenus;
  switch (action.type) {
    case types.SET_MENUS_LOADING:
      return Object.assign({}, state, {loading: action.payload});

    case types.SET_MENUS_SINGLE_LOADING:
      const newLoadingSingle = Object.assign({}, state.loadingSingle);
      newLoadingSingle[action.payload.id] = action.payload.status;
      return Object.assign({}, state, {loadingSingle: newLoadingSingle});

    case types.GET_MENUS_SUCCESS:
      newMenus = action.payload.data;
      const page = setPagination(action.payload);
      return Object.assign({}, state, {page, menus: newMenus, errors: {}});

    case types.MENU_CREATED:
      newMenus = state.menus.slice();
      newMenus.push(action.payload);
      return Object.assign({}, state, {menus: newMenus, errors: {}});

    case types.DELETE_MENU_SUCCESS:
      newMenus = state.menus.slice().filter((menu: MenuModel) => {
        return menu.id !== action.payload;
      });
      return Object.assign({}, state, {menus: newMenus});

    case types.UPDATE_MENU_SUCCESS:
      newMenus = state.menus.slice();
      const index = newMenus.findIndex(menu => menu.id === action.payload.id);
      newMenus[index] = action.payload;
      return Object.assign({}, state, {menus: newMenus});

    case types.UPDATE_MENU_FAILURE:
      const errors = Object.assign({}, state.errors);
      errors[action.payload.id] = action.payload.errors;
      return Object.assign({}, state, {errors});

    default:
      return state;
  }
};
