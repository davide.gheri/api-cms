import { combineReducers } from 'redux';
import contentTypes from './content-types';
import contentTypeFields from './content-type-fields';
import fieldSchemas from './field-schemas';
import taxonomies from './taxonomies';
import terms from './terms';
import contents from './contents';
import tokens from './tokens';
import medias from './medias';
import menus from './menus';
import settings from './settings';
import {
  ContentsState,
  ContentTypeFieldsState,
  ContentTypesState,
  FieldSchemasState,
  MediaState,
  MenusState,
  TaxonomiesState,
  TermState,
  TokensState,
} from '@models';

import { AuthState, UsersState, RolesState } from '@core/backend/models';
import coreReducers from '@core/backend/reducers';

export default combineReducers(Object.assign({
  contentTypes,
  contentTypeFields,
  fieldSchemas,
  taxonomies,
  terms,
  contents,
  tokens,
  medias,
  menus,
  settings,
}, coreReducers));

export interface State {
  auth: AuthState;
  contentTypes: ContentTypesState;
  contentTypeFields: ContentTypeFieldsState;
  fieldSchemas: FieldSchemasState;
  taxonomies: TaxonomiesState;
  terms: TermState;
  contents: ContentsState;
  users: UsersState;
  roles: RolesState;
  tokens: TokensState;
  medias: MediaState;
  menus: MenusState;
  settings: any; // TODO
}
