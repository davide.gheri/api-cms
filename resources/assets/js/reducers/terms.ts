import { TermState, Action } from '@models';
import { Reducer } from '@core/backend/reducers/reducer';

const initialState: TermState = {
  terms: {},
  loading: false,
  loadingSingle: {},
  errors: {},
};

class Termsreducer extends Reducer<TermState> {
  setTermsLoading = payload => this.setLoading(payload);

  setTermSingleLoading = payload => this.setNestedSingleLoading(payload, 'termId');

  getTermsSuccess = payload => this.setNestedEntities(payload, 'terms');

  termCreated = payload => this.setNestedCreatedEntity(payload, 'terms', 'term');

  updateTermSuccess = payload => this.updateNestedEntity(payload, 'terms', 'term');

  deleteTermSuccess = payload => this.deleteNestedEntity(payload, 'terms', 'termId');

  updateTermFailure = payload => this.setErrors(payload);
}

const reducer = new Termsreducer();

export default (state = initialState, action: Action): TermState => {
  return reducer.handle(state, action);
};
