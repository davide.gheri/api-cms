import { ContentTypeFieldsState, Action } from '@models';
import { Reducer } from '@core/backend/reducers/reducer';

const initialState: ContentTypeFieldsState = {
  fields: {},
  loading: false,
  loadingSingle: {},
  errors: {},
};

class ContentTypeFieldsReducer extends Reducer<ContentTypeFieldsState> {
  setCtfLoading = (payload): ContentTypeFieldsState => this.setLoading(payload);

  setCtfSingleLoading = (payload): ContentTypeFieldsState => {
    return this.setNestedSingleLoading(payload, 'fieldId');
  };

  getCtfSuccess = (payload): ContentTypeFieldsState => {
    return this.setNestedEntities(payload, 'fields');
  };

  createCtfSuccess = (payload): ContentTypeFieldsState => {
    return this.setNestedCreatedEntity(payload, 'fields', 'field');
  };

  updateCtfSuccess = (payload): ContentTypeFieldsState => {
    return this.updateNestedEntity(payload, 'fields', 'field');
  };

  deleteCtfSuccess = (payload): ContentTypeFieldsState => {
    return this.deleteNestedEntity(payload, 'fields', 'fieldId');
  };

  updateCtfFailure = (payload): ContentTypeFieldsState => {
    return this.setNestedErrors(payload, 'fieldId');
  }
}

const reducer = new ContentTypeFieldsReducer();

export default (state = initialState, action: Action): ContentTypeFieldsState => {
  return reducer.handle(state, action);
};
