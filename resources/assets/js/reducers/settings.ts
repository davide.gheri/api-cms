import { Action } from '@models';
import { Reducer } from '@core/backend/reducers/reducer';

const initialState: any = {

};

class SettingsReducer extends Reducer<any> {

}

const reducer = new SettingsReducer();

export default (state = initialState, action: Action): any => {
  return reducer.handle(state, action);
};
