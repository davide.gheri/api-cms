import { TokensState, Action } from '@models';
import { Reducer } from '@core/backend/reducers/reducer';

const initialState: TokensState = {
  tokens: [],
  loading: false,
  modal: {
    show: false,
    content: null,
  },
};

class Tokensreducer extends Reducer<TokensState> {
  setTokensLoading = payload => this.setLoading(payload);

  getTokensSuccess = payload => this.setEntities(payload, 'tokens');

  tokenCreated = payload => this.setCreatedEntity(payload, 'tokens');

  deleteTokenSuccess = payload => this.deleteEntity(payload, 'tokens');

  toggleAccessTokenModal = payload => {
    return this.updateState({
      modal: {
        show: payload.show,
        content: payload.accessToken || null,
      },
    });
  }
}

const reducer = new Tokensreducer();

export default (state = initialState, action: Action): TokensState => {
  return reducer.handle(state, action);
};
