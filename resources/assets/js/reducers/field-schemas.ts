import { FieldSchemasState, Action } from '@models';
import { Reducer } from '@core/backend/reducers/reducer';

const initialState: FieldSchemasState = {
  schemas: [],
  loading: false,
};

class FieldSchemasReducer extends Reducer<FieldSchemasState> {
  setFsLoading = payload => this.setLoading(payload);

  getFieldSchemaSuccess = payload => this.setEntities(payload, 'schemas');
}

const reducer = new FieldSchemasReducer();

export default (state = initialState, action: Action): FieldSchemasState => {
  return reducer.handle(state, action);
};
