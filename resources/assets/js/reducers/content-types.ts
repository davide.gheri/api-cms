import { ContentTypesState, Action } from '@models';
import pagination from '@core/backend/reducers/pagination';
import { Reducer } from '@core/backend/reducers/reducer';

const initialState: ContentTypesState = {
  contentTypes: [],
  loading: false,
  loadingSingle: {},
  errors: {},
  taxonomies: {},
  page: pagination,
};

class ContentTypesReducer extends Reducer<ContentTypesState> {
  setCtLoading = payload => this.setLoading(payload);

  setCtSingleLoading = payload => this.setSingleLoading(payload);

  getContentTypesSuccess = payload => this.setEntitiesWithPagination(payload, 'contentTypes');

  getContentTypeSuccess = payload => this.setEntityWithPagination(payload, 'contentTypes');

  contentTypeCreated = payload => this.setCreatedEntity(payload, 'contentTypes');

  updateContentTypeSuccess = payload => this.setUpdatedEntity(payload, 'contentTypes');

  deleteContentTypeSuccess = payload => this.deleteEntity(payload, 'contentTypes');

  updateContentTypefailure = payload => this.setErrors(payload);

  getCtTaxonomiesSucces = payload => this.setNestedEntities(payload, 'taxonomies');
}

const reducer = new ContentTypesReducer();

export default (state = initialState, action: Action): ContentTypesState => {
  return reducer.handle(state, action);
};
