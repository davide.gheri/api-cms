import { MediaState, Action } from '@models';
import pagination from '@core/backend/reducers/pagination';
import { Reducer } from '@core/backend/reducers/reducer';

const initialState: MediaState = {
  medias: [],
  loading: false,
  errors: {},
  page: pagination,
  progress: 0,
  loadingSingle: {},
};

class MediasReducer extends Reducer<MediaState> {
  setMediasProgress = payload => this.updateState({progress: payload});

  setMediasLoading = payload => this.setLoading(payload);

  setMediasSingleLoading = payload => this.setSingleLoading(payload);

  getMediasSuccess = payload => this.setEntitiesWithPagination(payload, 'medias');

  getMediaSuccess = payload => this.setEntityWithPagination(payload, 'medias');

  mediasCreated = payload => this.setCreatedEntity(payload, 'medias');

  deleteMediasSuccess = payload => this.deleteEntity(payload, 'medias');

  updateMediasSuccess = payload => this.setUpdatedEntity(payload, 'medias');
}

const reducer = new MediasReducer();

export default (state = initialState, action: Action): MediaState => {
  return reducer.handle(state, action);
};
