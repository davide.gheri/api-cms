import { ContentsState, Action } from '@models';
import pagination, { setPagination } from '@core/backend/reducers/pagination';
import { Reducer } from '@core/backend/reducers/reducer';

const initialState: ContentsState = {
  contents: [],
  loading: false,
  loadingSingle: {},
  errors: {},
  page: pagination,
};

class ContentsReducer extends Reducer<ContentsState> {
  setContentLoading = payload => this.setLoading(payload);

  setContentSingleLoading = payload => this.setNestedSingleLoading(payload, 'contentId');

  getContentsSuccess = payload => {
    const newContents = Object.assign({}, this.getState().contents);
    newContents[payload.id] = payload.contents.data;
    const page = setPagination(payload.contents);
    return this.updateState({
      page,
      contents: newContents,
      errors: {},
    });
  };

  getContentSuccess = payload => {
    const newContents = Object.assign({}, this.getState().contents);
    newContents[payload.id] = [payload.content];
    return this.updateState({
      contents: newContents,
      page: pagination,
      errors: {},
    });
  };

  contentCreated = payload => this.setNestedCreatedEntity(payload, 'contents', 'content');

  updateContentSuccess = payload => this.updateNestedEntity(payload, 'contents', 'content');

  deleteContentSuccess = payload => this.deleteNestedEntity(payload, 'contents', 'contentId');

  updateContentFailure = payload => {
    const newErrors = Object.assign({}, this.getState().errors);
    newErrors[payload.contentId] = payload.errors;

    return this.updateState({errors: newErrors});
  }
}

const reducer = new ContentsReducer();

export default (state = initialState, action: Action): ContentsState => {
  return reducer.handle(state, action);
};
