import { Dispatch } from 'redux';
import * as types from '@constants';
import Api from '@core/common/api';
import Url from '@urls';
import { handleErrors, notify } from '@core/backend/actions/common';
import { TokenModel } from '@models';
import { Action } from '@core/backend/models';
import { ToastType } from 'react-toastify';

export interface TokensActions {
  getTokens: () => (dispatch: Dispatch) => void;
  createToken: (data: {name: string, scopes: string[]}) => (dispatch: Dispatch) => void;
  deleteToken: (id: string) => (dispatch: Dispatch) => void;
  toggleModal: (show: boolean, accessToken?: string) => Action;
}

const setLoading = (bool: boolean): Action => ({
  type: types.SET_TOKENS_LOADING,
  payload: bool,
});

const setloadingSingle = (id: string, status: boolean): Action => ({
  type: types.SET_TOKEN_SINGLE_LOADING,
  payload: {id, status},
});

const onGetSucces = (tokens: TokenModel): Action => ({
  type: types.GET_TOKENS_SUCCESS,
  payload: tokens,
});

const onCreateSuccess = (token: TokenModel): Action => ({
  type: types.TOKEN_CREATED,
  payload: token,
});

const onDeleteSuccess = (id: string): Action => ({
  type: types.DELETE_TOKEN_SUCCESS,
  payload: id,
});

export const toggleModal = (show: boolean, accessToken?: string): Action => ({
  type: types.TOGGLE_ACCESS_TOKEN_MODAL,
  payload: {show, accessToken},
});

export const getTokens = () => dispatcth => {
  dispatcth(setLoading(true));
  Api.get(Url.tokens.index)
    .then(res => dispatcth(onGetSucces(res)))
    .catch(err => handleErrors(err))
    .then(() => dispatcth(setLoading(false)));
};

export const createToken = (data: {name: string, scopes: string[]}) => dispatch => {
  dispatch(setLoading(true));
  Api.post(Url.tokens.store, data)
    .then(res => {
      notify('Token created', ToastType.SUCCESS);
      dispatch(toggleModal(true, res.accessToken));
      dispatch(onCreateSuccess(res.token));
    })
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const deleteToken = (id: string) => dispatch => {
  dispatch(setloadingSingle(id, true));
  Api.delete([Url.tokens.delete, {id}])
    .then(res => {
      notify('Token deleted', ToastType.SUCCESS);
      dispatch(onDeleteSuccess(id));
    })
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setloadingSingle(id, false)));
};
