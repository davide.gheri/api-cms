import { Dispatch } from 'redux';
import * as types from '@constants';
import Api from '@core/common/api';
import Url from '@urls';
import { handleErrors, notify, push } from '@core/backend/actions/common';
import { ContentType, TaxonomyModel } from '@models';
import { Action } from '@core/backend/models';
import { ToastType } from 'react-toastify';

export interface TaxonomiesActions {
  getTaxonomies: () => (dispatch: Dispatch) => void;
  getTaxonomy: (id: number) => (dispatch: Dispatch) => void;
  createTaxonomy: (data: TaxonomyModel) => (dispatch: Dispatch) => void;
  updateTaxonomy: (id: number, data: TaxonomyModel) => (dispatch: Dispatch) => void;
  deleteTaxonomy: (id: number) => (dispatch: Dispatch) => void;
}

const setLoading = (bool: boolean): Action => ({
  type: types.SET_TAX_LOADING,
  payload: bool,
});

const setloadingSingle = (id: number, status: boolean): Action => ({
  type: types.SET_TAX_SINGLE_LOADING,
  payload: {id, status},
});

const onGetSUccess = (res: TaxonomyModel[]): Action => ({
  type: types.GET_TAX_SUCCESS,
  payload: res,
});

const onCreateSuccess = (taxonomy: TaxonomyModel): Action => ({
  type: types.TAX_CREATED,
  payload: taxonomy,
});

const onUpdateSuccess = (taxonomy: TaxonomyModel): Action => ({
  type: types.UPDATE_TAX_SUCCESS,
  payload: taxonomy,
});

const onDeleteSuccess = (id: number): Action => ({
  type: types.DELETE_TAX_SUCCESS,
  payload: id,
});

const onUpdateFailure = (errors: any, id: any): Action => ({
  type: types.UPDATE_TAX_FAILURE,
  payload: {errors, id},
});

const onGetContentTypesSuccess = (id: number, contentTypes: ContentType[]): Action => ({
  type: types.GET_TAX_CTS_SUCCESS,
  payload: {id, contentTypes},
});

export const getTaxonomies = () => dispatch => {
  dispatch(setLoading(true));
  Api.get(Url.taxonomies.index)
    .then(res => dispatch(onGetSUccess(res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const getTaxonomy = (id: number) => dispatch => {
  dispatch(setLoading(true));
  Api.get([Url.taxonomies.show, {id}])
    .then(res => dispatch(onGetSUccess([res])))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const createTaxonomy = (data: TaxonomyModel) => dispatch => {
  dispatch(setLoading(true));
  Api.post(Url.taxonomies.store, data)
    .then(res => {
      dispatch(onCreateSuccess(res));
      dispatch(push('/taxonomies'));
      notify('Taxonomy created', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(err.data.errors, 0));
    })
    .then(() => dispatch(setLoading(false)));
};

export const updateTaxonomy = (id: number, data: TaxonomyModel) => dispatch => {
  dispatch(setLoading(true));
  Api.patch([Url.taxonomies.update, {id}], data)
    .then(res => {
      dispatch(onUpdateSuccess(res));
      notify('Taxonomy updated', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(err.data.errors, id));
    })
    .then(() => dispatch(setLoading(false)));
};

export const deleteTaxonomy = (id: number) => dispatch => {
  dispatch(setloadingSingle(id, true));
  Api.delete([Url.taxonomies.delete, {id}])
    .then(res => {
      dispatch(onDeleteSuccess(id));
      notify('Taxonomy deleted', ToastType.SUCCESS);
      push('/taxonomies');
    })
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setloadingSingle(id, false)));
};

export const getTaxonomyContentTypes = (id: number) => dispatch => {
  dispatch(setloadingSingle(id, true));
  Api.get([Url.taxonomies.contentTypes, {id}])
    .then(res => dispatch(onGetContentTypesSuccess(id, res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setloadingSingle(id, false)));
};
