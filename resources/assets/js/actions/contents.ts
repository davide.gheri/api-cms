import { Dispatch } from 'redux';
import * as types from '@constants';
import Api from '@core/common/api';
import Url from '@urls';
import { handleErrors, notify, push } from '@core/backend/actions/common';
import { ContentModel } from '@models';
import { Action } from '@core/backend/models';
import { ToastType } from 'react-toastify';

export interface ContentsActions {
  getContents: (id: number, page?: number) => (dispatch: Dispatch) => void;
  getContent: (id: number, contentId: number) => (dispatch: Dispatch) => void;
  createContent: (id: number, data: ContentModel) => (dispatch: Dispatch) => void;
  updateContent: (id: number, contentId: number, data: ContentModel) => (dispatch: Dispatch) => void;
  deleteContent: (id: number, contentId: number) => (dispatch: Dispatch) => void;
}

const setLoading = (bool: boolean): Action => ({
  type: types.SET_CONTENT_LOADING,
  payload: bool,
});

const setloadingSingle = (id: number, contentId: number, status: boolean): Action => ({
  type: types.SET_CONTENT_SINGLE_LOADING,
  payload: {id, contentId, status},
});

const onGetSuccess = (id: number, contents: ContentModel[]): Action => ({
  type: types.GET_CONTENTS_SUCCESS,
  payload: {id, contents},
});

const onGetSingleSuccess = (id: number, content: ContentModel): Action => ({
  type: types.GET_CONTENT_SUCCESS,
  payload: {id, content},
});

const onCreateSuccess = (id: number, content: ContentModel): Action => ({
  type: types.CONTENT_CREATED,
  payload: {id, content},
});

const onUpdateSuccess = (id: number, contentId: number, content: ContentModel): Action => ({
  type: types.UPDATE_CONTENT_SUCCESS,
  payload: {id, contentId, content},
});

const onDeleteSuccess = (id: number, contentId: number): Action => ({
  type: types.DELETE_CONTENT_SUCCESS,
  payload: {id, contentId},
});

const onUpdateFailure = (id: number, contentId: number, errors: any): Action => ({
  type: types.UPDATE_CONTENT_FAILURE,
  payload: {id, contentId, errors},
});

export const getContents = (id: number, page: number = 1) => dispatch => {
  dispatch(setLoading(true));
  dispatch(setloadingSingle(id, 0, true));
  Api.get([`${Url.contents.index}?page=${page}`, {id}])
    .then(res => dispatch(onGetSuccess(id, res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => {
      dispatch(setLoading(false));
      dispatch(setloadingSingle(id, 0, false));
    });
};

export const getContent = (id: number, contentId: number) => dispatch => {
  Api.get([Url.contents.show, {id, contentId}])
    .then(res => dispatch(onGetSingleSuccess(id, res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const createContent = (id: number, data: ContentModel) => dispatch => {
  dispatch(setLoading(true));
  Api.post([Url.contents.store, {id}], data)
    .then(res => {
      dispatch(onCreateSuccess(id, res));
      dispatch(push(`/content-types/${id}/contents`));
      notify('Content created', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(id, 0, err.data.errors));
    })
    .then(() => dispatch(setLoading(false)));
};

export const updateContent = (id: number, contentId: number, data: ContentModel) => dispatch => {
  dispatch(setLoading(true));
  Api.patch([Url.contents.update, {id, contentId}], data)
    .then(res => {
      dispatch(onUpdateSuccess(id, contentId, res));
      notify('Content updated', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(id, contentId, err.data.errors));
    })
    .then(() => dispatch(setLoading(false)));
};

export const deleteContent = (id: number, contentId: number) => dispatch => {
  dispatch(setloadingSingle(id, contentId, true));
  Api.delete([Url.contents.delete, {id, contentId}])
    .then(res => {
      dispatch(onDeleteSuccess(id, contentId));
      dispatch(push(`/content-types/${id}/contents`));
      notify('Content deleted', ToastType.SUCCESS);
    })
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setloadingSingle(id, contentId, false)));
};
