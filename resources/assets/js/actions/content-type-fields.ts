import { Dispatch } from 'redux';
import * as types from '@constants';
import Api from '@core/common/api';
import Url from '@urls';
import { ContentTypeFieldsModel } from '@models';
import { Action } from '@core/backend/models';
import { handleErrors, notify, push } from '@core/backend/actions/common';
import { ToastType } from 'react-toastify';

export interface ContentTypeFieldsActions {
  getContentTypeFields: (id: number) => (dispatch: Dispatch) => void;
  createContentTypeField: (id: number, data: ContentTypeFieldsModel) => (dispatch: Dispatch) => void;
  updateContentTypeField: (id: number, firldId: number, data: ContentTypeFieldsModel) => (dispatch: Dispatch) => void;
  deleteContentTypeField: (id: number, fieldId: number) => (dispatch: Dispatch) => void;
}

const setLoading = (bool: boolean): Action => ({
  type: types.SET_CTF_LOADING,
  payload: bool,
});

const setLoadingSingle = (id: number, fieldId: number, status: boolean): Action => ({
  type: types.SET_CTF_SINGLE_LOADING,
  payload: {id, fieldId, status},
});

const onGetSuccess = (id: number, fields: ContentTypeFieldsModel[]): Action => ({
  type: types.GET_CTF_SUCCESS,
  payload: {id, fields},
});

const onCreateSuccess = (id: number, field: ContentTypeFieldsModel): Action => ({
  type: types.CREATE_CTF_SUCCESS,
  payload: {id, field},
});

const onUpdateSuccess = (id: number, fieldId: number, field: ContentTypeFieldsModel): Action => ({
  type: types.UPDATE_CTF_SUCCESS,
  payload: {id, fieldId, field},
});

const onDeleteSuccess = (id: number, fieldId: number): Action => ({
  type: types.DELETE_CTF_SUCCESS,
  payload: {id, fieldId},
});

const onUpdateFailure = (id: number, fieldId: number, errors: any): Action => ({
  type: types.UPDATE_CTF_FAILURE,
  payload: {id, fieldId, errors},
});

export const getContentTypeFields = (id: number) => dispatch => {
  dispatch(setLoading(true));
  Api.get([Url.contentTypeFields.index, {id}])
    .then(res => dispatch(onGetSuccess(id, res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const createContentTypeField = (id: number, data: ContentTypeFieldsModel) => dispatch => {
  dispatch(setLoading(true));
  Api.post([Url.contentTypeFields.store, {id}], data)
    .then(res => {
      dispatch(onCreateSuccess(id, res));
      dispatch(push(`/content-types/${id}`));
      notify('Field created', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(id, 0, err.data.errors));
    })
    .then(() => dispatch(setLoading(false)));
};

export const updateContentTypeField = (id: number, fieldId: number, data: ContentTypeFieldsModel) => dispatch => {
  dispatch(setLoading(true));
  Api.patch([Url.contentTypeFields.update, {id, fieldId}], data)
    .then(res => {
      dispatch(onUpdateSuccess(id, fieldId, res));
      dispatch(push(`/content-types/${id}`));
      notify('Field updated', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(id, fieldId, err.data.errors));
    })
    .then(() => dispatch(setLoading(false)));
};

export const deleteContentTypeField = (id: number, fieldId: number) => dispatch => {
  dispatch(setLoadingSingle(id, fieldId, true));
  Api.delete([Url.contentTypeFields.delete, {id, fieldId}])
    .then(res => {
      dispatch(onDeleteSuccess(id, fieldId));
      notify('Field deleted', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      notify('Cannot delete field, retry', ToastType.SUCCESS);
    })
    .then(() => setLoadingSingle(id, fieldId, false));
};
