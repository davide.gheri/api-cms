import { Dispatch } from 'redux';
import * as types from '@constants';
import Api from '@core/common/api';
import Url from '@urls';
import { ContentType, TaxonomyModel } from '@models';
import { Action } from '@core/backend/models';
import { handleErrors, notify, push } from '@core/backend/actions/common';
import { ToastType } from 'react-toastify';

export interface ContentTypesActions {
  getContentTypes: (page?: number) => (dispatch: Dispatch) => void;
  getContentType: (id: number) => (dispatch: Dispatch) => void;
  updateContentType: (id: number, data: ContentType) => (dispatch: Dispatch) => void;
  createContentType: (data: ContentType) => (dispatch: Dispatch) => void;
  deleteContentType: (id: number) => (dispatch: Dispatch) => void;
  getContentTypeTaxonomies: (id: number) => (dispatch: Dispatch) => void;
}

const setLoading = (bool: boolean): Action => ({
  type: types.SET_CT_LOADING,
  payload: bool,
});

const setloadingSingle = (id: number, status: boolean): Action => ({
  type: types.SET_CT_SINGLE_LOADING,
  payload: {id, status},
});

const onGetSuccess = (contentTypes: ContentType[]) => ({
  type: types.GET_CONTENT_TYPES_SUCCESS,
  payload: contentTypes,
});

const onGetSingleSuccess = (contentType: ContentType) => ({
  type: types.GET_CONTENT_TYPE_SUCCESS,
  payload: contentType,
});

const onCreateSuccess = (contentType: ContentType) => ({
  type: types.CONTENT_TYPE_CREATED,
  payload: contentType,
});

const onUpdateSuccess = (contentType: ContentType) => ({
  type: types.UPDATE_CONTENT_TYPE_SUCCESS,
  payload: contentType,
});

const onDeleteSuccess = (id: number): Action => ({
  type: types.DELETE_CONTENT_TYPE_SUCCESS,
  payload: id,
});

const onUpdateFailure = (errors: any, id: any): Action => ({
  type: types.UPDATE_CONTENT_TYPE_FAILURE,
  payload: {errors, id},
});

const onGetTaxonomiesSucces = (id: number, taxonomies: TaxonomyModel[]): Action => ({
  type: types.GET_CT_TAXONOMIES_SUCCESS,
  payload: {id, taxonomies},
});

export const getContentTypes = (page: number = 1) => dispatch => {
  dispatch(setLoading(true));
  Api.get(`${Url.contentTypes.index}?page=${page}`)
    .then(res => dispatch(onGetSuccess(res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const getContentType = (id: number | string) => dispatch => {
  dispatch(setLoading(true));
  Api.get([Url.contentTypes.show, {id}])
    .then(res => dispatch(onGetSingleSuccess(res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const updateContentType = (id: number, data: ContentType) => dispatch => {
  dispatch(setLoading(true));
  Api.patch([Url.contentTypes.update, {id}], data)
    .then(res => {
      dispatch(onUpdateSuccess(res));
      notify('Content type updated', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(err.data.errors, id));
    })
    .then(() => dispatch(setLoading(false)));
};

export const createContentType = (data: ContentType) => dispatch => {
  dispatch(setLoading(true));
  Api.post(Url.contentTypes.store, data)
    .then(res => {
      dispatch(onCreateSuccess(res));
      dispatch(push('/content-types'));
      notify('Content type created', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(err.data.errors, 0));
    })
    .then(() => dispatch(setLoading(false)));
};

export const deleteContentType = (id: number) => dispatch => {
  dispatch(setloadingSingle(id, true));
  Api.delete([Url.contentTypes.delete, {id}])
    .then(res => {
      dispatch(onDeleteSuccess(id));
      dispatch(push('/content-types'));
      notify('Content type deleted', ToastType.SUCCESS);
    })
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setloadingSingle(id, false)));
};

export const getContentTypeTaxonomies = (id: number) => dispatch => {
  dispatch(setloadingSingle(id, true));
  Api.get([Url.contentTypes.taxonomies, {id}])
    .then(res => dispatch(onGetTaxonomiesSucces(id, res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setloadingSingle(id, false)));
};
