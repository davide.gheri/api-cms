import { Dispatch } from 'redux';
import { push } from 'react-router-redux';
import * as types from '@constants';
import Api from '@core/common/api';
import Url from '@urls';
import { handleErrors, notify } from '@core/backend/actions/common';
import { MediaModel } from '@models';
import { Action } from '@core/backend/models';
import { ToastType } from 'react-toastify';

export interface MediasActions {
  getMedias: (page?: number) => (dispatch: Dispatch) => void;
  getMedia: (id: number) => (dispatch: Dispatch) => void;
  createMedia: (data: FormData) => (dispatch: Dispatch) => void;
  updateMedia: (id: number, data: FormData) => (dispatch: Dispatch) => void;
  deleteMedia: (id: number) => (dispatch: Dispatch) => void;
}

const setLoading = (bool: boolean): Action => ({
  type: types.SET_MEDIAS_LOADING,
  payload: bool,
});

const setloadingSingle = (id: number, status: boolean): Action => ({
  type: types.SET_MEDIAS_SINGLE_LOADING,
  payload: {id, status},
});

const setProgress = (percent: number): Action => ({
  type: types.SET_MEDIAS_PROGRESS,
  payload: percent,
});

const onGetSuccess = (medias: any) => ({
  type: types.GET_MEDIAS_SUCCESS,
  payload: medias,
});

const onGetSingleSuccess = (media: MediaModel) => ({
  type: types.GET_MEDIA_SUCCESS,
  payload: media,
});

const onCreateSuccess = (media: MediaModel) => ({
  type: types.MEDIAS_CREATED,
  payload: media,
});

const onUpdateSuccess = (id: number, media: MediaModel): Action => ({
  type: types.UPDATE_MEDIAS_SUCCESS,
  payload: {id, media},
});

const onUpdateFailure = (errors: any, id: any): Action => ({
  type: types.UPDATE_MEDIAS_FAILURE,
  payload: {errors, id},
});

const onDeleteSuccess = (id: number): Action => ({
  type: types.DELETE_MEDIAS_SUCCESS,
  payload: id,
});

export const getMedias = (page: number = 1) => dispatch => {
  dispatch(setLoading(true));
  Api.get(`${Url.medias.index}?page=${page}`)
    .then(res => dispatch(onGetSuccess(res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const getMedia = (id: number | string) => dispatch => {
  dispatch(setLoading(true));
  Api.get([Url.medias.show, {id}])
    .then(res => dispatch(onGetSingleSuccess(res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const createMedia = (data: FormData) => dispatch => {
  dispatch(setLoading(true));
  const config = {
    onUploadProgress: progressEvent => {
      const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
      dispatch(setProgress(percentCompleted));
    },
  };
  Api.post(Url.medias.store, data, config)
    .then(res => {
      dispatch(onCreateSuccess(res));
      dispatch(push('/medias'));
      notify('Media uploaded', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(err.data.errors, 0));
    })
    .then(() => dispatch(setLoading(false)));
};

export const updateMedia = (id: number, data: FormData) => dispatch => {
  data.append('_method', 'PATCH');
  dispatch(setLoading(true));
  Api.post([Url.medias.update, {id}], data)
    .then(res => {
      dispatch(onUpdateSuccess(id, res));
      dispatch(push('/medias'));
      notify('Media updated', ToastType.SUCCESS);
    })
    .catch(err => {
      console.log(err);
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(err.data.errors, id));
    })
    .then(() => dispatch(setLoading(false)));
};

export const deleteMedia = (id: number) => dispatch => {
  dispatch(setloadingSingle(id, true));
  Api.delete([Url.medias.delete, {id}])
    .then(res => {
      dispatch(onDeleteSuccess(id));
      dispatch(push('/medias'));
      notify('Media deleted', ToastType.SUCCESS);
    })
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setloadingSingle(id, false)));
};
