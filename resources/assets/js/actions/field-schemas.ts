import { Dispatch } from 'redux';
import * as types from '@constants';
import Api from '@core/common/api';
import Url from '@urls';
import { Action } from '@core/backend/models';
import { handleErrors } from '@core/backend/actions/common';

export interface FieldSchemasActions {
  getFieldSchemas: () => (dispatch: Dispatch) => void;
}

const setLoading = (bool: boolean): Action => ({
  type: types.SET_FS_LOADING,
  payload: bool,
});

const onGetSuccess = (schemas): Action => ({
  type: types.GET_FIELD_SCHEMAS_SUCCESS,
  payload: schemas,
});

export const getFieldSchemas = () => dispatch => {
  setLoading(true);
  Api.get(Url.fieldSchemas.index)
    .then(res => dispatch(onGetSuccess(res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};
