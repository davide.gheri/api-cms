import { Dispatch } from 'redux';
import * as types from '@constants';
import Api from '@core/common/api';
import Url from '@urls';
import { MenuModel } from '@models';
import { Action } from '@core/backend/models';
import { handleErrors, notify, push } from '@core/backend/actions/common';
import { ToastType } from 'react-toastify';

export interface MenusActions {
  getMenus: () => (dispatch: Dispatch) => void;
  getMenu: (id: number) => (dispatch: Dispatch) => void;
  createMenu: (data: MenuModel) => (dispatch: Dispatch) => void;
  updateMenu: (id: number, data: MenuModel) => (dispatch: Dispatch) => void;
  deleteMenu: (id: number) => (dispatch: Dispatch) => void;
}

const setLoading = (bool: boolean): Action => ({
  type: types.SET_MENUS_LOADING,
  payload: bool,
});

const setloadingSingle = (id: number, status: boolean): Action => ({
  type: types.SET_MENUS_SINGLE_LOADING,
  payload: {id, status},
});

const onGetSUccess = (res: MenuModel[]): Action => ({
  type: types.GET_MENUS_SUCCESS,
  payload: res,
});

const onGetSingleSuccess = (content: MenuModel): Action => ({
  type: types.GET_MENU_SUCCESS,
  payload: content,
});

const onCreateSuccess = (taxonomy: MenuModel): Action => ({
  type: types.MENU_CREATED,
  payload: taxonomy,
});

const onUpdateSuccess = (taxonomy: MenuModel): Action => ({
  type: types.UPDATE_MENU_SUCCESS,
  payload: taxonomy,
});

const onDeleteSuccess = (id: number): Action => ({
  type: types.DELETE_MENU_SUCCESS,
  payload: id,
});

const onUpdateFailure = (errors: any, id: any): Action => ({
  type: types.UPDATE_MENU_FAILURE,
  payload: {errors, id},
});

export const getMenus = (page: number = 1) => dispatch => {
  dispatch(setLoading(true));
  Api.get(`${Url.menus.index}?page=${page}`)
      .then(res => dispatch(onGetSUccess(res)))
      .catch(err => dispatch(handleErrors(err)))
      .then(() => dispatch(setLoading(false)));
};

export const getMenu = (id: number) => dispatch => {
  dispatch(setLoading(true));
  Api.get([Url.menus.show, {id}])
      .then(res => dispatch(onGetSingleSuccess(res)))
      .catch(err => dispatch(handleErrors(err)))
      .then(() => dispatch(setLoading(false)));
};

export const createMenu = (data: MenuModel) => dispatch => {
  dispatch(setLoading(true));
  Api.post(Url.menus.store, data)
    .then(res => {
      dispatch(onCreateSuccess(res));
      dispatch(push('/menus'));
      notify('Menu created', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(err.data.errors, 0));
    })
    .then(() => dispatch(setLoading(false)));
};

export const updateMenu = (id: number, data: MenuModel) => dispatch => {
  dispatch(setLoading(true));
  Api.patch([Url.menus.update, {id}], data)
    .then(res => {
      dispatch(onUpdateSuccess(res));
      notify('Menu updated', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(err.data.errors, id));
    })
    .then(() => dispatch(setLoading(false)));
};

export const deleteMenu = (id: number) => dispatch => {
  dispatch(setloadingSingle(id, true));
  Api.delete([Url.menus.delete, {id}])
    .then(res => {
      dispatch(onDeleteSuccess(id));
      notify('Menu deleted', ToastType.SUCCESS);
      push('/menus');
    })
  .catch(err => dispatch(handleErrors(err)))
  .then(() => dispatch(setloadingSingle(id, false)));
};
