import { Dispatch } from 'redux';
import * as types from '@constants';
import Api from '@core/common/api';
import Url from '@urls';
import { TermModel } from '@models';
import { handleErrors, notify, push } from '@core/backend/actions/common';
import { Action } from '@core/backend/models';
import { ToastType } from 'react-toastify';

export interface TermsActions {
  getTerms: (id: number) => (dispatch: Dispatch) => void;
  createTerm: (id: number, data: TermModel) => (dispatch: Dispatch) => void;
  updateTerm: (id: number, termId: number, data: TermModel) => (dispatch: Dispatch) => void;
  deleteterm: (id: number, termId: number) => (dispatch: Dispatch) => void;
}

const setLoading = (bool: boolean): Action => ({
  type: types.SET_TERMS_LOADING,
  payload: bool,
});

const setloadingSingle = (id: number, termId: number, status: boolean): Action => ({
  type: types.SET_TERM_SINGLE_LOADING,
  payload: {id, termId, status},
});

const onGetSuccess = (id: number, terms: TermModel[]): Action => ({
  type: types.GET_TERMS_SUCCESS,
  payload: {id, terms},
});

const onCreateSuccess = (id: number, term: TermModel): Action => ({
  type: types.TERM_CREATED,
  payload: {id, term},
});

const onUpdateSuccess = (id: number, termId: number, term: TermModel): Action => ({
  type: types.UPDATE_TERM_SUCCESS,
  payload: {id, termId, term},
});

const onDeleteSuccess = (id: number, termId: number): Action => ({
  type: types.DELETE_TERM_SUCCESS,
  payload: {id, termId},
});

const onUpdateFailure = (id: number, termId: number, errors: any): Action => ({
  type: types.UPDATE_TERM_FAILURE,
  payload: {id, termId, errors},
});

export const getTerms = (id: number) => dispatch => {
  dispatch(setLoading(true));
  Api.get([Url.terms.index, {id}])
    .then(res => dispatch(onGetSuccess(id, res)))
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setLoading(false)));
};

export const createTerm = (id: number, data: TermModel) => dispatch => {
  dispatch(setLoading(true));
  Api.post([Url.terms.store, {id}], data)
    .then(res => {
      dispatch(onCreateSuccess(id, res));
      dispatch(push(`/taxonomies/${id}/terms`));
      notify('Term created', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(id, 0, err.data.errors));
    })
    .then(() => dispatch(setLoading(false)));
};

export const updateTerm = (id: number, termId: number, data: TermModel) => dispatch => {
  dispatch(setLoading(true));
  Api.patch([Url.terms.update, {id, termId}], data)
    .then(res => {
      dispatch(onUpdateSuccess(id, termId, res));
      notify('Term updated', ToastType.SUCCESS);
    })
    .catch(err => {
      dispatch(handleErrors(err));
      dispatch(onUpdateFailure(id, termId, err.data.errors));
    })
    .then(() => dispatch(setLoading(false)));
};

export const deleteTerm = (id: number, termId: number) => dispatch => {
  dispatch(setloadingSingle(id, termId, true));
  Api.delete([Url.terms.delete, {id, termId}])
    .then(res => {
      dispatch(onDeleteSuccess(id, termId));
      dispatch(push(`/taxonomies/${id}/terms`));
      notify('Term deleted', ToastType.SUCCESS);
    })
    .catch(err => dispatch(handleErrors(err)))
    .then(() => dispatch(setloadingSingle(id, termId, false)));
};
