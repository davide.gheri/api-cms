import ICore from '@core/backend';
import ILogger from 'js-logger';
import * as ReactDOM from 'react-dom';
import * as React from 'react';
import App from './components/App';
import bootstrap from './bootstrap';

declare global {
  interface Function {
    displayName: string;
  }
  interface Window {
    [key: string]: any;
  }
  const Core: typeof ICore;
  const Logger: typeof ILogger;
}

bootstrap().then((conf: any) => {
  if (document.getElementById('root')) {
    ReactDOM.render(<App/>, document.getElementById('root'));
  } else {
    document.write('root component not found!');
    console.error('root component not found!');
  }
});
