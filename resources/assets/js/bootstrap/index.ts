import { bootstrap } from '@core/common/bootstrap';
import Api, { AxiosClient } from '@core/common/api';
import Url, { apiUrl } from '@urls';
import I18n from '@core/backend/utils/I18n';
import * as Sentry from '@sentry/browser';
import themes from '@themes/default';
import Core from '@core/backend';

async function setLocale() {
  // const lang = I18n.getBrowserLocale() || 'en';
  const lang = 'en';
  Logger.log(lang);
  const dict = await Api.get([Url.dictionaries, {lang}]);
  I18n.setDict(dict);
  I18n.keyAsDefault = true;
}

export default () => {
  return new Promise((res, rej) => {
    bootstrap({}).then(core => {
      Sentry.init({
        dsn: process.env.MIX_SENTRY_DSN,
      });
      const apiInstance = Api.init(new AxiosClient());
      Core.init({
        themes,
        apiInstance,
        del: Url.placeholderDelimeter,
        apiUrlFunc: apiUrl,
        attachmentUpload: Url.contents.attachment,
      });
      setLocale().then(_ => res());
    });
  });
};
