import * as React from 'react';
import SweetAlert from 'sweetalert2-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as ActionCreators from '@actions';
import { EnhancedTable, PageHeader } from '@core/backend/components';
import { Button, Icon } from '@core/common/components';
import { TaxonomyModel } from '@models';
import { can } from '@core/backend/utils';
import { asyncComponent } from '@core/common/routes/asyncComponent';
import { LocalizedRoute } from '@core/backend/routes';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state: any) => ({
  taxonomies: state.taxonomies,
  user: state.auth.user,
});
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);
const AsyncCreateTaxonomyModal = asyncComponent(import('./CreateModal'));

class Taxonomies extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      swalShow: false,
    };
  }

  componentDidMount() {
    this.props.getTaxonomies();
  }

  onDelete = () => {
    const id = this.state.swalShow;
    this.setState({swalShow: false});
    this.props.deleteTaxonomy(id);
  };

  render() {
    const { loading, taxonomies, loadingSingle } = this.props.taxonomies;
    const { user } = this.props;
    return (
      <div>
        <PageHeader title="Taxonomies">
          {can(user, 'create-taxonomies') &&
            <Button onClick={() => this.props.push('/taxonomies/create')} className="btn-teal shadow"><Icon icon="plus"/> Create new</Button>}
        </PageHeader>
        <EnhancedTable
          className="shadow"
          data={taxonomies}
          columns={Object.assign({
            name: 'Name',
            slug: 'Slug',
            updated_at: 'Last update',
            term_count: 'Terms',
          }, can(user, 'delete-taxonomies') ? {actions: 'Actions'} : {})}
          linkedCols={{
            name: can(user, 'update-taxonomies') ? '/taxonomies/{id}' : '/taxonomies/{id}/terms',
            term_count: '/taxonomies/{id}/terms',
          }}
          customCols={{actions: (el: TaxonomyModel) => <Button onClick={() => this.setState({swalShow: el.id})} className="btn-red btn-sm"><Icon icon="trash"/></Button>}}
          rowLoading={(el: TaxonomyModel) => loadingSingle[el.id] === true}
          loading={loading}/>
        {can(user, 'create-taxonomies') &&
          <LocalizedRoute path="/taxonomies/create" render={(props: any) => <AsyncCreateTaxonomyModal {...props}/>}/>}
        <SweetAlert show={Boolean(this.state.swalShow)}
                    title="Confirm"
                    text="Are you sure?"
                    showCancelButton={true}
                    onConfirm={this.onDelete}/>
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Taxonomies));
