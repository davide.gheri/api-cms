import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';
import * as ActionCreators from '@actions';
import { ConnectProps, TaxonomyModel } from '@models';
import {
    Form, FormGroup, Textarea, TextInput,
    Button, Icon, Card,
} from '@core/common/components';
import { PageHeader } from '@core/backend/components';
import { slugify } from '@core/common/utils';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state: any, ownState: any) => {
  let taxonomy = {};
  let notFound = false;
  const { id } = ownState.match.params;
  if (id) {
    taxonomy = state.taxonomies.taxonomies.find(ct => ct.id === +id) || {};
  } else {
    notFound = true;
  }
  return {
    taxonomy, notFound,
    loading: state.taxonomies.loading,
    loadingSingle: state.taxonomies.loadingSingle[id] || false,
    requestErrors: state.taxonomies.errors[id] || {},
    taxContentTypes: state.taxonomies.contentTypes[id] || [],
    contentTypes: state.contentTypes,
  };
};

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface TaxonomyProps extends ConnectProps {
  isNew: boolean;
  taxonomy: TaxonomyModel;
  notFound: boolean;
  loading: boolean;
  loadingSingle: boolean;
}

export interface TaxonomyState {
  taxonomy: TaxonomyModel;
  contentTypes_id: number[];
  errors: {[key: string]: boolean};
  valid: boolean;
  swalShow?: boolean;
  content_types_edited: boolean;
}

class Taxonomy extends React.Component<TaxonomyProps, TaxonomyState> {
  constructor(props: TaxonomyProps) {
    super(props);
    this.state = {
      taxonomy: {},
      contentTypes_id: [],
      errors: {},
      valid: false,
      swalShow: false,
      content_types_edited: false,
    };
  }

  componentDidMount() {
    const { isNew, taxonomy, notFound } = this.props;
    const { id } = this.props.match.params;
    if (notFound) {
      this.props.push('/404');
    }
    if (!isNew && !Object.keys(taxonomy).length) {
      this.props.getTaxonomy(id);
    }
    this.props.getTaxonomyContentTypes(id);
    this.props.getContentTypes();
  }

  static getDerivedStateFromProps(newProps: TaxonomyProps, state: TaxonomyState) {
    state.taxonomy = newProps.taxonomy;
    if (!state.content_types_edited) {
      state.contentTypes_id = newProps.taxContentTypes.map(ct => ct.id);
    }
    state.valid = Object.values(newProps.taxonomy).length > 0;
    return state;
  }

  onSubmit = () => {
    const { taxonomy, contentTypes_id } = this.state;
    this.props.updateTaxonomy(taxonomy.id, Object.assign(taxonomy, {content_types: contentTypes_id}));
  };

  onDelete = () => {
    this.setState({swalShow: false});
    const { id } = this.props.match.params;
    this.props.deleteTaxonomy(id);
  };

  verifyValue = (name: string) => {
    const toVerify = ['name', 'slug'];
    if (toVerify.includes(name)) {
      const { errors } = this.state;
      errors[name] = !Boolean(this.state.taxonomy[name]);
      this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
    }
  };

  onChange = e => {
    const { taxonomy } = this.state;
    const { name, value } = e.target;
    if (name === 'slug') {
      taxonomy[name] = slugify(value);
    } else {
      taxonomy[name] = value;
    }
    this.setState({taxonomy}, () => this.verifyValue(name));
  };

  onAutoSlug = () => {
    const { taxonomy } = this.state;
    taxonomy.slug = slugify(taxonomy.name);
    this.setState({taxonomy}, () => this.verifyValue('slug'));
  };

  onCheckboxChange = e => {
    const { checked, value } = e.target;
    let contentTypes_id = this.state.contentTypes_id.slice();
    if (checked) {
      contentTypes_id.push(+value);
    } else {
      contentTypes_id = contentTypes_id.filter(id => id !== +value);
    }
    this.setState({contentTypes_id, content_types_edited: true});
  };

  render() {
    const { taxonomy, errors, valid, contentTypes_id } = this.state;
    const { loading, loadingSingle, requestErrors, contentTypes } = this.props;

    return (
      <div>
        <Form onSubmit={this.onSubmit} disabled={loading || loadingSingle}>
          <PageHeader title="Edit taxonomy">
            <Link to={`/taxonomies/${taxonomy.id}/terms`} className="btn btn-teal shadow">Manage Terms</Link>
          </PageHeader>
          <Card>
            <FormGroup label="Name">
              <TextInput type="text" name="name"
                         value={taxonomy.name || ''}
                         onChange={this.onChange}
                         errored={errors.name || requestErrors.name || false}
                         required/>
              {errors.name && <span className="help help-error">Name is required!</span>}
              {requestErrors.name && <span className="help help-error">{requestErrors.name}</span>}
            </FormGroup>
            <FormGroup label="Slug">
              <TextInput type="text"
                         name="slug"
                         value={taxonomy.slug || ''}
                         errored={errors.slug || requestErrors.slug || false}
                         onChange={this.onChange}
                         append={() => <Button disabled={errors.name} onClick={this.onAutoSlug} text="Auto" type="button"/>}
                         required/>
              {errors.slug && <span className="help help-error">Slug is required!</span>}
              {requestErrors.slug && <span className="help help-error">{requestErrors.slug}</span>}
            </FormGroup>
            <FormGroup label="Description">
            <Textarea name="description"
                      value={taxonomy.description || ''}
                      errored={errors.description || requestErrors.description || false}
                      onChange={this.onChange}/>
              {errors.description && <span className="help help-error">Description is required!</span>}
              {requestErrors.description && <span className="help help-error">{requestErrors.description}</span>}
            </FormGroup>
          </Card>
          <Card className="mt-4">
            <PageHeader title="Associate Content types" heading="h3" backButton={false}/>
            <div className="flex">
              {contentTypes.contentTypes.map(ct => (
                <FormGroup key={ct.id} inline={true} label={ct.name} className="flex-1">
                  <TextInput type="checkbox" className="checkbox" value={ct.id} onChange={this.onCheckboxChange} checked={contentTypes_id.includes(ct.id)}/>
                </FormGroup>
              ))}
            </div>
          </Card>
          <div className="pt-4 flex">
            <Button className="btn-teal shadow" disabled={!valid || loading || loadingSingle} type="submit">
              {loading ? <Icon icon="spinner" spin/> : 'Save'}
            </Button>
            <Button disabled={loading || loadingSingle} className="btn-red ml-auto shadow" type="button" onClick={() => this.setState({swalShow: true})}>
              {loadingSingle ? <Icon icon="spinner" spin/> : <span><Icon icon="trash"/> Delete</span>}
            </Button>
          </div>
          <SweetAlert show={this.state.swalShow}
                      title="Confirm"
                      text="Are you sure?"
                      showCancelButton={true}
                      onConfirm={this.onDelete}/>
        </Form>
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Taxonomy));
