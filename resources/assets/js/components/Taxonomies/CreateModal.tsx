import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    Modal, ModalContent, ModalTitle,
    Form, FormGroup, Textarea, TextInput,
    Button, Icon,
} from '@core/common/components';
import { slugify } from '@core/common/utils';
import * as ActionCreators from '@actions';

const mapStateToProps = (state: any) => ({
  loading: state.taxonomies.loading,
  requestErrors: state.taxonomies.errors['0'] || {},
});
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class CreateModal extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      taxonomy: {
        name: '',
        slug: '',
        description: '',
      },
      errors: {},
      valid: false,
    };
  }

  onSubmit = () => {
    const { taxonomy } = this.state;
    this.props.createTaxonomy(taxonomy);
  };

  verifyValue = (name: string) => {
    const toVerify = ['name', 'slug'];
    if (toVerify.includes(name)) {
      const { errors } = this.state;
      errors[name] = !Boolean(this.state.taxonomy[name]);
      this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
    }
  };

  onChange = e => {
    const { taxonomy } = this.state;
    const { name, value } = e.target;
    if (name === 'slug') {
      taxonomy[name] = slugify(value);
    } else {
      taxonomy[name] = value;
    }
    this.setState({taxonomy}, () => this.verifyValue(name));
  };

  onAutoSlug = () => {
    const { taxonomy } = this.state;
    taxonomy.slug = slugify(taxonomy.name);
    this.setState({taxonomy}, () => this.verifyValue('slug'));
  };

  render() {
    const { taxonomy, errors, valid } = this.state;
    const { loading, push, requestErrors } = this.props;
    return (
      <Modal className="sm:w-1/2" onClose={() => push('/taxonomies')}>
        <ModalTitle title="Create new Taxonomy"/>
        <ModalContent>
          <Form onSubmit={this.onSubmit}>
            <FormGroup label="Name">
              <TextInput type="text" name="name"
                         value={taxonomy.name || ''}
                         onChange={this.onChange}
                         errored={errors.name || requestErrors.name || false}
                         required/>
              {errors.name && <span className="help help-error">Name is required!</span>}
              {requestErrors.name && <span className="help help-error">{requestErrors.name}</span>}
            </FormGroup>
            <FormGroup label="Slug">
              <TextInput type="text"
                         name="slug"
                         value={taxonomy.slug || ''}
                         errored={errors.slug || requestErrors.slug || false}
                         onChange={this.onChange}
                         append={() => <Button disabled={errors.name} onClick={this.onAutoSlug} text="Auto" type="button"/>}
                         required/>
              {errors.slug && <span className="help help-error">Slug is required!</span>}
              {requestErrors.slug && <span className="help help-error">{requestErrors.slug}</span>}
            </FormGroup>
            <FormGroup label="Description">
            <Textarea name="description"
                      value={taxonomy.description || ''}
                      errored={errors.description || requestErrors.description || false}
                      onChange={this.onChange}/>
              {errors.description && <span className="help help-error">Description is required!</span>}
              {requestErrors.description && <span className="help help-error">{requestErrors.description}</span>}
            </FormGroup>
            <div className="pt-4 flex">
              <Button className="btn-teal shadow" disabled={!valid || loading} type="submit">
                {loading ? <Icon icon="spinner" spin/> : 'Save'}
              </Button>
            </div>
          </Form>
        </ModalContent>
      </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateModal);
