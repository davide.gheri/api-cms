import * as React from 'react';
import { Modal, ModalContent } from '@core/common/components';
import Medias from './index';
import { MediaModel } from '@models';

export interface Props {
  onChoose: (media: any) => void;
  onClose: () => void;
}

export default class MediasModal extends React.Component<Props, any> {
  onMediaChoose = (media: MediaModel) => {
    this.props.onChoose(media);
    this.props.onClose();
  };

  render() {
    return (
      <Modal onClose={this.props.onClose} className="w-5/6 h-3/4">
        <ModalContent>
          <Medias isModal={true} onMediaChoose={this.onMediaChoose}/>
        </ModalContent>
      </Modal>
    );
  }
}
