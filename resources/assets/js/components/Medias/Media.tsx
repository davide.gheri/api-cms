import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import SweetAlert from 'sweetalert2-react';
import * as classNames from 'classnames';
import * as ActionCreators from '@actions';
import { Card, Form, FormGroup, TextInput, Button, Icon } from '@core/common/components';
import { PageHeader } from '@core/backend/components';
import { ConnectProps, MediaModel } from '@models';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state, ownState) => {
  const { id } = ownState.match.params;
  return {
    loading: state.medias.loading,
    singleLoading: state.medias.loadingSingle[id] || false,
    media: state.medias.medias.find(media => media.id === +id) || {},
  };
};

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface MediaProps extends ConnectProps {
  loading: boolean;
  singleLoading: boolean;
  media: MediaModel;
}

interface MediaState {
  media: MediaModel | {};
  cropper: {
    aspectRatio: number | null;
  };
  cropped: any;
  manipulations: {
    [key: string]: any;
  };
  swalShow: boolean;
  isConversion: boolean;
}

class Media extends React.Component<MediaProps, MediaState> {
  state = {
    media: {},
    cropper: {
      aspectRatio: null,
    },
    cropped: null,
    manipulations: {},
    swalShow: false,
    isConversion: false,
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getMedia(id);
  }

  static getDerivedStateFromProps(props: MediaProps, state) {
    if (Object.keys(state.media).length === 0) {
      state.media = props.media;
    }
    return state;
  }

  onChange = e => {
    const { name, value } = e.target;
    const { media } = this.state;
    media[name] = value;
    this.setState({media});
  };

  onSubmit = () => {
    const { name, id } = this.state.media as MediaModel;
    const { isConversion } = this.state;
    const cropper = this.refs.cropper as any;
    cropper.getCroppedCanvas().toBlob(blob => {
      const data = new FormData();
      data.append('media', blob);
      data.append('name', name);
      if (isConversion) {
        data.append('isConversion', 'true');
      }
      this.props.updateMedia(id, data);
    });
  };

  submitAsConversion = () => {
    this.setState({isConversion: true}, () => this.onSubmit());
  };

  onDelete = () => {
    this.setState({swalShow: false});
    const { id } = this.props.match.params;
    this.props.deleteMedia(id);
  };

  crop = () => {
  };

  transform = (method, value?) => {
    const { cropper } = this.refs;
    let manipulations = this.state.manipulations;
    if (typeof cropper[method] === 'function') {
      cropper[method](value);
      manipulations[method] = value;
      if (method === 'reset') {
        manipulations = {};
      }
      this.setState({manipulations});
    }
  };

  setCropState = (name: string, value: any) => {
    const { cropper } = this.state;
    cropper[name] = value;
    this.setState({cropper});
  };

  render() {
    const media = this.state.media as MediaModel;
    const { cropper, manipulations } = this.state;
    const { loading, singleLoading } = this.props;
    const aspectRatios = {
      None: null,
      '1/1': 1 / 1,
      '4/3': 4 / 3,
      '16/9': 16 / 9,
    };
    return (
      <div>
        <PageHeader title="Edit Media"/>
        <Card className="ml-0">
          <Form onSubmit={this.onSubmit}>
            <FormGroup label="Name">
              <TextInput value={media.name || ''} onChange={this.onChange} name="name"/>
            </FormGroup>
            <Cropper
              ref="cropper"
              src={media.url}
              style={{width: '100%'}}
              {...cropper}
              guides={true}
              autoCrop={false}
              crop={this.crop} />
            <div className="flex flex-wrap pt-4 pb-4">
              <div>
                <legend className="pb-2">Aspect ratio</legend>
                {Object.keys(aspectRatios).map((l , k) => (
                  <Button className={classNames('mr-2', cropper.aspectRatio === aspectRatios[l] ? 'btn-teal' : '')}
                          key={k} onClick={() => this.setCropState('aspectRatio', aspectRatios[l])}>{l}</Button>
                  ),
                )}
              </div>
              <div className="ml-auto">
                <legend className="pb-2 ml-2">Manipulations</legend>
                <Button className="ml-2" onClick={() => this.transform('scaleX', manipulations['scaleX'] === -1 ? 1 : -1)}><Icon icon="arrows-h"/> Flip</Button>
                <Button className="ml-2" onClick={() => this.transform('reset')}>Reset</Button>
              </div>
            </div>
            <div className="flex w-full flex-wrap border-t pt-4">
              <Button disabled={loading} className="btn-teal" type="submit">
                {loading ? <Icon icon="spinner" spin/> : 'Save'}
              </Button>
              <Button disabled={loading} className="btn-teal ml-2" onClick={this.submitAsConversion}>
                {loading ? <Icon icon="spinner" spin/> : 'Save as a conversion'}
              </Button>
              <Button disabled={loading || singleLoading} className="btn-red ml-auto" onClick={() => this.setState({swalShow: true})}>
                {singleLoading ? <Icon icon="spinner" spin/> : <span><Icon icon="trash"/> Delete</span>}
              </Button>
            </div>
          </Form>
        </Card>
        <SweetAlert show={this.state.swalShow}
                    title="Confirm"
                    text="Are you sure?"
                    showCancelButton={true}
                    onConfirm={this.onDelete}/>
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Media));
