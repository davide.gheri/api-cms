import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as classNames from 'classnames';
import { Route } from 'react-router';
import { Link } from 'react-router-dom';
import * as ActionCreators from '@actions';
import { ConnectProps, MediaModel } from '@models';
import { asyncComponent } from '@core/common/routes/asyncComponent';
import { Icon, Card } from '@core/common/components';
import { PageHeader } from '@core/backend/components';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state, ownState) => ({
  medias: state.medias.medias,
  loading: state.medias.loading,
  errors: state.medias.errors,
  isModal: ownState.isModal || false,
  onMediaChoose: ownState.onMediaChoose || undefined,
});

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface MediasProps extends ConnectProps {
  medias: MediaModel[];
  loading: boolean;
  errors: any;
  isModal?: boolean;
  onMediaChoose?: (url: string) => void;
}

const AsyncMediaCreateModal = asyncComponent(import('./MediaCreateModal'));

class Medias extends React.Component<MediasProps, any> {

  componentDidMount() {
    this.props.getMedias();
  }

  onThumbClick = (url: string) => {
    const { isModal } = this.props;
    if (isModal) {
      this.props.onMediaChoose(url);
    }
  };

  renderFullLoading = () => {
    return (
      <div>Loading...</div>
    );
  };

  renderThumb = (media: MediaModel) => {
    const { isModal } = this.props;
    let image = media.url;
    if (media.conversions && media.conversions.thumb) {
      image = media.conversions.thumb;
    }
    const classes = classNames(isModal ? 'cursor-pointer' : '');
    return (
      <div className="img-thumb">
        <img src={image} className={classes}/>
        {isModal &&
        <ul>
          <li className="cursor-pointer" onClick={() => this.onThumbClick(media.url)}>Original</li>
          {Object.keys(media.conversions).map((name, k) => (
            <li className="cursor-pointer" key={k} onClick={() => this.onThumbClick(media.conversions[name])}>{name}</li>
          ))}
        </ul>
        }
      </div>
    );
  };

  renderInfo = (media: MediaModel) => {
    const { isModal } = this.props;
    if (!isModal) {
      return <span className="card-title"><Link to={`/medias/${media.id}`}>{media.name}</Link></span>;
    }
    return (
      <div className="flex">
        <span className="card-title">{media.name}</span>
        <Link className="ml-auto" to={`/medias/${media.id}`}><Icon icon="edit"/></Link>
      </div>
    );
  };

  render() {
    const { medias, loading, isModal } = this.props;
    if (loading && medias.length === 0) {
      return this.renderFullLoading();
    }
    const containerClass = classNames('flex', 'w-full', 'flex-wrap');
    const itemClass = classNames('w-1/2', 'sm:w-1/2', 'md-w-1/3', 'lg:w-1/4', 'p-2');
    return (
      <div>
        <PageHeader title="Media" backButton={!isModal}>
          <Link className="btn btn-teal" to="/medias/create"><Icon icon="plus"/> Create new</Link>
        </PageHeader>
        <section className={containerClass}>

          {medias.map(media => (
            <div className={itemClass} key={media.id}>
              <Card className="image">
                <div className="card-header">
                  {this.renderThumb(media)}
                </div>
                <div className="card-body">
                  {this.renderInfo(media)}
                </div>
              </Card>
            </div>
          ))}
          <Route path="/medias/create" component={AsyncMediaCreateModal}/>
        </section>
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Medias));
