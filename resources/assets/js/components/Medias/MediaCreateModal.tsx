import * as React from 'react';
import Dropzone from 'react-dropzone';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as ActionCreators from '@actions';
import { ConnectProps } from '@models';
import {
    Modal, ModalContent, ModalTitle,
    FormGroup, TextInput,
    Button, Icon,
} from '@core/common/components';

const mapStateToProps = state => ({
  progress: state.medias.progress,
  uploading: state.medias.loading,
});

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class MediaCreateModal extends React.Component<ConnectProps, any> {
  state = {
    file: null,
    name: '',
  };

  onDrop = files => {
    const name = files[0].name.split('.').slice(0, -1).join('.');
    this.setState({name, file: files[0]});
  };

  onChange = e => {
    const { name, value } = e.target;
    this.setState({[name]: value});
  };

  removeFile = () => {
    this.setState({name: '', file: null});
  };

  onSubmit = () => {
    const { file, name } = this.state;
    const data = new FormData();
    data.append('media', file);
    data.append('name', name);
    this.props.createMedia(data);
  };

  renderFullDropzone = () =>  (
    <Dropzone multiple={false}
              onDrop={this.onDrop}
              className="w-full border h-full flex items-center justify-center">
      <p>Drop here</p>
    </Dropzone>
  );

  renderPreview = () => {
    const { file, name } = this.state;
    const { uploading } = this.props;
    const image = new Image();
    image.src = file.preview;
    return (
      <div className="w-full h-full">
        <div style={{height: 'calc(100% - 150px)'}} className="flex items-center w-full justify-center relative">
          <img src={file.preview} className="max-w-full max-h-full"/>
          <Button onClick={this.removeFile} className="absolute pin-t pin-r btn-red" style={{borderRadius: '100%', padding: 0, width: 30, height: 30}}><Icon icon="times"/></Button>
        </div>
        <div style={{height: 150}}>
          <FormGroup label="Name">
            <TextInput value={name} name="name" onChange={this.onChange}/>
          </FormGroup>
          <p>Original File name: <b>{file.name}</b></p>
          <p>Size: <b>{file.size}b</b></p>
          <div className="flex w-full mt-4">
            <Button className="btn-red" onClick={this.removeFile}>Cancel</Button>
            <Button className="btn-teal ml-auto" onClick={this.onSubmit} disabled={uploading}>
              {uploading ? <Icon icon="spinner" spin/> : 'Upload'}
            </Button>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { file } = this.state;
    return (
      <Modal onClose={() => this.props.routerPush('medias')} className="w-3/4 h-3/4">
        <ModalTitle title="Upload a new Media"/>
        <ModalContent style={{height: 'calc(100% - 18px)'}}>
          {!file ? this.renderFullDropzone() : this.renderPreview()}
        </ModalContent>
      </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MediaCreateModal);
