import * as React from 'react';
import { Provider } from 'react-redux';
import Routes from '@routes';
import Auth from '@core/backend/utils/Auth';
import { onLoginSuccess } from '@core/backend/actions/auth';
import { store, history } from '@store';
import { Storage } from '@core/common/utils/Storage';
import { ThemeContext, themes } from '@core/common/contexts';
import withErrorBoundary from '@components/withErrorBoundary';

class App extends React.Component<any, {theme: any, toggle: (theme: string) => void}> {
  constructor(props: any) {
    super(props);
    const theme = Storage.get('theme', 'teal');
    this.state = {
      theme: themes[theme] || themes.teal,
      toggle: this.onThemeUpdate,
    };
  }

  onThemeUpdate = (theme: string) => {
    if (Object.keys(themes).includes(theme)) {
      document.body.classList.remove(this.state.theme.body);
      this.setState({theme: themes[theme]}, () => {
        Storage.set('theme', theme);
        document.body.classList.add(this.state.theme.body);
      });
    }
  };

  async componentWillMount() {
    if (Auth.authenticated()) {
      store.dispatch(onLoginSuccess(Auth.token));
    }
  }

  componentDidMount() {
    document.body.classList.add(this.state.theme.body);
  }

  render() {
    return (
      <div className="main-wrapper">
        <ThemeContext.Provider value={this.state}>
          <Provider store={store}>
            <Routes history={history}/>
          </Provider>
        </ThemeContext.Provider>
      </div>
    );
  }
}

export default withErrorBoundary(App);
