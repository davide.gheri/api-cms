import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  FileUpload,
} from '@core/backend/components';
import { FormGroup, Option, SelectInput, TextInput } from '@core/common/components';
import { ContentType, ContentModel, ConnectProps, ContentTypeFieldsModel, FieldSchema } from '@models';
import * as ActionCreators from '@actions';

import DraftEditor from '../../Editor';

//
// const findCtIds = (schemas: ContentTypeFieldsModel[]) => {
//   return schemas.filter(schema => {
//     return schema.schema.name == 'ct_relation'
//   }).reduce((arr, schema) => {
//     schema.options.relations.forEach(r => arr.push(r));
//     return arr;
//   }, []);
// };
//
// const mapStateToProps = (state: any, ownState: any) => {
//   const relationsIds = findCtIds(ownState.schema);
//   const relations = relationsIds.reduce((obj, id) => {
//     obj[id] = state.contents.contents[id] || [];
//     return obj;
//   }, {});
//   const relationsLoading = relationsIds.reduce((obj, id) => {
//     obj[id] = state.contents.loadingSingle[id] ? state.contents.loadingSingle[id][0] : false;
//     return obj;
//   }, {});
//   return {
//     relations,
//     relationsIds,
//     relationsLoading,
//   }
// };
//
// const mapDispatchToProps = dispatch => (
//   bindActionCreators(ActionCreators, dispatch)
// );

interface ContentFieldsProps /*extends ConnectProps*/ {
  fields: { [key: string]: string };
  schema: any[];
  onChange: (e: any) => void;
  contentType: ContentType;
  content: ContentModel;
}

export default class ContentFields extends React.Component<ContentFieldsProps, any> {

  onChange = e => {
    this.props.onChange(e);
  };

  renderField = (fieldSchema: any, fields, content, contentType) => {
    switch (fieldSchema.schema.field_type) {
      case 'text':
        return <TextInput name={fieldSchema.slug} value={fields[fieldSchema.slug] || ''} key={fieldSchema.slug} onChange={this.onChange}/>;
      case 'textarea':
        return <DraftEditor content={fields[fieldSchema.slug] || ''} key={fieldSchema.slug} onChange={c => this.onChange({target: {name: fieldSchema.slug, value: c}})}/>;
      case 'file':
        return <FileUpload
          key={fieldSchema.slug}
          content={content}
          contentType={contentType}
          field={fieldSchema}
          onChange={f => this.onChange({target: {name: fieldSchema.slug, value: f}})}/>;
    }
  };

  render() {
    const { schema, fields, contentType, content } = this.props;
    return (
      <div>
        {schema.map(fieldSchema => this.renderField(fieldSchema, fields, content, contentType))}
      </div>
    );
  }
}

// export default connect(mapStateToProps, mapDispatchToProps)(ContentFields)
