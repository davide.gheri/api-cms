import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '@actions';
import { ConnectProps, TaxonomyModel, TermModel } from '@models';
import { FormGroup, Option, SelectInput } from '@core/common/components';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state: any, ownState: any) => ({
  terms: state.terms.terms,
  taxonomies: state.contentTypes.taxonomies[ownState.contentType.id] || [],
});
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

export class ContentTerms extends React.Component<ConnectProps, any> {
  constructor(props: ConnectProps) {
    super(props);
    this.state = {
      terms: {},
    };
  }

  componentWillMount() {
    this.props.getContentTypeTaxonomies(this.props.contentType.id);
  }

  componentDidUpdate(props) {
    if (props.taxonomies.length !== this.props.taxonomies.length) {
      this.props.taxonomies.forEach(tax => {
        this.props.getTerms(tax.id);
      });
    }
  }

  onChange = e => {
    const { name } = e.target;
    const selected = [...e.target.options].filter(o => o.selected).map(o => o.value);
    const { terms } = this.state;
    terms[name] = selected;
    this.setState({terms}, () => {
      const plainTerms = Object.keys(terms).reduce((array, id) => {
        terms[id].forEach(t => array.push(t));
        return array;
      }, []);
      this.props.onTermsChange(plainTerms);
    });
  };

  render() {
    const { taxonomies, terms, contentTerms } = this.props;
    return (
      <div>
        {taxonomies.map((tax: TaxonomyModel) => (
          <FormGroup label={tax.name} key={tax.id}>
            <SelectInput name={tax.id.toString()} multiple={true} onChange={this.onChange} value={contentTerms}>
              {terms[tax.id] && terms[tax.id].map((term: TermModel) => (
                <Option key={term.id} value={term.id}>{term.title}</Option>
              ))}
            </SelectInput>
          </FormGroup>
        ))}
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(ContentTerms));
