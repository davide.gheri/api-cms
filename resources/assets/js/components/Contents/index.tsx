import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SweetAlert from 'sweetalert2-react';
import * as ActionCreators from '@actions';
import { ConnectProps, ContentType, ContentModel, Pagination } from '@models';
import { EnhancedTable, PageHeader } from '@core/backend/components';
import { Button, Icon } from '@core/common/components';
import { currentPage } from '@core/common/utils';
import LocalizedLink from '@core/backend/routes/LocalizedLink';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state, ownState) => {
  const { id } = ownState.match.params;
  return {
    contentType: state.contentTypes.contentTypes.find(ct => ct.id === +id) || {},
    contents: state.contents.contents[id] || [],
    loading: state.contents.loading,
    loadingSingle: state.contents.loadingSingle[id] || {},
    ctId: id,
    page: state.contents.page,
  };
};

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface ContentsProps extends ConnectProps {
  contentType: ContentType;
  contents: ContentModel[];
  loading: boolean;
  loadingSingle: {[key: number]: boolean};
  ctId: number;
  page: Pagination;
}

class Contents extends React.Component<ContentsProps, any> {
  constructor(props: ContentsProps) {
    super(props);
    this.state = {
      swalShow: false,
    };
  }

  componentDidMount() {
    const { ctId } = this.props;
    this.props.getContentType(ctId);
    this.props.getContents(ctId, currentPage(this.props.location.search));
  }

  changePage = (page: number) => {
    const { ctId } = this.props;
    this.props.getContents(ctId, page);
    const { pathname } = this.props.location;
    this.props.push(`${pathname}?page=${page}`);
  };

  onDelete = () => {
    const { ctId } = this.props;
    const id = this.state.swalShow;
    this.setState({swalShow: false});
    this.props.deleteContent(ctId, id);
  };

  render() {
    const { contents, contentType, loading, loadingSingle, ctId, page } = this.props;
    return (
      <div>
        <PageHeader title={`Contents of type ${contentType.name}`}>
          <Button onClick={() => this.props.push(`/content-types/${ctId}/contents/create`)} className="btn-teal shadow"><Icon icon="plus"/> Create new</Button>
        </PageHeader>
        <EnhancedTable
          pagination={page}
          onPageChange={this.changePage}
          className="shadow"
          data={contents}
          columns={{title: 'Title', status: 'Status', actions: 'Actions'}}
          customCols={{actions: (el: ContentModel) => (
            <span>
              <LocalizedLink to={`/content-types/${contentType.id}/contents/${el.id}`} className="btn btn-teal btn-sm mr-3"><Icon icon="edit"/></LocalizedLink>
              <Button onClick={() => this.setState({swalShow: el.id})} className="btn-red btn-sm"><Icon icon="trash"/></Button>
            </span>
            )}}
          linkedCols={{title: '/content-types/{content_type_id}/contents/{id}'}}
          rowLoading={(el: ContentModel) => loadingSingle[el.id] === true}
          loading={loading}/>
        <SweetAlert show={Boolean(this.state.swalShow)}
                    title="Confirm"
                    text="Are you sure?"
                    showCancelButton={true}
                    onConfirm={this.onDelete}/>
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Contents));
