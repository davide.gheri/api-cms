import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { isObject, isArray } from 'lodash';
import * as ActionCreators from '@actions';
import { ConnectProps, ContentModel, ContentType } from '@models';
import {
    Form, FormGroup, Option, SelectInput, TextInput,
    Button, Icon, Card,
} from '@core/common/components';
import { PageHeader } from '@core/backend/components';
import ContentFields from './ContentFields';
import ContentTerms from './ContentTerms';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state, ownState) => {
  const { id, content_id } = ownState.match.params;
  let content = {terms: {}, fields: {}, status: 'DRAFT'};
  const contentType = state.contentTypes.contentTypes.find(ct => ct.id === +id) || {};
  let notFound = false;
  if (content_id) {
    const contents = state.contents.contents[id] || [];
    content = contents.find(c => c.id === +content_id) || {terms: {}, fields: {}, status: 'DRAFT'};
  } else if (!ownState.isNew) {
    notFound = true;
  }
  const loadingContents = state.contents.loadingSingle[id] || {};
  return {
    notFound, content, contentType,
    isNew: ownState.isNew || false,
    loading: state.contents.loading,
    loadingSingle: loadingContents[content_id] || false,
    requestErrors: {},
  };
};

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface ContentProps extends ConnectProps {
  isNew: boolean;
  content: ContentModel;
  contentType: ContentType;
  notFound: boolean;
  loading: boolean;
  loadingSingle: boolean;
  contentTypes: ContentType[];
}

class Content extends React.Component<any, any> {
  constructor(props: ContentProps) {
    super(props);
    this.state = {
      content: {},
      content_types_edited: false,
      valid: false,
      errors: {},
    };
  }

  componentDidMount() {
    const { isNew, content, notFound } = this.props;
    const { content_id, id } = this.props.match.params;
    if (notFound) {
      this.props.routerPush('/404');
    }
    if (!isNew && !content.hasOwnProperty('title')) {
      this.props.getContent(id, content_id);
    }
    this.props.getContentType(id);
  }

  static getDerivedStateFromProps(newProps: ContentProps, state: any) {
    if (!state.content_types_edited && newProps.content.hasOwnProperty('title')) {
      state.content = Object.assign({}, newProps.content);
      state.content_types_edited = true;

      state.content.terms = Object.keys(newProps.content.terms).reduce((array, tax) => {
        newProps.content.terms[tax].map(term => term.id).forEach(id => array.push(id));
        return array;
      }, []);
    }
    state.valid = Object.values(newProps.content).length > 0;
    return state;
  }

  verifyValue = (name: string) => {
    const toVerify = ['title', 'status'];
    if (toVerify.includes(name)) {
      const { errors } = this.state;
      errors[name] = !Boolean(this.state.content[name]);
      this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
    }
  };

  onChange = e => {
    const { content } = this.state;
    const { name, value } = e.target;
    content[name] = value;
    this.setState({content}, () => this.verifyValue(name));
  };

  onFieldsChange = e => {
    const { content } = this.state;
    const { name, value } = e.target;
    content.fields[name] = value;
    this.setState({content});
  };

  onTermsChange = terms => {
    const { content } = this.state;
    content.terms = terms;
    this.setState({content});
  };

  onSubmit = () => {
    const { contentType, isNew } = this.props;
    const { content } = this.state;
    if (content.fields) {
      content.fields = Object.keys(content.fields).reduce((obj, name) => {
        if (isObject(content.fields[name]) && !isArray(content.fields[name])) {
          if (content.fields[name].hasOwnProperty('full_url')) {
            obj[name] = content.fields[name].full_url;
          } else if (content.fields[name].hasOwnProperty('level')) {
            obj[name] = content.fields[name].level.content;
          }
        } else {
          obj[name] = content.fields[name];
        }
        return obj;
      }, {});
    }
    if (!content.status) {
      content.status = 'DRAFT';
    }
    if (isNew) {
      this.props.createContent(contentType.id, content);
    } else {
      this.props.updateContent(contentType.id, content.id, content);
    }
  };

  render() {
    const { isNew, contentType, loading, loadingSingle, requestErrors } = this.props;
    const { errors, content, valid } = this.state;
    const originalContent = this.props.content;
    return (
      <div>
        <Form onSubmit={this.onSubmit} disabled={loading || loadingSingle}>
          <PageHeader title={isNew ? `New ${contentType.name}` : `Edit ${contentType.name} ${originalContent.title}`}/>
          <Card>
            <FormGroup label="Title">
                  <TextInput type="text" name="title"
                             value={content.title || ''}
                             onChange={this.onChange}
                             errored={errors.title || requestErrors.title || false}
                             required/>
                  {errors.title && <span className="help help-error">Title is required!</span>}
                  {requestErrors.title && <span className="help help-error">{requestErrors.title}</span>}
              </FormGroup>
            <FormGroup label="Status">
                  <SelectInput onChange={this.onChange} name="status" value={content.status}>
                      <Option value="DRAFT" label="Draft"/>
                      <Option value="PUBLISHED" label="Published"/>
                      <Option value="ARCHIVED" label="Archived"/>
                  </SelectInput>
              </FormGroup>
          </Card>
          <Card className="mt-4">
            {!isNew ?
              <ContentFields
                  fields={content.fields || {}}
                  schema={contentType.fields || []}
                  contentType={contentType}
                  content={content}
                  onChange={this.onFieldsChange}/>
              : <span>Create the content before filling the custom fields</span>
            }
          </Card>
          {Object.keys(contentType).length > 0 &&
          <Card className="mt-4">
            <ContentTerms contentType={contentType} contentTerms={content.terms} onTermsChange={this.onTermsChange}/>
          </Card>
          }
          <div className="pt-4 flex">
            <Button className="btn-teal shadow" disabled={!valid || loading || loadingSingle} type="submit">
              {loading ? <Icon icon="spinner" spin/> : 'Save'}
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Content));
