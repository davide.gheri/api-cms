import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ConnectProps } from '@models';
import { Card, Icon } from '@core/common/components';
import * as ActionCreators from '@actions';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state: any) => ({
  users: {
    loading: state.users.loading,
    count: state.users.page.total,
  },
  contentTypes: {
    loading: state.contentTypes.loading,
    count: state.contentTypes.page.total,
  },
  taxonomies: {
    loading: state.taxonomies.loading,
    count: state.taxonomies.taxonomies.length, // TODO CHANGE WHEN PAGINATING THE TAXONOMIES
  },
});

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class Counters extends React.Component<ConnectProps, any> {
  componentWillMount() {
    this.props.getUsers();
    this.props.getContentTypes();
    this.props.getTaxonomies();
  }

  render() {
    const { users, contentTypes, taxonomies } = this.props;
    return (
      <div className="counters card-group">
        <Card><h2>{users.loading ? <Icon icon="spinner" spin/> : users.count} <small>Users</small></h2></Card>
        <Card><h2>{contentTypes.loading ? <Icon icon="spinner" spin/> : contentTypes.count} <small>Content types</small></h2></Card>
        <Card><h2>{taxonomies.loading ? <Icon icon="spinner" spin/> : taxonomies.count} <small>Taxonomies</small></h2></Card>
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Counters));
