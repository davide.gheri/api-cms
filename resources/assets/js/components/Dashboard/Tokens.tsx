import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SweetAlert from 'sweetalert2-react';
import * as ActionCreators from '@actions';
import { ConnectProps, TokenModel } from '@models';
import {
    Table, Tbody, Td, Th, Thead, Tr,
    Modal, ModalContent, ModalTitle,
    FormGroup, Form, CopyTextarea, TextInput,
    Button, Icon, Card,
} from '@core/common/components';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state: any) => ({
  tokens: state.tokens.tokens.filter(token => token.id !== state.auth.tokenId),
  loading: state.tokens.loading,
  tokenModal: state.tokens.modal,
});

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface TokensProps extends ConnectProps {
  tokens: TokenModel[];
  loading: boolean;
  tokenModal: {show: boolean, content: string};
}

interface TokensState {
  swalShow: boolean | string;
  newTokenModal: boolean;
  newToken: {
    name: string;
    scopes: string[]
  };
}

class Tokens extends React.Component<TokensProps, TokensState> {
  state = {
    swalShow: false,
    newTokenModal: false,
    newToken: {
      name: '',
      scopes: [],
    },
  };

  componentDidMount() {
    this.props.getTokens();
  }

  static getDerivedStateFromProps(props: TokensProps, state: TokensState) {
    if (props.tokenModal.show) {
      state.newTokenModal = false;
      state.newToken = {name: '', scopes: []};
    }

    return state;
  }

  onDelete = () => {
    const id: string = `${this.state.swalShow}`;
    this.props.deleteToken(id);
    this.setState({swalShow: false});
  };

  onModalClose = () => {
    this.props.toggleModal(false, null);
  };

  onCreateModalToggle = () => {
    this.setState({newTokenModal: !this.state.newTokenModal});
  };

  onSubmit = () => {
    const { newToken } = this.state;
    this.props.createToken(newToken);
  };

  onChange = e => {
    const { name, value } = e.target;
    const { newToken } = this.state;
    newToken[name] = value;
    this.setState({newToken});
  };

  render() {
    const { loading, tokens, tokenModal } = this.props;
    const { newToken, newTokenModal } = this.state;
    return (
      <Card className="p-0">
        <div className="flex py-2 px-4 items-center">
          <h3>Access tokens</h3>
          <Button className="ml-auto" onClick={this.onCreateModalToggle}><Icon icon="plus"/> Create new token</Button>
        </div>
        <Table>
          <Thead>
            <Tr>
              <Th>Name</Th>
              <Th>Expires at</Th>
              <Th>Actions</Th>
            </Tr>
          </Thead>
          {loading ? <Tbody><Tr><Td colspan={2}>Loading...</Td></Tr></Tbody> :
            <Tbody>
              {tokens.length === 0 ?
                <Tr><Td colspan={2}>No result</Td></Tr> :
                tokens.map(token => (
                  <Tr key={token.id}>
                    <Td title="Name">{token.name}</Td>
                    <Td title="Expires at">{token.expires_at}</Td>
                    <Td title="Actions">
                      <Button onClick={() => this.setState({swalShow: token.id})} className="btn-red btn-sm"><Icon icon="trash"/></Button>
                    </Td>
                  </Tr>
                ))
              }
            </Tbody>
          }
        </Table>
        <SweetAlert show={Boolean(this.state.swalShow)}
                    title="Confirm"
                    text="Are you sure?"
                    showCancelButton={true}
                    onConfirm={this.onDelete}/>
        {newTokenModal  &&
          <Modal onClose={this.onCreateModalToggle}>
            <ModalTitle title="Create new token"/>
            <ModalContent>
              <Form onSubmit={this.onSubmit}>
                <FormGroup label="Token Name">
                  <TextInput name="name" value={newToken.name} onChange={this.onChange}/>
                </FormGroup>
                <div className="pt-4 flex">
                  <Button className="btn-teal shadow" disabled={loading} type="submit">
                    {loading ? <Icon icon="spinner" spin/> : 'Save'}
                  </Button>
                </div>
              </Form>
            </ModalContent>
          </Modal>
        }
        {tokenModal.show &&
          <Modal onClose={this.onModalClose}>
            <ModalTitle title="Access token"/>
            <ModalContent>
              <p>Be sure to copy this token, this is the only time you can see it!</p>
              <br/>
              <CopyTextarea readOnly={true} value={tokenModal.content} style={{minHeight: 300}}/>
            </ModalContent>
          </Modal>
        }
      </Card>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Tokens));
