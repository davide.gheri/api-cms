import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '@actions';
import Counters from './Counters';
import Tokens from './Tokens';

const mapStateToProps = (state: any) => (state);

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class Dashboard extends React.Component<any, any> {
  render() {
    return (
      <div>
        <Counters/>
        <Tokens/>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
