import * as React from 'react';
import { ToastContainer, ToastPosition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Navbar from './Navbar';
import Sidebar from './Sidebar';
import { MainRoutes } from '@routes';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as ActionCreators from '@actions';
import { ConnectProps } from '@models';
import { can } from '@core/backend/utils';
import { PageLoading } from '@core/backend/components';
import { LocaleContext } from '@core/common/contexts';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = state => ({
  auth: state.auth,
  locale: state.locale,
});
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class Main extends React.Component<ConnectProps, any> {
  constructor(props: ConnectProps) {
    super(props);
    this.state = {
      ready: false,
      sidebarOpen: false,
    };
  }

  async componentDidMount() {
    await this.props.getAuthUser();
    const { user } = this.props.auth;

    if (can(user, 'update-content-types', 'create-content-types')) {
      this.props.getFieldSchemas();
    }
    if (can(user, 'update-users', 'create-users')) {
      this.props.getRoles();
    }
    this.setState({ready : true});
  }

  onSidebarToggle = () => {
    this.setState({sidebarOpen: !this.state.sidebarOpen});
  };

  render() {
    const { ready, sidebarOpen } = this.state;
    return (
      <LocaleContext.Provider value={this.props.locale}>
          <Navbar toggleSidebar={this.onSidebarToggle}/>
          <main id="main">
              <Sidebar open={sidebarOpen} toggleSidebar={this.onSidebarToggle}/>
              <div id="content">
                  {ready ? <MainRoutes/> : <PageLoading/>}
              </div>
          </main>
          <ToastContainer autoClose={3000} position={ToastPosition.BOTTOM_RIGHT} newestOnTop={true}/>
      </LocaleContext.Provider>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Main));
