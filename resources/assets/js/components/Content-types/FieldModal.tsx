import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    Modal, ModalContent, ModalTitle,
    Form, FormGroup, Option, SelectInput, TextInput,
    Button, Icon,
} from '@core/common/components';
import * as ActionCreators from '@actions';
import { ConnectProps, ContentType, FieldSchema, ContentTypeFieldsModel } from '@models';
import { slugify } from '@core/common/utils';
import { trans } from '@core/backend/utils';

const mapStateToProps = (state: any, ownState: any) => {
  const ctErrors = state.contentTypeFields.errors[ownState.contentType.id] || {};
  const field = ownState.field || {};
  return {
    field,
    contentType: ownState.contentType,
    contentTypes: state.contentTypes.contentTypes,
    schemas: state.fieldSchemas.schemas,
    loading: state.contentTypeFields.loading,
    requestErrors: ctErrors[field.id || 0] || {},
    isEdit: ownState.isEdit || false,
  };
};
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface FieldModalProps extends ConnectProps {
  [key: string]: any;
  contentType: ContentType;
  field?: ContentTypeFieldsModel;
  contentTypes: ContentType[];
}

interface FieldModalState {
  name: string;
  slug: string;
  field_schema_id: number;
  options: any;
  errors: any;
  selected_schema: FieldSchema;
  valid: boolean;
  [key: string]: any;
}

class FieldModal extends React.Component<FieldModalProps, FieldModalState> {
  constructor(props: FieldModalProps) {
    super(props);
    this.state = {
      name: '',
      slug: '',
      field_schema_id: 0,
      options: {},
      errors: {},
      selected_schema: null,
      valid: false,
    };
  }

  componentDidMount() {
    const newProps = this.props;
    this.props.getContentTypes();
    if (Object.keys(newProps.field).length > 0) {
      let name, slug, field_schema_id, options, selected_schema;
      name = newProps.field.name;
      slug = newProps.field.slug;
      field_schema_id = newProps.field.schema.id;
      options = Object.keys(newProps.field.options).reduce((obj, name) => {
        obj[name] = newProps.field.options[name] || '';
        return obj;
      }, {});
      if (newProps.field.schema.name === 'ct_relation') {
        options['relations'] = options['relations'] || [];
      }
      selected_schema = newProps.field.schema;
      this.setState({name, slug, field_schema_id, options, selected_schema});
    }

  }

  onSubmit = () => {
    const { name, slug, field_schema_id, options } = this.state;
    const { id } = this.props.contentType;
    if (this.props.isEdit) {
      this.props.updateContentTypeField(id, this.props.field.id, {name, slug, field_schema_id, options});
    } else {
      this.props.createContentTypeField(id, {name, slug, field_schema_id, options});
    }
  };

  verifyValue = (name: string) => {
    const toVerify = ['name', 'slug', 'field_schema_id'];
    if (toVerify.includes(name)) {
      const { errors } = this.state;
      errors[name] = !Boolean(this.state[name]);
      this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
    }
  };

  onChange = e => {
    const { name, value } = e.target;
    let selected_schema = this.state.selected_schema;
    const options = this.state.options;
    if (name === 'field_schema_id') {
      const { schemas } = this.props;
      selected_schema = schemas.find(schema => schema.id === value);
      for (const prop of Object.keys(options)) {
        delete options[prop];
      }
      const newProps = Object.assign(Object.keys(selected_schema.available_options).reduce((obj, opt) => {
        obj[opt] = '';
        return obj;
      }, {}), selected_schema.options);
      for (const p in newProps) options[p] = newProps[p];
    }
    this.setState({selected_schema, options, [name]: value}, () => this.verifyValue(name));
  };

  onOptionCheckBoxChange = e => {
    const { name, checked, value } = e.target;
    return this.onOptionsChange({target: {name, value: checked ? value : 0}});
  };

  onOptionMultipleCheckBoxChange = e => {
    const { name } = e.target;
    const checks = Array.from(document.getElementsByName(name));
    const value = [];
    checks.forEach((check: HTMLInputElement) => {
      if (check.checked) {
        value.push(check.value);
      }
    });
    const { options } = this.state;
    options[name.replace('[]', '')] = value;
    this.setState({options});
  };

  onOptionsChange = e => {
    const { name, value } = e.target;
    const { options } = this.state;
    options[name] = value;
    this.setState({options});
  };

  onAutoSlug = () => {
    const { name } = this.state;
    const slug = slugify(name);
    this.setState({slug}, () => this.verifyValue('slug'));
  };

  renderOptions = schema => {
    const { options } = this.state;

    return Object.keys(options).map((name, i) => {
      const type = schema.available_options[name];
      if (type) {
        switch (type) {
          case 'boolean':
            return (
              <FormGroup label={name} key={i} inline={true}>
                <TextInput type="checkbox" className="checkbox" value={1} checked={Boolean(options[name])} name={name} onChange={this.onOptionCheckBoxChange}/>
              </FormGroup>
            );
          case 'string':
            return (
              <FormGroup label={name} key={i}>
                <TextInput value={options[name]} name={name} onChange={this.onOptionsChange}/>
              </FormGroup>
            );
          case 'integer':
            return (
              <FormGroup label={name} key={i}>
                <TextInput type="number" value={options[name]} name={name} onChange={this.onOptionsChange}/>
              </FormGroup>
            );
        }
      }

      switch (name) {
        case 'relations':
          return (
            <div key={i}>
              <legend className="mb-2">Content types relation</legend>
              {this.props.contentTypes.map((ct: ContentType) => (
                <FormGroup key={ct.id} label={ct.name} inline={true}>
                  <TextInput type="checkbox"
                             className="checkbox"
                             value={ct.id}
                             checked={options[name].includes(ct.id.toString())} name={`${name}[]`} onChange={this.onOptionMultipleCheckBoxChange}/>
                </FormGroup>
              ))}
            </div>
          );
      }
    });
  };

  render() {
    const { push, contentType, schemas, requestErrors, loading, isEdit } = this.props;
    const { name, errors, slug, field_schema_id, selected_schema, valid } = this.state;
    return(
      <Modal className="sm:w-3/4" onClose={() => push(`/content-types/${contentType.id}`)}>
        <ModalTitle title={isEdit ? trans('contents.content-type.fields.edit-field', {field: name}) as string : trans('contents.content-type.fields.add-new') as string}/>
        <ModalContent>
          <Form onSubmit={this.onSubmit}>
            <FormGroup label={trans('contents.name.label') as string}>
              <TextInput value={name} name="name" onChange={this.onChange} required errored={errors.name || requestErrors.name || false}/>
              {errors.name && <span className="help help-error">{trans('contents.name.required')}</span>}
              {requestErrors.name && <span className="help help-error">{requestErrors.name}</span>}
            </FormGroup>
            <FormGroup label={trans('contents.slug.label') as string}>
              <TextInput type="text"
                         name="slug"
                         value={slug || ''}
                         errored={errors.slug || requestErrors.slug || false}
                         onChange={this.onChange}
                         append={() => <Button disabled={errors.name} onClick={this.onAutoSlug} text="Auto" type="button"/>}
                         required/>
              {errors.slug && <span className="help help-error">{trans('contents.slug.required')}</span>}
              {requestErrors.slug && <span className="help help-error">{requestErrors.slug}</span>}
            </FormGroup>
            <FormGroup label="Field Schema">
              <SelectInput onChange={this.onChange} name="field_schema_id" value={field_schema_id}>
                <Option disabled value={0}>{trans('select-option')}</Option>
                {schemas.map(schema => (
                  <Option key={schema.id} value={schema.id}>{schema.name}</Option>
                ))}
              </SelectInput>
            </FormGroup>
            <hr/>
            {selected_schema && this.renderOptions(selected_schema)}
            <div className="pt-4 flex">
              <Button className="btn-teal shadow" disabled={!valid || loading} type="submit">
                {loading ? <Icon icon="spinner" spin/> : 'Save'}
              </Button>
            </div>
          </Form>
        </ModalContent>
      </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FieldModal);
