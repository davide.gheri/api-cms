import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SweetAlert from 'sweetalert2-react';
import * as ActionCreators from '@actions';
import { ConnectProps, ContentType as ContentTypeModel } from '@models';
import { PageHeader } from '@core/backend/components';
import { Form, FormGroup, Textarea, TextInput, Button, Icon, Card } from '@core/common/components';
import { slugify } from '@core/common/utils';
import ContentTypeFields from './Content-type-fields';
import LocalizedLink from '@core/backend/routes/LocalizedLink';
import { State } from '@reducers';
import withErrorBoundary from '@components/withErrorBoundary';
import { trans } from '@core/backend/utils';

const mapStateToProps = (state: State, ownState: any) => {
  let contentType = {};
  let notFound = false;
  const { id } = ownState.match.params;
  if (id) {
    contentType = state.contentTypes.contentTypes.find(ct => ct.id === +id) || {};
  } else {
    notFound = true;
  }
  return {
    contentType, notFound,
    loading: state.contentTypes.loading,
    loadingSingle: state.contentTypes.loadingSingle[id] || false,
    requestErrors: state.contentTypes.errors[id] || {},
  };
};

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface ContentTypeProps extends ConnectProps {
  isNew: boolean;
  contentType: ContentTypeModel;
  notFound: boolean;
  loading: boolean;
  loadingSingle: boolean;
}

export interface ContentTypeState {
  contentType: ContentTypeModel;
  errors: {[key: string]: boolean};
  valid: boolean;
  swalShow?: boolean;
}

class ContentType extends React.Component<ContentTypeProps, ContentTypeState> {
  constructor(props: ContentTypeProps) {
    super(props);
    this.state = {
      contentType: {},
      errors: {},
      valid: false,
      swalShow: false,
    };
  }

  componentDidMount() {
    const { isNew, contentType, notFound } = this.props;
    const { id } = this.props.match.params;
    if (notFound) {
      this.props.push('/404');
    }
    if (!isNew && !Object.keys(contentType).length) {
      this.props.getContentType(id);
    }
  }

  static getDerivedStateFromProps(newProps: ContentTypeProps, state: ContentTypeState) {
    state.contentType = newProps.contentType;
    state.valid = Object.values(newProps.contentType).length > 0;
    return state;
  }

  onSubmit = () => {
    const { contentType } = this.state;
    this.props.updateContentType(contentType.id, contentType);
  };

  onDelete = () => {
    this.setState({swalShow: false});
    const { id } = this.props.match.params;
    this.props.deleteContentType(id);
  };

  verifyValue = (name: string) => {
    const toVerify = ['name', 'slug'];
    if (toVerify.includes(name)) {
      const { errors } = this.state;
      errors[name] = !Boolean(this.state.contentType[name]);
      this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
    }
  };

  onChange = e => {
    const { contentType } = this.state;
    const { name, value } = e.target;
    if (name === 'slug') {
      contentType[name] = slugify(value);
    } else {
      contentType[name] = value;
    }
    this.setState({contentType}, () => this.verifyValue(name));
  };

  onAutoSlug = () => {
    const { contentType } = this.state;
    contentType.slug = slugify(contentType.name);
    this.setState({contentType}, () => this.verifyValue('slug'));
  };

  render() {
    const { contentType, errors, valid } = this.state;
    const { loading, loadingSingle, requestErrors } = this.props;
    return (
      <div>
        <Form onSubmit={this.onSubmit} disabled={loading || loadingSingle}>
          <PageHeader title="Edit content type">
            <LocalizedLink to={`/content-types/${contentType.id}/contents`} className="btn btn-teal shadow">{trans('contents.manage')}</LocalizedLink>
          </PageHeader>
          <Card>
            <FormGroup label="Name">
                <TextInput type="text" name="name"
                           value={contentType.name || ''}
                           onChange={this.onChange}
                           errored={errors.name || requestErrors.name || false}
                           required/>
                {errors.name && <span className="help help-error">{trans('contents.name.required')}</span>}
                {requestErrors.name && <span className="help help-error">{requestErrors.name}</span>}
            </FormGroup>
            <FormGroup label="Slug">
                <TextInput type="text"
                           name="slug"
                           value={contentType.slug || ''}
                           errored={errors.slug || requestErrors.slug || false}
                           onChange={this.onChange}
                           append={() => <Button disabled={errors.name} onClick={this.onAutoSlug} text="Auto" type="button"/>}
                           required/>
                {errors.slug && <span className="help help-error">{trans('contents.slug.required')}</span>}
                {requestErrors.slug && <span className="help help-error">{requestErrors.slug}</span>}
            </FormGroup>
            <FormGroup label="Description">
              <Textarea name="description"
                        value={contentType.description || ''}
                        errored={errors.description || requestErrors.description || false}
                        onChange={this.onChange}/>
                    {errors.description && <span className="help help-error">{trans('description.slug.required')}</span>}
                    {requestErrors.description && <span className="help help-error">{requestErrors.description}</span>}
            </FormGroup>
          </Card>
          <div className="pt-4 flex">
            <Button className="btn-teal shadow" disabled={!valid || loading || loadingSingle} type="submit">
              {loading ? <Icon icon="spinner" spin/> : 'Save'}
            </Button>
            <Button disabled={loading || loadingSingle} className="btn-red ml-auto shadow" type="button" onClick={() => this.setState({swalShow: true})}>
              {loadingSingle ? <Icon icon="spinner" spin/> : <span><Icon icon="trash"/> {trans('delete')}</span>}
            </Button>
          </div>
          <SweetAlert show={this.state.swalShow}
                      title={trans('confirm.title')}
                      text={trans('confirm.message')}
                      showCancelButton={true}
                      onConfirm={this.onDelete}/>
        </Form>
        {Object.keys(contentType).length > 0 && <ContentTypeFields contentType={contentType}/>}
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(ContentType));
