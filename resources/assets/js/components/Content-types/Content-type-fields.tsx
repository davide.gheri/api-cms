import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Switch, withRouter } from 'react-router';
import SweetAlert from 'sweetalert2-react';
import * as ActionCreators from '@actions';
import { ConnectProps, ContentType as ContentTypeModel, ContentTypeFieldsModel } from '@models';
import { EnhancedTable, PageHeader } from '@core/backend/components';
import { Button, Icon } from '@core/common/components';
import { asyncComponent } from '@core/common/routes/asyncComponent';
import LocalizedRoute from '@core/backend/routes/LocalizedRoute';
import withErrorBoundary from '@components/withErrorBoundary';
import { trans } from '@core/backend/utils';

const mapStateToProps = (state, ownState) => ({
  contentType: ownState.contentType,
  fields: state.contentTypeFields.fields[ownState.contentType.id] || [],
  loading: state.contentTypeFields.loading,
  loadingSingle: state.contentTypeFields.loadingSingle[ownState.contentType.id] || {},
});

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface ContentTypeFieldsProps extends ConnectProps {
  contentType: ContentTypeModel;
  fields: ContentTypeFieldsModel[];
  loading: boolean;
  loadingSingle: {[key: number]: boolean};
}

export interface ContentTypeFieldsState {
  swalShow?: boolean | number;
}

const AsyncFieldModalComponent = asyncComponent(import('./FieldModal'));

class ContentTypeFields extends React.Component<ContentTypeFieldsProps, ContentTypeFieldsState> {
  constructor(props: ContentTypeFieldsProps) {
    super(props);
    this.state = {
      swalShow: false,
    };
  }

  componentDidMount() {
    const { contentType, fields } = this.props;
    if (!fields.length) {
      this.props.getContentTypeFields(contentType.id);
    }
  }

  onDelete = () => {
    const id = this.state.swalShow;
    const { contentType } = this.props;
    this.setState({swalShow: false});
    this.props.deleteContentTypeField(contentType.id, id as number);
  };

  render() {
    const { loading, fields, contentType, push, loadingSingle } = this.props;
    return(
        <div className="mt-8">
          <hr/>
          <PageHeader title={trans('contents.content-type.fields.edit') as string} heading="h3" backButton={false}>
            <Button onClick={() => this.props.push(`/content-types/${contentType.id}/fields/create`)} className="btn-teal ml-auto shadow"><Icon icon="plus"/> {trans('add-new')}</Button>
          </PageHeader>
          <EnhancedTable data={fields}
                         columns={{name: 'Name', type: 'Type', actions: 'Actions'}}
                         loading={loading}
                         className="shadow"
                         linkedCols={{name: '/'}}
                         rowLoading={(el: ContentTypeFieldsModel) => loadingSingle[el.id] === true}
                         customCols={{
                           type: (el: ContentTypeFieldsModel) => el.schema.field_type === 'text' ? el.options.type : el.schema.field_type,
                           actions: (el: ContentTypeFieldsModel) => (
                             <span>
                                <Button onClick={() => push(`/content-types/${contentType.id}/fields/${el.id}`)} className="btn-teal btn-sm mr-3"><Icon icon="edit"/></Button>
                                <Button onClick={() => this.setState({swalShow: el.id})} className="btn-red btn-sm"><Icon icon="trash"/></Button>
                              </span>
                           ),
                         }}/>
          <SweetAlert show={Boolean(this.state.swalShow)}
                      title={trans('confirm.title')}
                      text={trans('confirm.message')}
                      showCancelButton={true}
                      onConfirm={this.onDelete}/>
          {/*<Switch>*/}
            <LocalizedRoute path="/content-types/:id/fields/create" render={(props: any) => <AsyncFieldModalComponent {...props} contentType={contentType}/>}/>
            <LocalizedRoute path="/content-types/:id/fields/:field_id(\d+)" render={(props: any) => {
              const { field_id } = props.match.params;
              const field = fields.find(field => field.id === field_id);
              return <AsyncFieldModalComponent {...props} contentType={contentType} field={field} isEdit={true}/>;
            }}/>
          {/*</Switch>*/}
        </div>
    );
  }
}

export default withErrorBoundary(withRouter(connect(mapStateToProps, mapDispatchToProps)(ContentTypeFields)));
