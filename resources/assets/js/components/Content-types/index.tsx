import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '@actions';
import { PageHeader} from '@core/backend/components';
import { Icon, Button } from '@core/common/components';
import { can, trans } from '@core/backend/utils';
import { currentPage } from '@core/common/utils';
import { asyncComponent } from '@core/common/routes/asyncComponent';
import { LocalizedRoute } from '@core/backend/routes';
import ContentTypesTable from '@components/Content-types/ContentTypesTable';

const mapStateToProps = (state: any) => ({
  user: state.auth.user,
});

const mapDispatchToprops = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);
const AsyncCreateContentTypeModal = asyncComponent(import('./CreateModal'));

class ContentTypes extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      swalShow: false,
    };
  }

  componentDidMount() {
    this.props.getContentTypes(currentPage(this.props.location.search));
  }

  changePage = (page: number) => {
    this.props.getContentTypes(page);
    const { pathname } = this.props.location;
    this.props.push(`${pathname}?page=${page}`);
  };

  onDelete = () => {
    const id = this.state.swalShow;
    this.setState({swalShow: false});
    this.props.deleteContentType(id);
  };

  render() {
    const { user } = this.props;
    return (
      <div>
        <PageHeader title="Content types">
          {can(user, 'create-content-types') &&
          <Button onClick={() => this.props.push('/content-types/create')} className="btn-teal shadow">
            <Icon icon="plus"/> {trans('create-new')}
          </Button>
          }
        </PageHeader>
        <ContentTypesTable/>
        {can(user, 'create-content-types') && <LocalizedRoute path="/content-types/create" render={(props: any) => <AsyncCreateContentTypeModal {...props}/>}/>}
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToprops)(ContentTypes);
