import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    Modal, ModalContent, ModalTitle,
    Form, FormGroup, Textarea, TextInput,
    Button, Icon,
} from '@core/common/components';
import { slugify } from '@core/common/utils';
import { trans } from '@core/backend/utils';
import { ContentTypeState } from './Content-type';
import * as ActionCreators from '@actions';

const mapStateToProps = (state: any) => ({
  loading: state.contentTypes.loading,
  requestErrors: state.contentTypes.errors['0'] || {},
});
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class CreateModal extends React.Component<any, ContentTypeState> {
  constructor(props: any) {
    super(props);
    this.state = {
      contentType: {
        name: '',
        slug: '',
        description: '',
      },
      errors: {},
      valid: false,
    };
  }

  onSubmit = () => {
    const { contentType } = this.state;
    this.props.createContentType(contentType);
  };

  verifyValue = (name: string) => {
    const toVerify = ['name', 'slug'];
    if (toVerify.includes(name)) {
      const { errors } = this.state;
      errors[name] = !Boolean(this.state.contentType[name]);
      this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
    }
  };

  onChange = e => {
    const { contentType } = this.state;
    const { name, value } = e.target;
    if (name === 'slug') {
      contentType[name] = slugify(value);
    } else {
      contentType[name] = value;
    }
    this.setState({contentType}, () => this.verifyValue(name));
  };

  onAutoSlug = () => {
    const { contentType } = this.state;
    contentType.slug = slugify(contentType.name);
    this.setState({contentType}, () => this.verifyValue('slug'));
  };

  render() {
    const { contentType, errors, valid } = this.state;
    const { loading, push, requestErrors } = this.props;
    return (
      <Modal className="sm:w-1/2" onClose={() => push('/content-types')}>
        <ModalTitle title={trans('contents.content-type.create-new') as string}/>
        <ModalContent>
          <Form onSubmit={this.onSubmit}>
              <FormGroup label="Name">
                <TextInput type="text" name="name"
                           value={contentType.name || ''}
                           onChange={this.onChange}
                           errored={errors.name || requestErrors.name || false}
                           required/>
                {errors.name && <span className="help help-error">{trans('contents.name.required')}</span>}
                {requestErrors.name && <span className="help help-error">{requestErrors.name}</span>}
              </FormGroup>
              <FormGroup label="Slug">
                <TextInput type="text"
                           name="slug"
                           value={contentType.slug || ''}
                           errored={errors.slug || requestErrors.slug || false}
                           onChange={this.onChange}
                           append={() => <Button disabled={errors.name} onClick={this.onAutoSlug} text="Auto" type="button"/>}
                           required/>
                {errors.slug && <span className="help help-error">{trans('contents.slug.required')}</span>}
                {requestErrors.slug && <span className="help help-error">{requestErrors.slug}</span>}
              </FormGroup>
              <FormGroup label="Description">
            <Textarea name="description"
                      value={contentType.description || ''}
                      errored={errors.description || requestErrors.description || false}
                      onChange={this.onChange}/>
                {errors.description && <span className="help help-error">{trans('contents.description.required')}</span>}
                {requestErrors.description && <span className="help help-error">{requestErrors.description}</span>}
              </FormGroup>
            <div className="pt-4 flex">
              <Button className="btn-teal shadow" disabled={!valid || loading} type="submit">
                {loading ? <Icon icon="spinner" spin/> : 'Save'}
              </Button>
            </div>
          </Form>
        </ModalContent>
      </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateModal);
