import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SweetAlert from 'sweetalert2-react';
import * as ActionCreators from '@actions';
import { EnhancedTable } from '@core/backend/components';
import { Icon, Button } from '@core/common/components';
import { ContentType } from '@models';
import { can, trans } from '@core/backend/utils';
import { currentPage } from '@core/common/utils';
import withErrorBoundary from '@components/withErrorBoundary';
import { withRouter } from 'react-router';

const mapStateToProps = (state: any) => ({
  contentTypes: state.contentTypes,
  user: state.auth.user,
});

const mapDispatchToprops = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class ContentTypesTable extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      swalShow: false,
    };
  }

  componentDidMount() {
    this.props.getContentTypes(currentPage(this.props.location.search));
  }

  changePage = (page: number) => {
    this.props.getContentTypes(page);
    const { pathname } = this.props.location;
    this.props.push(`${pathname}?page=${page}`);
  };

  onDelete = () => {
    const id = this.state.swalShow;
    this.setState({swalShow: false});
    this.props.deleteContentType(id);
  };

  render() {
    const { loading, contentTypes, loadingSingle, page } = this.props.contentTypes;
    const { user } = this.props;
    return (
      <div>
        <EnhancedTable
          pagination={page}
          onPageChange={this.changePage}
          className="shadow"
          data={contentTypes}
          columns={Object.assign({
            name: trans('columns.name'),
            slug: trans('columns.slug'),
            updated_at: trans('columns.last-edit'),
            content_count: trans('contents.contents'),
          }, can(user, 'delete-content-types') ? {actions: trans('columns.actions')} : {})}
          linkedCols={{
            name: can(user, 'update-content-types') ? '/content-types/{id}' : '/content-types/{id}/contents',
            content_count: '/content-types/{id}/contents',
          }}
          customCols={{actions: (el: ContentType) => <Button onClick={() => this.setState({swalShow: el.id})} className="btn-red btn-sm"><Icon icon="trash"/></Button>}}
          rowLoading={(el: ContentType) => loadingSingle[el.id] === true}
          loading={loading}/>
        <SweetAlert show={Boolean(this.state.swalShow)}
                    title={trans('confirm.title')}
                    text={trans('confirm.message')}
                    showCancelButton={true}
                    onConfirm={this.onDelete}/>
      </div>
    );
  }
}

export default withRouter(withErrorBoundary(connect(mapStateToProps, mapDispatchToprops)(ContentTypesTable)));
