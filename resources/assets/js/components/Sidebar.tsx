import * as React from 'react';
import { connect } from 'react-redux';
import * as classNames from 'classnames';
import { Link } from 'react-router-dom';
import { Icon } from '@core/common/components';
import { can } from '@core/backend/utils';
import { withTheme, WithThemeInterface } from '@core/common/contexts';

namespace Props {
  export interface Group {
    hasChildren?: boolean;
    toggler?: () => any;
    children: any;
  }
  export interface Item extends WithThemeInterface {
    onClick?: () => void;
    hasChildren?: boolean;
    title: string;
    icon?: string;
    toggleIcon?: string;
    href?: string;
  }
}

const SidebarItemComponent: React.StatelessComponent<Props.Item> = (props: Props.Item) => {
  const classes = classNames(props.theme.sidebar_item);
  if (props.hasChildren) {
    return (
      <a className={classes} href="#" onClick={props.onClick}>
        {props.icon && <Icon icon={props.icon}/>}
        {props.title}
        <Icon className="toggle" icon={props.toggleIcon}/>
      </a>
    );
  }
  return (
    <Link to={props.href} className={classes}>
      {props.icon && <Icon icon={props.icon}/>}
      {props.title}
    </Link>
  );
};

SidebarItemComponent.defaultProps = {
  href: '/',
};
SidebarItemComponent.displayName = 'SidebarItem';

const SidebarItem = withTheme<Props.Item>(SidebarItemComponent);

class SidebarGroup extends React.Component<Props.Group, {open: boolean}> {
  constructor(props: Props.Group) {
    super(props);
    this.state = {
      open: false,
    };
  }

  onToggle = e => {
    e.preventDefault();
    this.setState({open: !this.state.open});
  };

  render() {
    const { open } = this.state;
    const { hasChildren, children, toggler } = this.props;
    const classes = classNames('sidebar-section-group', {
      'with-children': hasChildren,
    });
    const childrenClasses = classNames('sidebar-section-group-children', {
      open,
    });
    const toggleIcon = open ? 'chevron-up' : 'chevron-down';
    if (hasChildren) {
      const newToggler = React.cloneElement(toggler(), {
        toggleIcon,
        hasChildren: true,
        onClick: this.onToggle,
      });
      return (
        <div className={classes}>
          {newToggler}
          <div className={childrenClasses}>
            {children}
          </div>
        </div>
      );
    }
    return (
      <div className={classes}>
        {children}
      </div>
    );
  }
}

const mapStateToProps = (state: any, ownState: any) => ({
  user: state.auth.user || {roles: []},
  locale: state.locale,
  ...ownState,
}) ;
const mapDispatchToProps = ({});

class Sidebar extends React.Component<any, any> {

  handleClick = e => {
    if (e.target.getAttribute('href') === '#' || e.target.parentElement.getAttribute('href') === '#') {
      return;
    }
    this.props.toggleSidebar();
  };

  render() {
    const classes = classNames('sidebar', this.props.theme.sidebar, {
      open : this.props.open,
    });
    const { user, locale } = this.props;
    if (!user.roles) {
      user.roles = [];
    }
    const localizedUrl = url => {
      return `/${locale +  url}`;
    };

    return [
      <aside className={classes} onClick={this.handleClick} key={'aside-sidebar'}>
        <div className="sidebar-section">
          <div className="sidebar-section-title">Main</div>
          <SidebarGroup>
            <SidebarItem title="Dashboard" href="/" icon="tachometer"/>
          </SidebarGroup>
          {can(user, 'create-users', 'update-users', 'delete-users') &&
            <SidebarGroup hasChildren={true} toggler={() => <SidebarItem title="Users" icon="users"/>}>
              <SidebarItem title="All users" href={localizedUrl('/users')}/>
              <SidebarItem title="Add new" href={localizedUrl('/users/create')}/>
            </SidebarGroup>
          }
          {can(user, 'create-menus', 'update-menus', 'delete-menus') &&
            <SidebarGroup hasChildren={true} toggler={() => <SidebarItem title="Menu" icon="bars"/>}>
              <SidebarItem title="All menus" href={localizedUrl('/menus')}/>
              <SidebarItem title="Add new" href={localizedUrl('/menus/create')}/>
            </SidebarGroup>
          }
        </div>
          <div className="sidebar-section">
            <div className="sidebar-section-title">Contents</div>
              {can(user, 'create-content-types', 'update-content-types', 'delete-content-types', 'create-contents', 'update-contents', 'delete-contents') &&
                <SidebarGroup hasChildren={true} toggler={() => <SidebarItem title="Content types" icon="cogs"/>}>
                  <SidebarItem title="All content types" href={localizedUrl('/content-types')}/>
                  {can(user, 'create-content-types', 'update-content-types', 'delete-content-types') &&
                    <SidebarItem title="Add new" href={localizedUrl('/content-types/create')}/>
                  }
                </SidebarGroup>
              }
              {can(user, 'create-taxonomies', 'update-taxonomies', 'delete-taxonomies', 'create-terms', 'update-terms', 'delete-terms') &&
                <SidebarGroup hasChildren={true} toggler={() => <SidebarItem title="Taxonomies" icon="cogs"/>}>
                  <SidebarItem title="All taxonomies" href={localizedUrl('/taxonomies')}/>
                  {can(user, 'create-taxonomies', 'update-taxonomies', 'delete-taxonomies') &&
                    <SidebarItem title="Add new" href={localizedUrl('/taxonomies/create')}/>
                  }
                </SidebarGroup>
              }
              <SidebarGroup>
                <SidebarItem title="Medias" href="/medias"/>
              </SidebarGroup>
          </div>
      </aside>,
      <div key={'backdrop-sidebar'} className="sidebar-backdrop" onClick={this.props.toggleSidebar}/>,
    ];
  }
}

export default withTheme<any>(connect(mapStateToProps, mapDispatchToProps)(Sidebar));
