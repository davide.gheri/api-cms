import * as React from 'react';
import { connect } from 'react-redux';
import * as classNames from 'classnames';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '@actions';
import { Form, FormGroup, TextInput, Button, Icon } from '@core/common/components';
import withErrorBoundary from '@components/withErrorBoundary';
import { trans } from '@core/backend/utils';

const mapStateToProps = (state: any) => ({
  auth: state.auth,
});

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

interface LoginState {
  email: string;
  password: string;
  errors: {
    [key: string]: boolean | string;
  };
  valid: boolean;
  [key: string]: any;
}

class Login extends React.Component<any, LoginState> {
  constructor(props: any) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errors: {
        email: false,
        password: false,
      },
      valid: false,
    };
  }

  componentWillMount() {
    if (this.props.auth.authenticated) {
      this.props.push('/');
    }
  }

  verifyValue = (name: string) => {
    const { errors } = this.state;
    errors[name] = !Boolean(this.state[name]);
    this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
  };

  onChange = e => {
    const { name, value } = e.target;
    this.setState({[name]: value}, () => this.verifyValue(name));
  };

  onSubmit = () => {
    const { email, password } = this.state;
    this.props.login({email, password});
  };

  render() {
    const classes = classNames('flex', 'items-center', 'justify-center', 'h-screen', 'px-4', 'login');
    const { email, password, errors, valid } = this.state;
    const { loading, errored } = this.props.auth;
    return (
      <div className={classes}>
        <div className="card max-w-sm">
          <div className="card-body">
            <h5 className="card-title">Login</h5>
            <Form onSubmit={this.onSubmit}>
              <FormGroup label="Email" name="email">
                <TextInput type="email" name="email" required value={email} onChange={this.onChange} errored={Boolean(errors.email) || errored}/>
                {errors.email && <span className="help help-error">{trans('auth.email.required')}</span>}
                {(!errors.email && errored) && <span className="help help-error">{trans('auth.failed')}</span>}
              </FormGroup>
              <FormGroup label="Password" name="password">
                <TextInput type="password" name="password" required value={password} onChange={this.onChange} errored={Boolean(errors.password) || errored}/>
                {errors.password && <span className="help help-error">{trans('auth.password.required')}</span>}
              </FormGroup>
              <Button className="w-24" disabled={!valid} type="submit">
                {loading ? <Icon icon="spinner" spin/> : 'Login'}
              </Button>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Login));
