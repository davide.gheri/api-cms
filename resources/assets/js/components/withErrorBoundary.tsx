import * as React from 'react';
import * as Sentry from '@sentry/browser';
import { Button, Icon } from '@core/common/components';
import { withTheme } from '@core/common/contexts';
import classNames from 'classnames';

export function withErrorBoundary<T = {}>(Component: React.ComponentType<T>) {
  class ErrorBoundary extends React.Component<any, any> {
    static displayName = `withErrorBoundary(${Component.displayName || 'Component'})`;
    state = {
      errored: false,
      error: null,
      errorInfo: null,
    };

    componentDidCatch(error, errorInfo) {
      this.setState({
        error,
        errorInfo,
        errored: true,
      });
      if (process.env.MIX_SENTRY_REPORT) {
        Sentry.configureScope(scope => {
          Object.keys(errorInfo).forEach(key => {
            scope.setExtra(key, errorInfo[key]);
          });
        });
        Sentry.captureException(error);
      }
    }

    render() {
      const { theme } = this.props;
      if (this.state.errored) {
        return (
          <div className={classNames('error-boundary', theme.errorBoundary)}>
            <span>
              <Icon icon={'exclamation-triangle'} className="mr-2 text-red"/>
              An error occoured
              <Icon icon={'exclamation-triangle'} className="ml-2 text-red"/>
            </span>
            <Button onClick={() => Sentry.showReportDialog()}>Report feedback</Button>
          </div>
        );
      }
      return <Component {...this.props}/>;
    }
  }

  return withTheme(ErrorBoundary);
}

export default withErrorBoundary;
