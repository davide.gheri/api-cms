import * as React from 'react';
import * as classNames from 'classnames';
import { Button } from '@core/common/components';

export default (props: any) => {
  const { className, ...rest } = props;
  const classes = classNames('page-error', '403', 'text-teal', className);
  return (
    <div className={classes}>
      <h1>403</h1>
      <h3>Forbidden</h3>
      <Button className="btn-teal" onClick={() => props.history.goBack()}>Go back</Button>
    </div>
  );
};
