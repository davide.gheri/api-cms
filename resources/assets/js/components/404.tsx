import * as React from 'react';
import * as classNames from 'classnames';
import { Button } from '@core/common/components';

export default (props: any) => {
  const { className, ...rest } = props;
  const classes = classNames('page-error', '404', 'text-teal', className);
  return (
    <div className={classes}>
      <h1>404</h1>
      <h3>Not found</h3>
      <Button className="btn-teal" onClick={() => props.history.goBack()}>Go back</Button>
    </div>
  )
};
