import * as React from 'react';
import * as classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '@actions';
import { Button, Icon, Dropdown, DropdownDivider, DropdownItem, DropdownSubMenu } from '@core/common/components';
import { withTheme, ThemeContext, themes } from '@core/common/contexts';

const mapStateToProps = (state: any, ownState: any) => ({
  auth: state.auth,
  ...ownState,
});
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class Navbar extends React.Component<any, any> {
  onLogout = () => {
    this.props.logout();
  };

  render() {
    const { loading } = this.props.auth;
    const classes = classNames('navbar', this.props.theme.navbar);
    return(
      <header className={classes}>
        <div className="navbar-title">
          <div className="navbar-toggle">
            <button onClick={this.props.toggleSidebar}>
              <svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <title>Menu</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
              </svg>
            </button>
          </div>
          <h3>Api-Cms</h3>
        </div>
        <nav className="navbar-menu">
          <Dropdown title={() => (<span><Icon icon="cog"/> <Icon icon="caret-down"/></span>)}>
            <DropdownSubMenu title="Locale">
              <DropdownItem onClick={() => this.props.setLocale('it')}>It</DropdownItem>
              <DropdownDivider/>
              <DropdownItem onClick={() => this.props.setLocale('en')}>En</DropdownItem>
            </DropdownSubMenu>
            <ThemeContext.Consumer>
              {({theme, toggle}) => (
                <DropdownSubMenu title="Theme">
                  {Object.keys(themes).map((name, k) => (
                    <DropdownItem key={k} onClick={() => toggle(name)}>{name}</DropdownItem>
                  ))}
                </DropdownSubMenu>
              )}
            </ThemeContext.Consumer>
          </Dropdown>
          <Button className="ml-auto btn-none" onClick={this.onLogout}>
            {loading ? <Icon icon="spinner" spin/> : <Icon icon="sign-out"/>}
          </Button>
        </nav>
      </header>
    );
  }
}

export default withTheme<any>(connect(mapStateToProps, mapDispatchToProps)(Navbar));
