import * as React from 'react';
import { connect } from 'react-redux';
import * as classNames from 'classnames';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '@actions';
import { Icon } from '@core/common/components';

const mapStateToProps = (state: any) => ({
  auth: state.auth,
});

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class Activate extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  componentWillMount() {
    if (this.props.auth.authenticated) {
      this.props.push('/');
    }
    const { token } = this.props.match.params;
    if (!token) {
      this.props.push('/404');
    } else {
      this.props.activate(token);
    }
  }

  componentWillReceiveProps(props) {
    if (props.auth) {
      this.props.push('/');
    }
  }

  render() {
    const classes = classNames('flex', 'items-center', 'justify-center', 'h-screen', 'px-4', 'login');
    return (
      <div className={classes}>
        <div className="card max-w-sm bg-white">
          <div className="card-body flex items-center flex-col">
            <h5 className="card-title">We are activating your account...</h5>
            <div className="p-12">
              <Icon icon="circle-notch" className="text-teal" size="3x" spin/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Activate);
