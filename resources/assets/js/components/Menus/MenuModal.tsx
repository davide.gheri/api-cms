import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { slugify } from '@core/common/utils';
import * as ActionCreators from '@actions';
import {
    Modal, ModalContent, ModalTitle,
    Form, FormGroup, Textarea, TextInput,
    Button, Icon,
} from '@core/common/components';
import { MenuModel } from '@models';

const mapStateToProps = (state: any, ownState: any) => ({
  loading: state.menus.loading,
  requestErrors: state.menus.errors['0'] || {},
  menu: ownState.menu || {},
  isEdit: ownState.isEdit || false,
});
const mapDispatchToProps = dispatch => (
    bindActionCreators(ActionCreators, dispatch)
);

class MenuModal extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      menu: {
        name: '',
        slug: '',
        description: '',
      },
      errors: {},
      valid: false,
      dirty: false,
    };
  }

  componentDidMount() {
    const { menu, isEdit } = this.props as {menu: MenuModel, isEdit: boolean};
    const { dirty } = this.state;
    if (isEdit && !dirty) {
      const newMenu = {
        name: menu.name || '',
        description: menu.description || '',
        slug: menu.slug || '',
      };
      this.setState({menu: newMenu});
    }
  }

  componentWillReceiveProps(props: {menu: MenuModel, isEdit: boolean}) {
    const { menu, isEdit } = props;
    const { dirty } = this.state;
    if (isEdit && !dirty) {
      const newMenu = {
        name: menu.name || '',
        description: menu.description || '',
        slug: menu.slug || '',
      };
      this.setState({menu: newMenu, dirty: Object.keys(menu).length > 0});
    }
  }

  onSubmit = () => {
    const { menu } = this.state;
    const { id } = this.props.menu;
    if (this.props.isEdit) {
      this.props.updateMenu(id, menu);
    } else {
      this.props.createMenu(menu);
    }
  };

  verifyValue = (name: string) => {
    const toVerify = ['name', 'slug'];
    if (toVerify.includes(name)) {
      const { errors } = this.state;
      errors[name] = !Boolean(this.state.menu[name]);
      this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
    }
  };

  onChange = e => {
    const { menu } = this.state;
    const { name, value } = e.target;
    if (name === 'slug') {
      menu[name] = slugify(value);
    } else {
      menu[name] = value;
    }
    this.setState({menu}, () => this.verifyValue(name));
  };

  onAutoSlug = () => {
    const { menu } = this.state;
    menu.slug = slugify(menu.name);
    this.setState({menu}, () => this.verifyValue('slug'));
  };

  render() {
    const { menu, errors, valid } = this.state;
    Logger.log(menu);
    const { loading, push, requestErrors } = this.props;
    return (
      <Modal className="sm:w-1/2" onClose={() => push('/menus')}>
        <ModalTitle title="Create new Menu"/>
        <ModalContent>
          <Form onSubmit={this.onSubmit}>
            <FormGroup label="Name">
              <TextInput type="text"
                         name="name"
                         value={menu.name || ''}
                         onChange={this.onChange}
                         errored={errors.name || requestErrors.name || false}
                         required/>
                {errors.name && <span className="help help-error">Name is required!</span>}
                {requestErrors.name && <span className="help help-error">{requestErrors.name}</span>}
            </FormGroup>
            <FormGroup label="Slug">
              <TextInput type="text"
                         name="slug"
                         value={menu.slug || ''}
                         errored={errors.slug || requestErrors.slug || false}
                         onChange={this.onChange}
                         append={() => <Button disabled={errors.name} onClick={this.onAutoSlug} text="Auto" type="button"/>}
                         required/>
              {errors.slug && <span className="help help-error">Slug is required!</span>}
              {requestErrors.slug && <span className="help help-error">{requestErrors.slug}</span>}
            </FormGroup>
            <FormGroup label="Description">
              <Textarea name="description"
                        value={menu.description || ''}
                        errored={errors.description || requestErrors.description || false}
                        onChange={this.onChange}/>
              {errors.description && <span className="help help-error">Description is required!</span>}
              {requestErrors.description && <span className="help help-error">{requestErrors.description}</span>}
            </FormGroup>
            <div className="pt-4 flex">
              <Button className="btn-teal shadow" disabled={!valid || loading} type="submit">
                {loading ? <Icon icon="spinner" spin/> : 'Save'}
              </Button>
            </div>
          </Form>
        </ModalContent>
      </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuModal);
