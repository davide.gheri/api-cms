import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SweetAlert from 'sweetalert2-react';
import * as ActionCreators from '@actions';
import { EnhancedTable, PageHeader} from '@core/backend/components';
import { Button, Icon } from '@core/common/components';
import { MenuModel } from '@models';
import { can } from '@core/backend/utils';
import { asyncComponent } from '@core/common/routes/asyncComponent';
import { LocalizedRoute } from '@core/backend/routes';

const mapStateToProps = (state: any) => ({
  menus: state.menus,
  user: state.auth.user,
});
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

const AsyncMenuModal = asyncComponent(import('./MenuModal'));

class Menus extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      swalShow: false,
    };
  }

  componentDidMount() {
    this.props.getMenus();
  }

  onDelete = () => {
    const id = this.state.swalShow;
    this.setState({swalShow: false});
    this.props.deleteMenu(id);
  };

  render() {
    const { loading, menus, loadingSingle } = this.props.menus;
    const { user } = this.props;
    return (
      <div>
        <PageHeader title="Menus">
          {can(user, 'create-menus') &&
          <Button onClick={() => this.props.push('/menus/create')} className="btn-teal shadow"><Icon icon="plus"/> Create new</Button>}
        </PageHeader>
        <EnhancedTable
          className="shadow"
          data={menus}
          columns={Object.assign({
            name: 'Name',
            slug: 'Slug',
            updated_at: 'Last update',
            menu_items_count: 'Items',
          }, can(user, 'delete-menus') ? {actions: 'Actions'} : {})}
          linkedCols={{
            name: can(user, 'update-menus') ? '/menus/{id}' : '/menus/{id}/menus',
            menu_items_count: '/menus/{id}/menus',
          }}
          customCols={{actions: (el: MenuModel) => <Button onClick={() => this.setState({swalShow: el.id})} className="btn-red btn-sm"><Icon icon="trash"/></Button>}}
          rowLoading={(el: MenuModel) => loadingSingle[el.id] === true}
          loading={loading}/>
        {can(user, 'create-menus') && <LocalizedRoute path="/menus/create" render={(props: any) => <AsyncMenuModal {...props}/>}/>}
        {can(user, 'update-menus') && <LocalizedRoute path="/menus/:id(\d+)" render={(props: any) => {
          const { id } = props.match.params;
          const menu = menus.find(menu => menu.id === +id);
          return <AsyncMenuModal {...props} menu={menu} isEdit={true}/>;
        }}/>}
        <SweetAlert show={Boolean(this.state.swalShow)}
                    title="Confirm"
                    text="Are you sure?"
                    showCancelButton={true}
                    onConfirm={this.onDelete}/>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menus);
