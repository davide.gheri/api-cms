import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SweetAlert from 'sweetalert2-react';
import * as ActionCreators from '@actions';
import { ConnectProps, TaxonomyModel, TermModel } from '@models';
import { EnhancedTable, PageHeader } from '@core/backend/components';
import { Button, Icon } from '@core/common/components';
import { asyncComponent } from '@core/common/routes/asyncComponent';
import { LocalizedRoute } from '@core/backend/routes';
import withErrorBoundary from '@components/withErrorBoundary';

const mapStateToProps = (state, ownState) => {
  const { id } = ownState.match.params;
  return {
    taxonomy: state.taxonomies.taxonomies.find(tax => tax.id === +id) || {},
    terms: state.terms.terms[id] || [],
    loading: state.terms.loading,
    loadingSingle: state.terms.loadingSingle[id] || {},
    taxId: id,
  };
};

const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

const AsyncTermModal = asyncComponent(import('./TermModal'));

interface TermsProps extends ConnectProps {
  taxonomy: TaxonomyModel;
  terms: TermModel[];
  loading: boolean;
  loadingSingle: {[key: number]: boolean};
  taxId: number;
}

class Terms extends React.Component<TermsProps, any> {
  constructor(props: TermsProps) {
    super(props);
    this.state = {
      swalShow: false,
    };
  }

  componentDidMount() {
    const { taxId } = this.props;
    this.props.getTaxonomy(taxId);
    this.props.getTerms(taxId);
  }

  onDelete = () => {
    const { taxId } = this.props;
    const id = this.state.swalShow;
    this.setState({swalShow: false});
    this.props.deleteTerm(taxId, id);
  };

  render() {
    const { terms, taxonomy, loading, loadingSingle, taxId, push } = this.props;
    return (
      <div>
        <PageHeader title={`Terms of taxonomy ${taxonomy.name}`}>
          <Button onClick={() => this.props.push(`/taxonomies/${taxId}/terms/create`)} className="btn-teal shadow"><Icon icon="plus"/> Create new</Button>
        </PageHeader>
        <EnhancedTable
          className="shadow"
          data={terms}
          columns={{title: 'Title', content_count: 'Contents', actions: 'Actions'}}
          customCols={{actions: (el: TermModel) => (
            <span>
              <Button onClick={() => push(`/taxonomies/${taxonomy.id}/terms/${el.id}`)} className="btn-teal btn-sm mr-3"><Icon icon="edit"/></Button>
              <Button onClick={() => this.setState({swalShow: el.id})} className="btn-red btn-sm"><Icon icon="trash"/></Button>
            </span>
          )}}
          rowLoading={(el: TermModel) => loadingSingle[el.id] === true}
          loading={loading}/>
          <LocalizedRoute path={'/taxonomies/:id(\\d+)/terms/create'} render={(props: any) => <AsyncTermModal {...props} taxonomy={taxonomy}/>}/>
          <LocalizedRoute path={'/taxonomies/:id(\\d+)/terms/:term_id(\\d+)'} render={(props: any) => {
            const { term_id } = props.match.params;
            const term = terms.find(term => term.id === +term_id);
            return <AsyncTermModal {...props} taxonomy={taxonomy} term={term} isEdit={true}/>;
          }}/>
        <SweetAlert show={Boolean(this.state.swalShow)}
                    title="Confirm"
                    text="Are you sure?"
                    showCancelButton={true}
                    onConfirm={this.onDelete}/>
      </div>
    );
  }
}

export default withErrorBoundary(connect(mapStateToProps, mapDispatchToProps)(Terms));
