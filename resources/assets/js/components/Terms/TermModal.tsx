import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as ActionCreators from '@actions';
import {
    Modal, ModalContent, ModalTitle,
    Form, FormGroup, Textarea, TextInput,
    Button, Icon,
} from '@core/common/components';

const mapStateToProps = (state: any, ownState: any) => ({
  loading: state.terms.loading,
  requestErrors: state.terms.errors[ownState.term ? ownState.term.id : '0'] || {},
  taxonomy: ownState.taxonomy,
  term: ownState.term || {},
  isEdit: ownState.isEdit || false,
});
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class TermModal extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      term: {
        title: '',
        description: '',
      },
      errors: {},
      valid: false,
      dirty: false,
    };
  }

  componentDidMount() {
    const { term, isEdit } = this.props;
    const { dirty } = this.state;
    if (isEdit && !dirty) {
      const newTerm = {
        title: term.title || '',
        description: term.description || '',
      };
      this.setState({term: newTerm});
    }
  }

  componentWillReceiveProps(props) {
    const { term, isEdit } = props;
    const { dirty } = this.state;
    if (isEdit && !dirty) {
      const newTerm = {
        title: term.title || '',
        description: term.description || '',
      };
      this.setState({term: newTerm, dirty: Object.keys(term).length > 0});
    }
  }

  onSubmit = () => {
    const { term } = this.state;
    const { id } = this.props.taxonomy;
    const propsTerm = this.props.term;
    if (this.props.isEdit) {
      this.props.updateTerm(id, propsTerm.id, term);
    } else {
      this.props.createTerm(id, term);
    }
  };

  verifyValue = (name: string) => {
    const toVerify = ['title', 'description'];
    if (toVerify.includes(name)) {
      const { errors } = this.state;
      errors[name] = !Boolean(this.state.term[name]);
      this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
    }
  };

  onChange = e => {
    const { term } = this.state;
    const { name, value } = e.target;
    term[name] = value;
    this.setState({term, dirty: true}, () => this.verifyValue(name));
  };

  render() {
    const { term, errors, valid } = this.state;
    const { loading, push, requestErrors, taxonomy, isEdit } = this.props;
    return (
      <Modal className="sm:w-1/2" onClose={() => push(`/taxonomies/${taxonomy.id}/terms`)}>
        <ModalTitle title={isEdit ? 'Edit Term' : 'Create new Term'}/>
        <ModalContent>
          <Form onSubmit={this.onSubmit}>
            <FormGroup label="Title">
              <TextInput type="text" name="title"
                         value={term.title || ''}
                         onChange={this.onChange}
                         errored={errors.title || requestErrors.title || false}
                         required/>
              {errors.title && <span className="help help-error">Title is required!</span>}
              {requestErrors.title && <span className="help help-error">{requestErrors.title}</span>}
            </FormGroup>
            <FormGroup label="Description">
            <Textarea name="description"
                      value={term.description || ''}
                      errored={errors.description || requestErrors.description || false}
                      onChange={this.onChange}/>
              {errors.description && <span className="help help-error">Description is required!</span>}
              {requestErrors.description && <span className="help help-error">{requestErrors.description}</span>}
            </FormGroup>
            <div className="pt-4 flex">
              <Button className="btn-teal shadow" disabled={!valid || loading} type="submit">
                {loading ? <Icon icon="spinner" spin/> : 'Save'}
              </Button>
            </div>
          </Form>
        </ModalContent>
      </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TermModal);
