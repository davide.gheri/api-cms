import * as React from 'react';
import { PageHeader } from '@core/backend/components';
import { Icon, Button } from '@core/common/components';
import { asyncComponent } from '@core/common/routes/asyncComponent';
import LocalizedRoute from '@core/backend/routes/LocalizedRoute';
import UsersTable from '@components/Users/UsersTable';

const AsyncUserModal = asyncComponent(import('./UserModal'));

const Users: React.StatelessComponent<any> = (props: any) => (
  <div>
    <PageHeader title="Users">
      <Button onClick={() => this.props.push('/users/create')} className="btn-teal shadow"><Icon icon="plus"/> Create new</Button>
    </PageHeader>
    <UsersTable />
    <LocalizedRoute path="/users/create" render={(props: any) => <AsyncUserModal {...props}/>}/>
  </div>
);

Users.displayName = 'Users';

export default Users;
