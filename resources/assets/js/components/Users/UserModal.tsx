import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    Modal, ModalContent, ModalTitle,
    Form, FormGroup, Option, SelectInput, TextInput,
    Button, Icon,
} from '@core/common/components';
import * as ActionCreators from '@actions';
import { ConnectProps } from '@models';

const mapStateToProps = (state: any, ownState: any) => ({
  loading: state.users.loading,
  requestErrors: state.users.errors[ownState.user ? ownState.user.id : '0'] || {},
  user: ownState.user || {},
  isEdit: ownState.isEdit || false,
  roles: state.roles.roles,
});
const mapDispatchToProps = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);

class UserModal extends React.Component<Partial<ConnectProps>, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      user: {
        name: '',
        email: '',
        password: '',
        password_confirmation: '',
        active: false,
        roles: [],
      },
      errors: {
        passwordNotMatch: false,
      },
      valid: false,
      dirty: false,
      passwordVisible: false,
    };
  }

  componentDidMount() {
    const { user, isEdit } = this.props;
    const { dirty } = this.state;
    if (isEdit && !dirty) {
      const newUser = {
        name: user.name || '',
        email: user.email || '',
        active: user.active || false,
        roles: user.roles ? user.roles.map(r => r.id) : [],
      };
      this.setState({user: newUser});
    }
  }

  componentWillReceiveProps(props) {
    const { user, isEdit } = props;
    const { dirty } = this.state;
    if (isEdit && !dirty) {
      const newUser = {
        name: user.name || '',
        email: user.email || '',
        active: user.active || false,
        roles: user.roles ? user.roles.map(r => r.id) : [],
      };
      this.setState({user: newUser, dirty: Object.keys(user).length > 0});
    }
  }

  onSubmit = () => {
    const { user } = this.state;
    const propsUser = this.props.user;
    if (this.props.isEdit) {
      this.props.updateUser(propsUser.id, user);
    } else {
      this.props.createUser(user);
    }
  };

  verifyValue = (name: string) => {
    const toVerify = ['name', 'email', 'password'];
    if (toVerify.includes(name)) {
      const { errors } = this.state;
      errors[name] = !Boolean(this.state.user[name]);
      this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
    }
  };

  onChange = e => {
    const { user } = this.state;
    const { name, value } = e.target;
    user[name] = value;
    this.setState({user, dirty: true}, () => {
      const { password, password_confirmation } = this.state.user;
      if (name === 'password_confirmation' || (name === 'password' && password_confirmation !== '')) {
        const { errors } = this.state;
        errors['passwordNotMatch'] = password !== password_confirmation;
        this.setState({errors, valid: Object.keys(errors).every(k => errors[k] === false)});
      } else {
        this.verifyValue(name);
      }
    });
  };

  onRoleChange = e => {
    const { user } = this.state;
    user.roles = [...e.target.options].filter(o => o.selected).map(o => +o.value);
    this.setState({user, dirty: true});
  };

  onCheckboxChange = e => {
    const { checked } = e.target;
    const { user } = this.state;
    let active = false;
    if (checked) {
      active = true;
    }
    user.active = active;
    this.setState({user, dirty: true});
  };

  render() {
    const { user, errors, valid, passwordVisible } = this.state;
    const { loading, push, requestErrors, isEdit, roles } = this.props;

    return (
      <Modal className="sm:w-1/2" onClose={() => push('/users')}>
        <ModalTitle title={isEdit ? 'Edit User' : 'Create new User'}/>
        <ModalContent>
          <Form onSubmit={this.onSubmit}>
            <FormGroup label="Name">
              <TextInput type="text" name="name"
                         value={user.name || ''}
                         onChange={this.onChange}
                         errored={errors.name || requestErrors.name || false}
                         required/>
              {errors.name && <span className="help help-error">Name is required!</span>}
              {requestErrors.name && <span className="help help-error">{requestErrors.name}</span>}
            </FormGroup>
            <FormGroup label="Email">
              <TextInput name="email"
                         type="email"
                         value={user.email || ''}
                         errored={errors.email || requestErrors.email || false}
                         onChange={this.onChange}/>
                {errors.email && <span className="help help-error">Email is required!</span>}
                {requestErrors.email && <span className="help help-error">{requestErrors.email}</span>}
            </FormGroup>
            <FormGroup label="Password">
              <TextInput name="password"
                         type={passwordVisible ? 'text' : 'password'}
                         value={user.password || ''}
                         errored={errors.password || errors.passwordNotMatch || requestErrors.password || false}
                         onChange={this.onChange}/>
              {errors.password && <span className="help help-error">Password is required!</span>}
              {errors.passwordNotMatch && <span className="help help-error">Password not match!</span>}
              {requestErrors.password && <span className="help help-error">{requestErrors.password}</span>}
            </FormGroup>
            <FormGroup label="Confirm password">
              <TextInput name="password_confirmation"
                         type={passwordVisible ? 'text' : 'password'}
                         value={user.password_confirmation || ''}
                         errored={errors.password_confirmation || errors.passwordNotMatch || requestErrors.password_confirmation || false}
                         onChange={this.onChange}/>
              {requestErrors.password_confirmation && <span className="help help-error">{requestErrors.password_confirmation}</span>}
            </FormGroup>
            <FormGroup label="Roles">
              <SelectInput multiple value={user.roles} onChange={this.onRoleChange} errored={requestErrors.roles}>
                {roles.map(role => (
                  <Option key={role.id} value={role.id}>{role.name}</Option>
                ))}
              </SelectInput>
              {requestErrors.roles && <span className="help help-error">{requestErrors.roles}</span>}
            </FormGroup>
            <FormGroup label="Active" inline={true}>
              <TextInput type="checkbox" className="checkbox" value={1} checked={user.active} onChange={this.onCheckboxChange}/>
            </FormGroup>
            <div className="pt-4 flex">
              <Button className="btn-teal shadow" disabled={!valid || loading} type="submit">
                {loading ? <Icon icon="spinner" spin/> : 'Save'}
              </Button>
            </div>
          </Form>
        </ModalContent>
      </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserModal);
