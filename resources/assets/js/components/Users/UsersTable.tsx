import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '@actions';
import { EnhancedTable } from '@core/backend/components';
import { Icon, Button } from '@core/common/components';
import { asyncComponent } from '@core/common/routes/asyncComponent';
import SweetAlert from 'sweetalert2-react';
import { ConnectProps } from '@models';
import { UserModel } from '@core/backend/models';
import { currentPage } from '@core/common/utils';
import LocalizedRoute from '@core/backend/routes/LocalizedRoute';
import LocalizedLink from '@core/backend/routes/LocalizedLink';
import withErrorBoundary from '@components/withErrorBoundary';
import { withRouter } from 'react-router';

const mapStateToProps = (state: any) => ({
  users: state.users,
});
const mapDispatchToprops = dispatch => (
  bindActionCreators(ActionCreators, dispatch)
);
const AsyncUserModal = asyncComponent(import('./UserModal'));

class UsersTable extends React.Component<ConnectProps, any> {
  constructor(props: ConnectProps) {
    super(props);
    this.state = {
      swalShow: false,
    };
  }

  componentDidMount() {
    this.props.getUsers(currentPage(this.props.location.search));
  }

  changePage = (page: number) => {
    this.props.getUsers(page);
    const { pathname } = this.props.location;
    this.props.push(`${pathname}?page=${page}`);
  };

  onDelete = () => {
    const id = this.state.swalShow;
    this.setState({swalShow: false});
    this.props.deleteUser(id);
  };

  render() {
    const { loading, users, loadingSingle, page } = this.props.users;
    return (
      <div>
        <EnhancedTable
          pagination={page}
          onPageChange={this.changePage}
          className="shadow"
          data={users}
          columns={{
            name: 'Name',
            email: 'Email',
            active: 'Active',
            actions: 'Actions',
          }}
          customCols={{
            name: (el: UserModel) => <span className="flex items-center">{el.avatar && <img className="mr-2" width="25" height="25" src={el.avatar}/>} {el.name}</span>,
            active: (el: UserModel) => el.active ? <span className="label label-teal">Active</span> : <span className="label label-grey">Not active</span>,
            actions: (el: UserModel) => (
              <span>
                <LocalizedLink to={`/users/${el.id}`} className="btn btn-teal btn-sm mr-3"><Icon icon="edit"/></LocalizedLink>
                <Button onClick={() => this.setState({swalShow: el.id})} className="btn-red btn-sm"><Icon icon="trash"/></Button>
              </span>
            ),
          }}
          rowLoading={(el: UserModel) => loadingSingle[el.id] === true}
          loading={loading}/>
        <LocalizedRoute path="/users/:id(\d+)" render={(props: any) => {
          const { id } = props.match.params;
          const user = users.find(user => user.id === +id);
          return <AsyncUserModal {...props} user={user} isEdit={true}/>;
        }}/>
        <SweetAlert show={Boolean(this.state.swalShow)}
                    title="Confirm"
                    text="Are you sure?"
                    showCancelButton={true}
                    onConfirm={this.onDelete}
                    onCancel={() => this.setState({swalShow: false})}/>
      </div>
    );
  }
}

export default withRouter(withErrorBoundary(connect(mapStateToProps, mapDispatchToprops)(UsersTable)));
