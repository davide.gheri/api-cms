
const VERSION = 'v1';
const del = ':';
export const apiUrl = (uri: string) => `/api/${VERSION}/${uri}`;

export default {
  placeholderDelimeter: del,
  dictionaries: apiUrl(`lang/${del}lang`),
  tokens: {
    index: '/oauth/personal-access-tokens',
    store: '/oauth/personal-access-tokens',
    delete: `/oauth/personal-access-tokens/${del}id`,
  },
  fieldSchemas: {
    index: apiUrl('field-schemas'),
  },
  contentTypes: {
    index: apiUrl('content-types'),
    show: apiUrl(`content-types/${del}id`),
    update: apiUrl(`content-types/${del}id`),
    store: apiUrl('content-types'),
    delete: apiUrl(`content-types/${del}id`),
    taxonomies: apiUrl(`content-types/${del}id/taxonomies`),
  },
  contentTypeFields: {
    index: apiUrl(`content-types/${del}id/fields`),
    store: apiUrl(`content-types/${del}id/fields`),
    update: apiUrl(`content-types/${del}id/fields/${del}fieldId`),
    delete: apiUrl(`content-types/${del}id/fields/${del}fieldId`),
  },
  contents: {
    index: apiUrl(`content-types/${del}id/contents`),
    show: apiUrl(`content-types/${del}id/contents/${del}contentId`),
    update: apiUrl(`content-types/${del}id/contents/${del}contentId`),
    store: apiUrl(`content-types/${del}id/contents`),
    delete: apiUrl(`content-types/${del}id/contents/${del}contentId`),
    attachment: apiUrl(`content-types/${del}id/contents/${del}contentId/attachments`),
  },
  taxonomies: {
    index: apiUrl('taxonomies'),
    show: apiUrl(`taxonomies/${del}id`),
    update: apiUrl(`taxonomies/${del}id`),
    store: apiUrl('taxonomies'),
    delete: apiUrl(`taxonomies/${del}id`),
    contentTypes: apiUrl(`taxonomies/${del}id/content-types`),
  },
  terms: {
    index: apiUrl(`taxonomies/${del}id/terms`),
    show: apiUrl(`taxonomies/${del}id/terms/${del}termId`),
    update: apiUrl(`taxonomies/${del}id/terms/${del}termId`),
    store: apiUrl(`taxonomies/${del}id/terms`),
    delete: apiUrl(`taxonomies/${del}id/terms/${del}termId`),
  },
  users: {
    index: apiUrl('users'),
    show: apiUrl(`users/${del}id`),
    update: apiUrl(`users/${del}id`),
    store: apiUrl('users'),
    delete: apiUrl(`users/${del}id`),
  },
  roles: {
    index: apiUrl('roles'),
  },
  medias: {
    index: apiUrl('medias'),
    show: apiUrl(`medias/${del}id`),
    update: apiUrl(`medias/${del}id`),
    store: apiUrl('medias'),
    delete: apiUrl(`medias/${del}id`),
  },
  menus: {
    index: apiUrl('menus'),
    show: apiUrl(`menus/${del}id`),
    update: apiUrl(`menus/${del}id`),
    store: apiUrl('menus'),
    delete: apiUrl(`menus/${del}id`),
  },
};
