import createBrowserHistory from 'history/createBrowserHistory';
import { routerMiddleware } from 'react-router-redux';
import { applyMiddleware, compose, createStore, Store } from 'redux';
import reducers from '@reducers';
import thunk from 'redux-thunk';

const w = window as any;

export const history = createBrowserHistory();
const middleware = routerMiddleware(history);
const composeEnhancers = w.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store: Store = createStore(
    reducers,
    composeEnhancers(applyMiddleware(middleware, thunk)),
);
