<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTypeFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_type_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('field_schema_id')->index();
            $table->unsignedInteger('content_type_id')->index();
            $table->string('name');
            $table->string('slug');
            $table->json('options');
            $table->timestamps();

            $table->unique(['slug', 'content_type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_type_fields');
    }
}
