<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin_role = \Spatie\Permission\Models\Role::create(['name' => 'admin', 'guard_name' => 'api']);
        $editor_role = \Spatie\Permission\Models\Role::create(['name' => 'editor', 'guard_name' => 'api']);
        $subscriber_role = \Spatie\Permission\Models\Role::create(['name' => 'subscriber', 'guard_name' => 'api']);

        $users_permissions = collect([
            ['name' => 'create-users'],
            ['name' => 'update-users'],
            ['name' => 'delete-users'],
        ])->mapInto(\Spatie\Permission\Models\Permission::class)->each->save()->each->syncRoles([$admin_role]);

        $content_types_permissions = collect([
            ['name' => 'create-content-types'],
            ['name' => 'update-content-types'],
            ['name' => 'delete-content-types'],
        ])->mapInto(\Spatie\Permission\Models\Permission::class)->each->save()->each->syncRoles([$admin_role]);

        $contents_permissions = collect([
            ['name' => 'create-contents'],
            ['name' => 'update-contents'],
            ['name' => 'delete-contents'],
        ])->mapInto(\Spatie\Permission\Models\Permission::class)->each->save()->each->syncRoles([$admin_role, $editor_role]);

        $taxonomies_permissions = collect([
            ['name' => 'create-taxonomies'],
            ['name' => 'update-taxonomies'],
            ['name' => 'delete-taxonomies'],
        ])->mapInto(\Spatie\Permission\Models\Permission::class)->each->save()->each->syncRoles([$admin_role]);

        $terms_permissions = collect([
            ['name' => 'create-terms'],
            ['name' => 'update-terms'],
            ['name' => 'delete-terms'],
        ])->mapInto(\Spatie\Permission\Models\Permission::class)->each->save()->each->syncRoles([$admin_role, $editor_role]);

        $menus_permissions = collect([
            ['name' => 'create-menus'],
            ['name' => 'update-menus'],
            ['name' => 'delete-menus'],
        ])->mapInto(\Spatie\Permission\Models\Permission::class)->each->save()->each->syncRoles([$admin_role]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Spatie\Permission\Models\Role::whereIn('name', ['admin', 'subscriber', 'editor'])->delete();
        \Spatie\Permission\Models\Permission::whereIn('name', [
            'create-users',
            'update-users',
            'delete-users',
            'create-content-types',
            'update-content-types',
            'delete-content-types',
            'create-contents',
            'update-contents',
            'delete-contents',
            'create-taxonomies',
            'update-taxonomies',
            'delete-taxonomies',
            'create-terms',
            'update-terms',
            'delete-terms',
        ])->delete();
    }
}
