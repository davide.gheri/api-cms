<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldSchemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_schemas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('field_type');
            $table->json('options')->nullable();
            $table->json('available_options');
        });

        $this->fill();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_schemas');
    }

    private function fill()
    {
        $fields = [
            ['name' => 'textfield', 'field_type' => 'text',
                'options' => json_encode(['type' => 'text'], JSON_FORCE_OBJECT),
                'available_options' => json_encode([
                    'type' => 'string',
                    'required' => 'boolean',
                    'maxlength' => 'integer',
                ], JSON_FORCE_OBJECT)
            ],
            ['name' => 'textarea', 'field_type' => 'textarea',
                'options' => json_encode(['excerpt' => true], JSON_FORCE_OBJECT),
                'available_options' => json_encode([
                    'required' => 'boolean',
                    'maxlength' => 'integer',
                    'excerpt' => 'boolean'
                ], JSON_FORCE_OBJECT)
            ]
        ];
        \Illuminate\Support\Facades\DB::table('field_schemas')->insert($fields);
    }
}
