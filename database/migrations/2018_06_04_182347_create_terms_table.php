<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('taxonomy_id')->index();
            $table->json('title');
            $table->json('description');
            $table->timestamps();

            $table->foreign('taxonomy_id')->references('id')->on('taxonomies')->onDelete('cascade');
        });

        Schema::create('content_term', function (Blueprint $table) {
            $table->unsignedInteger('content_id')->index();
            $table->unsignedInteger('term_id')->index();

            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
            $table->foreign('term_id')->references('id')->on('terms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_term');
        Schema::dropIfExists('terms');
    }
}
