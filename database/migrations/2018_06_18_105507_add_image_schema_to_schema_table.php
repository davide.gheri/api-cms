<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageSchemaToSchemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $fields = [
            ['name' => 'attachment', 'field_type' => 'file',
                'options' => json_encode(['type' => 'file'], JSON_FORCE_OBJECT),
                'available_options' => json_encode([
                    'required' => 'boolean',
                ], JSON_FORCE_OBJECT)
            ],
        ];
        \Illuminate\Support\Facades\DB::table('field_schemas')->insert($fields);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::table('field_schemas')->where('name', 'attachment')->delete();
    }
}
