let mix = require('laravel-mix');
let webpack = require('webpack');
let tailwindcss = require('tailwindcss');
let TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

mix.disableNotifications();
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    resolve: {
        plugins: [new TsconfigPathsPlugin()]
    },
    // entry: {
    //     vendor: [
    //         'react',
    //         'redux',
    //         'lodash',
    //         'draft-js',
    //         '@core/backend'
    //     ]
    // },
    // output: {
    //     chunkFilename: `js/chunks/[chunkhash].chunk.js`,
    //     publicPath: '/',
    // }
});

mix.ts('resources/assets/js/app.tsx', 'public/js').sourceMaps()
    .extract([
        'react',
        'redux',
        'lodash',
        'draft-js',
        '@core/backend'
    ])
    .sass('resources/assets/sass/app.scss', 'public/css').version().sourceMaps()
    .options({
        processCssUrls: false,
        postCss: [
            tailwindcss('./tailwind.js')
        ]
    });
